/**
 * Includes all validation scripts, uses Jquery Validate Plugin.
 * Author: Virgilio Lalamoro 8/9/2016
 */

(function($) {
	$.script = {

		init : function()
		{
			$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
			});

			$this = $.script;

			// On click validation for password reset form
			$('#reset-password-btn').on('click', function(e)
			{
				e.preventDefault();
				var url = $(this).data('email-validate-uri'),
					valid = $this.validatePasswordReset(url);
				if(valid){
					$this.submitPasswordReset();
				}

				return false;
			});

			// On click validation create category form
			$('body').on('click', '.create-category-btn', function(e)
			{
				e.preventDefault();
				var url = $('#creating-expense-category-form').attr('action'),
					valid = $this.validateExpenseCategoryForm();

				if(valid){
					$('#creating-expense-category-form').submit();
				}

				return false;
			});

			$('body').on('click', '.update-category-btn', function(e)
			{
				e.preventDefault();
				var url = $('#updating-expense-category-form').attr('action'),
					category = $(this).data('category'),
					valid = $this.validateUpdatingExpenseCategoryForm(category);

				if(valid){
					$('#updating-expense-category-form').submit();
				}

				return false;
			});
		},

		/**
		 * Error message placement function
		 */
		errorPlacementAction : function(form, error)
		{
			var placement = $(form).find('.error-placement');

			placement.removeClass('hidden');
			error.insertAfter(placement);
		},

		/**
		 * Validation Functions
		 */
		validatePasswordReset : function(url)
		{
			$('#form-password-reset').validate({
				rules : {
					email: {
                        required: true,
                        email: true,
                        remote: {
                            url : url,
                            type : 'POST'
                        }
                    },
				},
				messages : {
					email: {
                        required: 'Please provide the email, in where we can send the reset link.',
                        email: 'This is not a valid email',
                        remote: 'We cannot find an account associated with this email.'
                    },
				}
			})
			var result = $('#form-password-reset').validate().form();
			return result;
		},

		validateExpenseCategoryForm : function()
		{
			$('#creating-expense-category-form').validate({
				rules : {
					title: {
                        required: true,
                        remote: {
                            url : 'category/validate',
                            type : 'GET'
                        }
                    },
				},
				messages : {
					title: {
                        required: 'Please provide the title of the category',
                        remote: 'We cannot find an account associated with this email.'
                    },
				}
			})
			var result = $('#creating-expense-category-form').validate().form();
			return result;
		},

		validateUpdatingExpenseCategoryForm : function(category)
		{
			$('#updating-expense-category-form').validate({
				rules : {
					title: {
                        required: true,
                        remote: {
                            url : 'category/validate?category='+category,
                            type : 'GET'
                        }
                    },
				},
				messages : {
					title: {
                        required: 'Please provide the title of the category',
                        remote: 'We cannot find an account associated with this email.'
                    },
				}
			})
			var result = $('#updating-expense-category-form').validate().form();
			return result;
		},

		/**
		 * Submission Functions
		 */
		submitPasswordReset : function()
		{
			var $url = $('#form-password-reset').attr('action');
			$('#form-password-reset').find('.validation-group alert').remove();
			$.ajax({
				url : $url,
				type : "POST",
				data : $('#form-password-reset').serialize(),
				error : function(data){
					$('.validation-group').removeClass('hidden').append('<div class="alert alert-danger">'+data.message+'</div>');
				},
				success : function(data){
					$('.validation-group').removeClass('hidden').append('<div class="alert alert-success">'+data.message+'</div>');
				}
			});
		},

	};

	$.script.init();
})(jQuery);