/**
 * Main JS file that includes aesthetics scripts and others.
 * Author: Jr Lalamoro 2/19/2017
 */

$(document).ready(function(){

    // Initialize AJAX script requirements of Laravel
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    // Set the Base URL
    var baseUrl = $('meta[name="base-url"]').attr('content');

	/**
     * Script to Initialize Quick preview of image
     * use .quick-preview class with data-img attribute of path of the image
     */
    $('body').on('click', '.btn-preview-modal-image', function(e){
        var src = $(this).data('path');

        $('#modal-preview-image .modal-preview-img').attr('src', src);
    });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.fileinput-preview').find('img').attr('src', e.target.result).closest('div').removeClass('hidden');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $('#file-browse').on('change', function(){
        readURL(this);
    });

    /**
     * Script to span to all .restop-checkbox
     */
    restopCheckbox = function(){
        $('body').find('.restop-checkbox input[type="checkbox"]').each(function(){
            if($(this).siblings('span').length < 1){
                $(this).after('<span></span>');
            }
        });
    }
    restopCheckbox();

    /**
     * Script to span to all .restop-radio
     */
    restopRadio = function(){
        $('body').find('.restop-radio input[type="radio"]').each(function(){
            if($(this).siblings('span').length < 1){
                $(this).after('<span></span>');
            }
        });
    }
    restopRadio();

    /**
     * Script to initialize the Chosen SELECT
     */
    $(".chosen-select").chosen();
    $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
    
    /**
     * Script to initialize the Selectize SELECT
     */
    if ($('.selectize').length) {
        $('.selectize').selectize();
    }

    if ($('.selectize-create').length) {
        $('.selectize-create').selectize({
            delimiter: ',',
            persist: false,
            create: function(input) {
                return {
                    value: input,
                    text: input
                }
            }
        });
    }

    /**
     * Script to initiliaze time picker
     */
    $('.timepicker').timepicker();

    /**
     * Script to Initialize Alert for Deletion
     */
    $(".confirmAlert").click(function(e) {
        e.preventDefault();

        var msg = $(this).data('confirm-message') || 'Are you sure?';
        var msgText = ($(this).data('confirm-text') != '' || $(this).data('confirm-text') != undefined) ? $(this).data('confirm-text') : 'You won`t be able to revert this!';
        var _self = $(this);
        swal({   
            title: msg,   
            text: msgText,   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonText: "Yes, Continue!",   
            confirmButtonColor: "#FF5722",
            closeOnConfirm: false 
        }).then(function(isConfirm){
            if(isConfirm) {
                window.location = _self.attr("href")
            }
        });
    });

    /**
     * Script to Initialize Alert to Prevent Deletion
     */
    $(".deleteAlert").click(function(e) {
        e.preventDefault();

        var msg = $(this).data('confirm-message') || 'Delete Unable!';
        var msgText = ($(this).data('confirm-text') != '') ? $(this).data('confirm-text') : '';
        var _self = $(this);

        swal({   
            title: msg,
            text: msgText,
            type: 'warning'
        });
    });

    /**
     * Scripts to initialize Table Script
     */
    $('.table-data-init').dataTable({
        "lengthMenu": [[ 10, 25, 50, -1 ], [10, 25, 50, "All"]],
    });

    /**
     * Scripts to initialize Daterange Picker
     */
    $('.date-range-picker').daterangepicker({
        locale: {
            format: 'M/D/Y'
        },
    });

    $(".datepicker").bsdatepicker({
        autoclose : true,
    });
    $('.datepicker').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });

    /**
     * Script to initilize error messages at top of screen
     * @type {[type]}
     */
    var notifBar = $('#notification-show');
    if(notifBar.length > 0){
        var self = notifBar,
            message = self.data('message'),
            type = self.data('type');

        $.jGrowl(message, {
            sticky: false,
            position: 'top-right',
            theme: type,
            life: 6000
        });
    }

    // Click of Enter the click the button to
    $('body').on('click', '.input-keypress', function(){
        let target = $(this).data('click-target');
        $(this).keypress(function(e) {
            if(e.which == 13) {
                $(target).click();
            }
        });
    });

	/* Multiselect inputs */
    $(function() { "use strict";
        $(".spinner-input").spinner();

        $('.input-switch').bootstrapSwitch();

        $(".multi-select").multiSelect();
        $(".ms-container").append('<i class="glyph-icon icon-exchange"></i>');
    });

	function setMainSectionHeight(){
		var headerHeight = $('.header').height(),
			screenHeight = $(window).height(),
			mainHeight = screenHeight - headerHeight - 35;

		$('.main').css('min-height', mainHeight);
	}
	setMainSectionHeight();

	$(window).load(function(){
        setTimeout(function() {
            $('#loading').fadeOut( 400, "linear" );
        }, 300);
    });

    /**
     *
     * @param type string 'insertAfter' or 'insertBefore'
     * @param entityName
     * @param id
     * @param positionId
     */
    var changePosition = function(requestData){
        $.ajax({
            'url': baseUrl + '/sort',
            'type': 'POST',
            'data': requestData,
            'success': function(data) {
                if (data.success) {
                    console.log('Saved!');
                } else {
                    console.error(data.errors);
                }
            },
            'error': function(){
                console.error('Something wrong!');
            }
        });
    };

    var $sortableTable = $('.sortable');
    if ($sortableTable.length > 0) {
        $sortableTable.sortable({
            handle: '.sortable-handle',
            axis: 'y',
            update: function(a, b){

                var entityName = $(this).data('entityname');
                var $sorted = b.item;

                var $previous = $sorted.prev();
                var $next = $sorted.next();

                if ($previous.length > 0) {
                    changePosition({
                        parentId: $sorted.data('parentid'),
                        type: 'moveAfter',
                        entityName: entityName,
                        id: $sorted.data('itemid'),
                        positionEntityId: $previous.data('itemid')
                    });
                } else if ($next.length > 0) {
                    changePosition({
                        parentId: $sorted.data('parentid'),
                        type: 'moveBefore',
                        entityName: entityName,
                        id: $sorted.data('itemid'),
                        positionEntityId: $next.data('itemid')
                    });
                } else {
                    console.error('Something wrong!');
                }
            },
            cursor: "move"
        });
    }
});