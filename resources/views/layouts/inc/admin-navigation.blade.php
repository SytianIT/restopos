<li class="{{ Request::is('admin') ? 'active' : '' }}"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
<li class="{{ Request::is('admin/client') || Request::is('admin/clients/*') ? 'active' : '' }}"><a href="{{ route('admin.clients') }}"><i class="fa fa-black-tie" aria-hidden="true"></i> Clients</a></li>
<li class="dropdown-trigger">
    <a href="javascript:;"><i class="fa fa-cogs"></i> Settings <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
    <ul class="dropdown">
        <li class="{{ Request::is('admin/options') || Request::is('admin/options/*') ? 'active' : '' }}"><a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Options</a></li>
        <li class="{{ Request::is('admin/users') || Request::is('admin/users/*') ? 'active' : '' }}"><a href="{{ route('admin.users') }}"><i class="fa fa-user" aria-hidden="true"></i> Users</a></li>
        <li class="{{ Request::is('admin/roles') || Request::is('admin/roles/*') ? 'active' : '' }}"><a href="{{ route('admin.roles') }}"><i class="fa fa-id-card-o" aria-hidden="true"></i> Roles</a></li>
    </ul>
</li>