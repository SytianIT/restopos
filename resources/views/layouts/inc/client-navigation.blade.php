<li><a href="{{ route('client.dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
<li class="{{ Request::is('client/branches') || Request::is('client/branches/*') ? 'active' : '' }}"><a href="{{ route('client.branches') }}"><i class="fa fa-black-tie" aria-hidden="true"></i> Branches</a></li>
<li class="dropdown-trigger">
	<a href="javascript:;"><i class="fa fa-cutlery"></i> Item <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
	<ul class="dropdown">
		<li class="{{ Request::is('client/items') || Request::is('client/items/*') ? 'active' : '' }}"><a href="{{ route('client.items') }}"><i class="fa fa-cutlery" aria-hidden="true"></i> All Items</a></li>
		<li class="{{ Request::is('client/items/categories') || Request::is('client/items/categories/*') ? 'active' : '' }}"><a href="{{ route('client.items.categories') }}"><i class="fa fa-quote-left" aria-hidden="true"></i> Categories</a></li>
	</ul>
</li>
<li class="dropdown-trigger">
	<a href="javascript:;"><i class="fa fa-th-list"></i> Inventory <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
	<ul class="dropdown">
		<li class="{{ Request::is('client/stock-locations') || Request::is('client/stock-locations/*') ? 'active' : '' }}"><a href="{{ route('client.stock-locations') }}"><i class="fa fa-black-tie" aria-hidden="true"></i> Stock Locations</a></li>
		<li class="{{ Request::is('client/stock-transfer') || Request::is('client/stock-transfer/*') ? 'active' : '' }}"><a href="{{ route('client.stock-transfer') }}"><i class="fa fa-exchange" aria-hidden="true"></i> Stock Transfer</a></li>
		<li class="{{ Request::is('client/suppliers') || Request::is('client/suppliers/*') ? 'active' : '' }}"><a href="{{ route('client.suppliers') }}"><i class="fa fa-truck" aria-hidden="true"></i> Suppliers</a></li>
	</ul>
</li>
<li class="{{ Request::is('client/customers') || Request::is('client/customers/*') ? 'active' : '' }}">
	<a href="{{ route('client.customers') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Customers</a>
</li>
<li class="{{ Request::is('client/employees') || Request::is('client/employees/*') ? 'active' : '' }}">
	<a href="{{ route('client.employees') }}"><i class="fa fa-briefcase" aria-hidden="true"></i> Employees</a>
</li>
<li class="dropdown-trigger">
	<a href="javascript:;"><i class="fa fa-cogs"></i> Settings <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
	<ul class="dropdown">
		<li class="{{ Request::is('client/tables') || Request::is('client/tables/*') ? 'active' : '' }}"><a href="{{ route('client.tables') }}"><i class="fa fa-th" aria-hidden="true"></i> Tables</a></li>
		<li class="{{ Request::is('client/users') || Request::is('client/users/*') ? 'active' : '' }}"><a href="{{ route('client.users') }}"><i class="fa fa-users" aria-hidden="true"></i> Users</a></li>
		<li class="{{ Request::is('client/roles') || Request::is('client/roles/*') ? 'active' : '' }}"><a href="{{ route('client.roles') }}"><i class="fa fa-users" aria-hidden="true"></i> Roles</a></li>
	</ul>
</li>