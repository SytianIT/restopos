<li><a href="{{ route('client.dashboard') }}"><i class="fa fa-home" aria-hidden="true"></i> Home</a></li>
<li class="{{ Request::is('client/branches') || Request::is('client/branches/*') ? 'active' : '' }}"><a href="{{ route('client.branches') }}"><i class="fa fa-black-tie" aria-hidden="true"></i> Branches</a></li>
<li class="dropdown-trigger">
    <a href="javascript:;"><i class="fa fa-cutlery"></i> Menu <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
    <ul class="dropdown">
        <li class="{{ Request::is('client/menus') || Request::is('client/menus/*') ? 'active' : '' }}"><a href="{{ route('client.menus') }}"><i class="fa fa-cutlery" aria-hidden="true"></i> All Menu</a></li>
        <li class="{{ Request::is('client/menus/categories') || Request::is('client/menus/categories/*') ? 'active' : '' }}"><a href="{{ route('client.menus.categories') }}"><i class="fa fa-quote-left" aria-hidden="true"></i> Categories</a></li>
    </ul>
</li>
<li class="dropdown-trigger">
    <a href="javascript:;"><i class="fa fa-shopping-basket"></i> Products <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
    <ul class="dropdown">
        <li class="{{ Request::is('client/products') || Request::is('client/products/*') ? 'active' : '' }}"><a href="{{ route('client.products') }}"><i class="fa fa-shopping-basket" aria-hidden="true"></i> All Products</a></li>
        <li class="{{ Request::is('client/product/categories') || Request::is('client/product/categories/*') ? 'active' : '' }}"><a href="{{ route('client.products.categories') }}"><i class="fa fa-quote-left" aria-hidden="true"></i> Categories</a></li>
    </ul>
</li>
<li class="dropdown-trigger">
    <a href="javascript:;"><i class="fa fa-th-list"></i> Inventory <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
    <ul class="dropdown">
        <li class="{{ Request::is('client/stock-locations') || Request::is('client/stock-locations/*') ? 'active' : '' }}"><a href="{{ route('client.stock-locations') }}"><i class="fa fa-black-tie" aria-hidden="true"></i> Stock Locations</a></li>
        <li class="{{ Request::is('client/suppliers') || Request::is('client/suppliers/*') ? 'active' : '' }}"><a href="{{ route('client.suppliers') }}"><i class="fa fa-truck" aria-hidden="true"></i> Suppliers</a></li>
    </ul>
</li>
<li class="{{ Request::is('client/customers') || Request::is('client/customers/*') ? 'active' : '' }}">
    <a href="{{ route('client.customers') }}"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Customers</a>
</li>
<li class="{{ Request::is('client/employees') || Request::is('client/employees/*') ? 'active' : '' }}">
    <a href="{{ route('client.employees') }}"><i class="fa fa-users" aria-hidden="true"></i> Employees</a>
</li>
<li class="{{ Request::is('client/options') || Request::is('client/options/*') ? 'active' : '' }}">
    <a href="javascript:;"><i class="fa fa-cogs"></i> Settings <i class="fa fa-chevron-down font-size-10 menu-dropdown-arrow"></i></a>
</li>