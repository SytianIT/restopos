<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <meta name="base-url" content="{{ url('/') }}">
    <meta name="description" content="{{ isset($description) ? $description : 'no description' }}">
    <meta name="keywords" content="{{ isset($keywords) ? $keywords : 'no keywords' }}">
    
    <!-- Add-on Meta -->
    @yield('meta')

    <title>@yield('title', (isset($title) && $title != null) ? $title : 'Resto POS')</title>

    <!-- Styles -->
    <link href="{{ asset('css/delight.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    @yield('css')

    <!-- Head Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    @yield('head-scripts')

</head>
<body id="@yield('body-id')" class="@yield('body-class')">
    <div id="loading">
        <div class="svg-icon-loader">
            <img src="{{ asset('/images/svg-loaders/bars.svg') }}" width="40" alt="">
        </div>
    </div>

    {{-- Layout Intended for Guest --}}
    @section('guest')
    @show

    {{-- Layout Intended for Admin --}}
    @if(Auth::user())
    <header class="header">
        <div class="inner clearfix">
            <div class="upper container-fluid ">
                <div class="pane-left float-left">
                    <h4>Restop - Administrator Panel</h4>
                </div>
                <div id="header-nav-left" class="panel-right float-right">
                    <div class="user-account-btn dropdown mrg0T">
                        <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                            <img width="28" src="{{ $authUser->user_picture != null ? asset($authUser->user_picture) : asset('images/gravatar.jpg') }}" alt="Profile image">
                            <span>{{ $authUser->first_name }}</span>
                            <i class="glyph-icon icon-angle-down"></i>
                        </a>
                        <div class="dropdown-menu float-right">
                            <div class="box-sm">
                                <div class="login-box clearfix">
                                    <div class="user-img">
                                        <a href="" title="" class="change-img">Change photo</a>
                                        <img src="{{ $authUser->user_picture != null ? asset($authUser->user_picture) : asset('images/gravatar.jpg') }}" alt="">
                                    </div>
                                    <div class="user-info">
                                        <span>
                                            <a href="">{{ $authUser->name }}</a>
                                            <i>{{ $authUser->job_title }}</i>
                                        </span>
                                        {{-- <a href="#" title="Edit profile">Edit profile</a> --}}
                                        {{-- <a href="#" title="View notifications">View notifications</a> --}}
                                    </div>
                                </div>
                                <div class="button-pane button-pane-alt pad5L pad5R text-center">
                                    <a href="{{ url('logout') }}" class="btn btn-flat display-block font-normal btn-success">
                                        <i class="glyph-icon icon-power-off"></i>
                                        Logout
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="lower">
                <div class="container">
                    <ul id="main-navigation" class="list-inline">
                        @if($authUser->isAdmin())
                            @include('layouts.inc.admin-navigation')
                        @elseif($authUser->isClient())
                            @include('layouts.inc.client-navigation')
                        @elseif($authUser->isBranch())
                            @include('layouts.inc.branch-navigation')
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <main id="@yield('main-id')" class="main">
        <div class="page-headings">
            <div class="container">
                <h1 class="main-title">@yield('main-title', 'Page Heading')</h1>
                <div class="secondary-title">
                    @yield('secondary-title')
                </div>
                <ul class="breadcrumb">
                    <li><a href="{{ url('/') }}"><i class="fa fa-home"></i></a></li>
                    @section('crumbs')
                    @show
                </ul>
            </div>
        </div>
        <div class="main-content">
            <div class="container">
                @section('content')
                @show
            </div>
            @include('layouts.inc.notification')
        </div>
    </main>
    <footer class="footer">
        
    </footer>
    @endif

    <!-- Scripts -->
    @yield('beforeScripts')
    <script src="{{ asset('js/delight.js') }}"></script>
    <script src="{{ asset('js/all.js') }}"></script>
    @yield('scripts')
    
</body>
</html>
