@extends('layouts.app')

@section('css')
<style type="text/css">
    html,body {
        height: 100%;
    }
</style>
@stop

@section('guest')
<img src="{{ asset('images/delight/image-resources/blurred-bg/blurred-bg-3.jpg') }}" class="login-img wow fadeIn">

<div class="center-vertical">
    <div class="center-content">
        <div class="col-md-3 center-margin">
            {!! Form::open(['route' => 'login', 'method' => 'POST', 'id' => 'form-login', 'class' => 'form-validation-true']) !!}
                <div class="content-box wow bounceInDown modal-content">
                    <h3 class="content-box-header content-box-header-alt bg-default">
                        <span class="icon-separator">
                            <i class="glyph-icon icon-cog"></i>
                        </span>
                        <span class="header-wrapper">
                            Members area
                            <small>Login to your account.</small>
                        </span>
                        {{-- <span class="header-buttons">
                            <a href="{{ url('register') }}" class="btn btn-sm btn-primary" title="">Sign Up</a>
                        </span> --}}
                    </h3>
                    <div class="content-box-wrapper">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control {{ $errors->has('cuid') ? 'parsley-error' : '' }}" id="exampleInputCuid" placeholder="Client ID" name="cuid" value="{{ old('cuid') }}">
                                <span class="input-group-addon bg-blue">
                                    <i class="fa fa-id-badge"></i>
                                </span>
                            </div>
                            @if ($errors->has('cuid'))
                                <label class="error">{{ $errors->first('cuid') }}</label class="error">
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control {{ $errors->has('username') ? 'parsley-error' : '' }}" id="exampleInputEmail1" placeholder="Username" name="username" value="{{ old('username') }}">
                                <span class="input-group-addon bg-blue">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>
                            @if ($errors->has('username'))
                            <label class="error">{{ $errors->first('username') }}</label class="error">
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="password" class="form-control {{ $errors->has('password') ? 'parsley-error' : '' }}" id="exampleInputPassword1" name="password" placeholder="Password">
                                <span class="input-group-addon bg-blue">
                                    <i class="glyph-icon icon-unlock-alt"></i>
                                </span>
                            </div>
                            @if ($errors->has('password'))
                            <label class="error">{{ $errors->first('password') }}</label>
                            @endif
                        </div>
                        <div class="form-group">
                            <a href="{{ url('password/reset') }}" title="Recover password">Forgot Your Password?</a>
                        </div>
                        <button class="btn btn-azure btn-block">Sign In</button>
                        @if(session()->has('error'))
                        <label class="error">{{ session('error') }}</label>
                        @endif
                    </div>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>
@stop
