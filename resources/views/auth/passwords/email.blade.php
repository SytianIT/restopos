@extends('layouts.app')

@section('css')
<style type="text/css">
    html,body {
        height: 100%;
    }
</style>
@stop

<!-- Main Content -->
@section('guest')
<img src="{{ asset('images/delight/image-resources/blurred-bg/blurred-bg-3.jpg') }}" class="login-img wow fadeIn">

<div class="center-vertical">
    <div class="center-content">
        <div class="col-md-3 center-margin">
                    <form class="form-horizontal form-validation-true" role="form" method="POST" action="{{ url('/password/email') }}">
                        <div class="content-box wow bounceInDown modal-content">
                            <h3 class="content-box-header content-box-header-alt bg-default">
                                <span class="icon-separator">
                                    <i class="glyph-icon icon-cog"></i>
                                </span>
                                <span class="header-wrapper">
                                    Members Accounts Retrieval Area
                                    <small>&nbsp;</small>
                                </span>
                                <span class="header-buttons">
                                    <a href="{{ url('login') }}" class="btn btn-sm btn-azure" title="">Sign In</a>
                                </span>
                            </h3>
                            <div class="content-box-wrapper">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <div class="input-group">
                                            <input id="email" type="email" class="form-control {{ $errors->has('password') ? 'parsley-error' : '' }}" name="email" value="{{ old('email') }}" placeholder="Enter your email address to receive a password reset link." required>
                                            <span class="input-group-addon bg-azure">
                                                <i class="glyph-icon icon-unlock-alt"></i>
                                            </span>
                                        </div>
                                        @if ($errors->has('email'))
                                        <label class="error">{{ $errors->first('email') }}</label>
                                        @endif
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-azure btn-block">Send Password Reset Link</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    wow = new WOW({
        animateClass: 'animated',
        offset: 100
    });
    wow.init();
</script>
@stop