{!! Form::model($supplier, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Supplier Information</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<label for="company-name">Supplier Name <span class="req">*</span></label>
								<input type="text" name="title" id="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $supplier->title ?: old('title') }}">
								@if($errors->has('title'))
								<label class="error">{{ $errors->first('title') }}</label>
								@endif
							</div>
						</div>
						 <div class="form-group">
                            <div class="col-sm-12">
                                <label for="address-1">Address</label>
                                <input type="text" name="address" id="address-1" class="form-control {{ ($errors->has('address')) ? 'parsley-error' : '' }}" value="{{ $supplier->address ?: old('address') }}">
                            </div>
                        </div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="company-name">Branch <span class="req">*</span></label>
								<select name="branch[]" id="branch" multiple class="selectize {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
									@foreach($branches as $branch)
									<option value="{{ $branch->id }}" {{ $supplier->branches->contains($branch->id) ? 'selected' : '' }}>{{ $branch->title }}</option>
									@endforeach
								</select>
								@if($errors->has('branch'))
								<label class="error">{{ $errors->first('branch') }}</label>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="company-name">Notes </label>
								<textarea name="notes" id="notes" class="form-control" rows="4">{{ $supplier->notes ?: old('notes') }}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Contact Details</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="pc-first-name">First Name <span class="req">*</span></label>
                                <input type="text" name="c_first_name" id="pc-first-name" class="form-control {{ ($errors->has('c_first_name')) ? 'parsley-error' : '' }}"  value="{{ $supplier->c_first_name ?: old('c_first_name') }}">
                                @if ($errors->has('c_first_name'))
                                <label class="error">{{ $errors->first('c_first_name') }}</label>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label for="pc-last-name">Last Name <span class="req">*</span></label>
                                <input type="text" name="c_last_name" id="pc-last-name" class="form-control {{ ($errors->has('c_last_name')) ? 'parsley-error' : '' }}"  value="{{ $supplier->c_last_name ?: old('c_last_name') }}">
                                @if ($errors->has('c_last_name'))
                                <label class="error">{{ $errors->first('c_last_name') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="pc-email">Email</label>
                                <input type="email" name="c_email" id="pc-email" class="form-control {{ ($errors->has('c_email')) ? 'parsley-error' : '' }}"  value="{{ $supplier->c_email ?: old('c_email') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="pc-mobile">Mobile Number</label>
                                <input type="text" name="c_mobile" id="pc-mobile" class="form-control {{ ($errors->has('c_mobile')) ? 'parsley-error' : '' }}"  value="{{ $supplier->c_mobile ?: old('c_mobile') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="pc-telephone">Telephone Number</label>
                                <input type="text" name="c_telephone" id="pc-telephone" class="form-control"  value="{{ $supplier->c_telephone ?: old('c_telephone') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="panel">
			<div class="panel-body">
				<button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}