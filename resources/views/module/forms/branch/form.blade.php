{!! Form::model($branch, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Branch Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Company Name <span class="req">*</span></label>
                                <input type="text" name="title" id="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $branch->title ?: old('title') }}">
                                @if ($errors->has('title'))
                                <label class="error">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="address-1">Address 1 <span class="req">*</span></label>
                                <input type="text" name="address_1" id="address-1" class="form-control {{ ($errors->has('address_1')) ? 'parsley-error' : '' }}" value="{{ $branch->address_1 ?: old('address_1') }}">
                                @if ($errors->has('address_1'))
                                <label class="error">{{ $errors->first('address_1') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="address-2">Address 2</label>
                                <input type="text" name="address_2" id="address-2" class="form-control" value="{{ $branch->address_2 ?: old('address_2') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="city">City <span class="req">*</span></label>
                                <input type="text" name="city" id="city" class="form-control {{ ($errors->has('city')) ? 'parsley-error' : '' }}" value="{{ $branch->city ?: old('city') }}">
                                @if ($errors->has('city'))
                                <label class="error">{{ $errors->first('city') }}</label>
                                @endif
                            </div>
                            <div class="col-sm-5">
                                <label for="country">Country <span class="req">*</span></label>
                                <select name="country" id="country" class="restop-select form-control {{ ($errors->has('country')) ? 'parsley-error' : '' }}">
                                    @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ $branch->getMyCountry($country) }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('country'))
                                <label class="error">{{ $errors->first('country') }}</label>
                                @endif
                            </div>
                            <div class="col-sm-3">
                                <label for="zip">Zip Code <span class="req">*</span></label>
                                <input type="text" name="zip" id="zip" class="form-control {{ ($errors->has('zip')) ? 'parsley-error' : '' }}" value="{{ $branch->zip ?: old('zip') }}">
                                @if ($errors->has('zip'))
                                <label class="error">{{ $errors->first('zip') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <label for="website">Website</label>
                                <input type="text" name="website" id="website" class="form-control" value="{{ $branch->website ?: old('website') }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="mobile">Mobile Number</label>
                                <input type="text" name="mobile" id="mobile" class="form-control" value="{{ $branch->mobile ?: old('mobile') }}">
                            </div>
                            <div class="col-sm-4">
                                <label for="telephone">Telephone Number</label>
                                <input type="text" name="telephone" id="telephone" class="form-control" value="{{ $branch->telephone ?: old('telephone') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Primary Contact Details</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="pc-first-name">First Name <span class="req">*</span></label>
                                <input type="text" name="pc_first_name" id="pc-first-name" class="form-control {{ ($errors->has('pc_first_name')) ? 'parsley-error' : '' }}"  value="{{ $branch->pc_first_name ?: old('pc_first_name') }}">
                                @if ($errors->has('pc_first_name'))
                                <label class="error">{{ $errors->first('pc_first_name') }}</label>
                                @endif
                            </div>
                            <div class="col-sm-6">
                                <label for="pc-last-name">Last Name <span class="req">*</span></label>
                                <input type="text" name="pc_last_name" id="pc-last-name" class="form-control {{ ($errors->has('pc_last_name')) ? 'parsley-error' : '' }}"  value="{{ $branch->pc_last_name ?: old('pc_last_name') }}">
                                @if ($errors->has('pc_last_name'))
                                <label class="error">{{ $errors->first('pc_last_name') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="pc-email">Email <span class="req">*</span></label>
                                <input type="email" name="pc_email" id="pc-email" class="form-control {{ ($errors->has('pc_email')) ? 'parsley-error' : '' }}"  value="{{ $branch->pc_email ?: old('pc_email') }}">
                                @if ($errors->has('pc_email'))
                                <label class="error">{{ $errors->first('pc_email') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="pc-mobile">Mobile Number <span class="req">*</span></label>
                                <input type="text" name="pc_mobile" id="pc-mobile" class="form-control {{ ($errors->has('pc_mobile')) ? 'parsley-error' : '' }}"  value="{{ $branch->pc_mobile ?: old('pc_mobile') }}">
                                @if ($errors->has('pc_mobile'))
                                <label class="error">{{ $errors->first('pc_mobile') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="pc-telephone">Telephone Number</label>
                                <input type="text" name="pc_telephone" id="pc-telephone" class="form-control"  value="{{ $branch->pc_telephone ?: old('pc_telephone') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">Secondary Contact Details</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="sc-first-name">First Name</label>
                                <input type="text" name="sc_first_name" id="sc-first-name" class="form-control"  value="{{ $branch->sc_first_name ?: old('sc_first_name') }}">
                            </div>
                            <div class="col-sm-6">
                                <label for="sc-last-name">Last Name</label>
                                <input type="text" name="sc_last_name" id="sc-last-name" class="form-control" value="{{ $branch->sc_last_name ?: old('sc_last_name') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="sc-email">Email</label>
                                <input type="email" name="sc_email" id="sc-email" class="form-control" value="{{ $branch->sc_email ?: old('sc_email') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="sc-mobile">Mobile Number</label>
                                <input type="text" name="sc_mobile" id="sc-mobile" class="form-control" value="{{ $branch->sc_mobile ?: old('sc_mobile') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="sc-telephone">Telephone Number</label>
                                <input type="text" name="sc_telephone" id="sc-telephone" class="form-control" value="{{ $branch->sc_telephone ?: old('sc_telephone') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Branch Settings</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <label class="">Working Hours</label>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="{{ $errors->has('settings.working_from') ? 'parsley-error' : '' }} input-group">
                                            <label for="paymentTypes" class="input-group-addon">From</label>
                                            {!! Form::text('settings[working_from]', $branch->getSetting('working_from'), ['class' => 'form-control timepicker']) !!}
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="{{ $errors->has('settings.working_to') ? 'parsley-error' : '' }} input-group">
                                            <label for="subscriptionAmount" class="input-group-addon">To</label>
                                            {!! Form::text('settings[working_to]', $branch->getSetting('working_to'), ['class' => 'form-control timepicker']) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="{{$errors->has('settings.pay_types') ? 'has-error' : ''}}">
                                    <label for="paymentTypes">Payment Types Accepted</label>
                                    {!! Form::select('settings[pay_types][]', App\Repository\Etc\PaymentType::$types, $branch->getSetting('pay_types'), ['class' => 'selectize', 'multiple']) !!}
                                </div>
                            </div>
                        </div>
                        @if(!isset($clientLvl))
                        <div class="form-group">
                            <div class="col-sm-6">
                                <label for="subscriptionAmount">Subscription Amount</label>
                                {!! Form::text('settings[subscription_amount]', null, ['class' => 'form-control']) !!}
                            </div>
                            <div class="col-sm-6">
                                <div class="relative {{ $errors->has('subscription_expiration') ? 'has-error' : '' }}">
                                    <label for="expiration">Subscription Expiration</label>
                                    {!! Form::text('subscription_expiration', $branch->subscription_expiration != null ? $branch->subscription_expiration->format('m/y/d h:m:a') : '', ['class' => 'date form-control datetimepkr']) !!}
                                </div>
                            </div>
                        </div>
                        @else
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <label for="subscriptionAmount">Subscription Amount: <span class="font-bold font-size-15 font-blue">@money($branch->getSetting('subscription_amount'))</span></label>
                                </div>
                                <div class="col-sm-6">
                                    <div class="relative {{ $errors->has('subscription_expiration') ? 'has-error' : '' }}">
                                        <label for="expiration">Subscription Expiration <span class="font-bold font-size-15 font-blue">{{ $branch->formatFormDate('subscription_expiration') }}</span></label>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <div class="col-sm-6">
                                <div class="relative {{$errors->has('date_created') ? 'has-error' : ''}}">
                                    <label for="expiration">Date Created</label>
                                    {!! Form::text('date_created', $branch->date_created != null ? $branch->formatFormDate('date_created') : '', ['class' => 'form-control datepicker']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label for="paymentTypes">Allowed IP Address</label>
                                {!! Form::text('settings[allowed_ip]', $branch->getSetting('allowed_ip'), ['class' => 'selectize-create']) !!}
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <div class="col-xs-12">
                                <div class="checkbox">
                                    <label>
                                      {!! Form::checkbox('settings[share_customers]') !!} Can share customers to branches
                                  </label>
                              </div>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="col-xs-12">
                            <label for="notes">Notes</label>
                            {!! Form::textarea('settings[notes]', null, ['class' => 'form-control', 'rows' => 3, 'placeholder' => 'Admin notes']) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="col-sm-3">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="ccontrol-label">Branch Logo</label>
                        <div class="fileinput-new" data-provides="fileinput">
                            <div class="fileinput fileinput-preview thumbnail {{ $branch->company_logo == null ? 'hidden' : '' }}" data-trigger="fileinput"><img src="{{ asset($branch->avatar) }}" class="img-responsive width-reset"></div>
                            <div>
                            <a href="javascript:;" class="btn btn-azure btn-file btn-sm">
                                <span class="fileinput-new">Browse...</span>
                                <input name="image" type="file" id="file-browse">
                                <div class="ripple-wrapper"></div>
                            </a><br>
                            <em class="font-size-10">For better results, please provide an image with a resolution of 50px in height and any in width.</em>
                            @if ($errors->has('image'))
                            <label class="error">{{ $errors->first('image') }}</label>
                            @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if(!isset($clientLvl))
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="restop-checkbox control-label">
                            <input type="checkbox" name="active" {{ $branch->id != null ? (($branch->active) ? 'checked' : '') : 'checked' }}>
                            Activate
                        </label>
                    </div>
                </div>
                @endif
                @if(isset($showButton))
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
                @endif
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}