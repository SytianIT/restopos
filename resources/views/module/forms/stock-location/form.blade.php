{!! Form::model($stockLocation, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Stock Location Information</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-12">
								<label for="company-name">Location Name <span class="req">*</span></label>
								<input type="text" name="location_title" id="location_title" class="form-control {{ ($errors->has('location_title')) ? 'parsley-error' : '' }}" value="{{ $stockLocation->location_title ?: old('location_title') }}">
								@if($errors->has('location_title'))
								<label class="error">{{ $errors->first('location_title') }}</label>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="company-name">Branch <span class="req">*</span></label>
								<select name="branch" id="branch" class="selectize {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
									@foreach($branches as $branch)
									<option value="{{ $branch->id }}" {{ $branch->id == $stockLocation->branch_id ? 'selected' : '' }}>{{ $branch->title }}</option>
									@endforeach
								</select>
								@if($errors->has('branch'))
								<label class="error">{{ $errors->first('branch') }}</label>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="company-name">Remarks</label>
								<textarea name="remarks" id="remarks" cols="30" rows="5" class="form-control">{{ $stockLocation->remarks ?: old('remarks') }}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="panel">
			<div class="panel-body">
				<button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}