{!! Form::model($category, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Category Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Title <span class="req">*</span></label>
                                <input type="text" name="title" id="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $category->title ?: old('title') }}">
                                @if($errors->has('title'))
                                    <label class="error">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Parent Category <span class="req">*</span></label>
                                <select name="parent_id" id="parent_id" class="restop-select selectize {{ ($errors->has('parent_id')) ? 'parsley-error' : '' }}">
                                    @foreach($categories as $itemCategory)
                                        <option value=""></option>
                                        @include('module.forms.item.category-option', [
                                            'category' => $category,
                                            'itemCategory' => $itemCategory
                                        ])
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Branch <span class="req">*</span></label>
                                <select name="branch[]" id="branch" multiple class="selectize {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}" {{ $category->branches->contains($branch->id) ? 'selected' : '' }}>{{ $branch->title }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('branch'))
                                    <label class="error">{{ $errors->first('branch') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Notes </label>
                                <textarea name="notes" id="notes" class="form-control" rows="4">{{ $category->notes ?: old('notes') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="restop-checkbox"><input type="checkbox" id="disable-front-end" name="pos_disabled" {{ $category->pos_disabled ? 'checked' : '' }}> Disable from showing in POS</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}