<div id="add-ons-rows" class="add-ons-rows">
    <div class="add-on-heading row {{ isset($addons) ? ($addons->count() == 0 ? 'hidden' : '') : 'hidden' }}">
        <div class="col-xs-3"><h5 class="font-bold">Add-on Title</h5></div>
        <div class="col-xs-5"><div class="row"><div class="col-xs-6"><h5 class="font-bold">Add to Price (Dine-in)</h5></div><div class="col-xs-6"><h5 class="font-bold">Add to Price (Take-out)</h5></div></div></div>
        <div class="col-xs-1"><h5 class="font-bold">Editable in POS</h5></div>
    </div>
    <div class="add-on-row-inner">
        @if(isset($addons))
        @foreach($addons as $key => $addon)
        <div class="add-on mrg15B row">
            <div class="col-xs-3">
                <input type="text" name="addon[{{ $key }}][title]" class="form-control" value="{{ $addon ? $addon->title : old('addon.'+$key+'.title') }}">
                <input type="hidden" name="addon[{{ $key }}][addon_id]" value="{{ $addon ? $addon->id : old('addon.'+$key+'.addon_id') }}">
            </div>
            <div class="col-xs-5">
                <div class="row">
                    <div class="col-xs-6"><input type="text" name="addon[{{ $key }}][dine_in_pr]" class="form-control" value="{{ $addon ? $addon->dine_in_price : old('addon.'+$key+'.dine_in_pr') }}"></div>
                    <div class="col-xs-6"><input type="text" name="addon[{{ $key }}][take_out_pr]" class="form-control" value="{{ $addon ? $addon->take_out_price : old('addon.'+$key+'.take_out_pr') }}"></div>
                </div>
                <div class="row mrg5T tax-included-price">
                    <div class="col-xs-6"><em class="font-size-10">Tax included price:</em><input type="text" name="addon[{{ $key }}][dine_in_pr_tax_inc]" class="form-control" value="{{ $addon ? $addon->dine_in_price_tax_inc : old('addon.'+$key+'.dine_in_pr_tax_inc') }}"></div>
                    <div class="col-xs-6"><em class="font-size-10">Tax included price:</em><input type="text" name="addon[{{ $key }}][take_out_pr_tax_inc]" class="form-control" value="{{ $addon ? $addon->take_out_price_tax_inc : old('addon.'+$key+'.take_out_pr_tax_inc') }}"></div>
                </div>
            </div>
            <div class="col-xs-1"><label class="restop-checkbox"><input type="checkbox" id="checkbox-editable-id" name="addon[{{ $key }}][editable_pos]" {{ $addon ? ($addon->editable_pos ? 'checked' : '') : old('addon.'.$key.'.editable_pos') }}></label></div>
            <div class="col-xs-2"><a href="javascript:;" class="font-red btn-remove-add-on-row"><i class="fa fa-trash"></i> Delete</a> <a href="javascript:;" class="sorting-icon sorting-hand mrg15L"><i class="fa fa-sort font-size-16" aria-hidden="true"></i> </a></div>
        </div>
        @endforeach
        @endif
    </div>
</div>
<div class="mrg15T">
    <a href="javascript:;" id="create-addon" class="font-blue font-size-15" data-counter="{{ isset($addons) ? ($addons->count() == 0 ? 0 : $addons->count() + 1) : 0 }}">
        <i class="fa fa-plus-circle"></i>&nbsp;Create Add-On
    </a>
</div>