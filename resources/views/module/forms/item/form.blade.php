{!! Form::model($item, ['method' => isset($method) ? $method : 'POST', 'id' => 'form-item-creation', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">General</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="title">Item Title <span class="req">*</span></label>
                                <input type="text" name="title" id="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $item->title ?: old('title') }}">
                                @if($errors->has('title'))
                                    <label class="error">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <div class="col-md-12">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="2" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <label class="restop-checkbox font-size-15">
                                    <input type="checkbox" name="enable_inventory" id="enable-inventory" autocomplete="off" {{ $item->enable_inventory ? 'checked' : '' }}> Track Inventory
                                </label>
                            </div>
                        </div>
                        <div class="form-group inventory-enabled {{ $item->enable_inventory ? '' : 'inventory-hidden' }}">
                            <div class="col-xs-12">
                                <label for="inventory-type">Inventory Type</label>
                                <select name="inventory_type" id="inventory-type" class="form-control">
                                    <option value="simple" {{ $item->inventory_type == App\Repository\Etc\Inventory::SIMPLE ? 'selected' : '' }}>Simple</option>
                                    <option value="composite" {{ $item->inventory_type == App\Repository\Etc\Inventory::COMPOSITE ? 'selected' : '' }}>Composite</option>
                                </select>
                            </div>
                        </div>
                        <div class="content-box tabs">
                            <h3 class="content-box-header bg-blue">
                                <span>&nbsp;</span>
                                <ul>
                                    <li><a href="#tabs-details" title="Tab 1">Details</a></li>
                                    <li><a href="#tabs-example-3" title="Tab 3">Add-Ons</a></li>
                                    <li class="hidden"><a href="#tabs-example-4" title="Tab 3">Discounts</a></li>
                                </ul>
                            </h3>
                            <div id="tabs-details">
                                @include('module.forms.item.form-details')
                            </div>
                            <div id="tabs-example-3">
                                @include('module.forms.item.form-add-ons')
                            </div>
                            <div id="tabs-example-4">
                                Coming Soon!
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="panel panel-default panel-variations" {{ $item->attributes->count() == 0 ? 'style="display: none;"' : '' }}>
                    <div class="panel-heading">Variations</div>
                    <div class="panel-body">
                    @include('module.forms.item.form-variations')
                    </div>
                </div>
            </div>

            <div class="col-sm-12 inventory-enabled has-variations inventory-composite">
                <div class="panel panel-default">
                    <div class="panel-heading">Recipes</div>
                    <div class="panel-body">
                        <div class="related-items-panel">
                            <div class="form-group related-items-heading {{ $item->recipes->count() == 0 ? 'hidden' : '' }}">
                                <div class="col-xs-4"><label class="control-label">Item Title</label></div>
                                <div class="col-xs-1 inventory-enabled inventory-composite"></div>
                                <div class="col-xs-4 inventory-enabled inventory-composite"><label class="control-label">Item Amount</label></div>
                                <div class="col-xs-1"></div>
                            </div>
                            @if(isset($recipes))
                            @foreach($recipes as $key => $recipe)
                                <div class="form-group related-items-row">
                                    <div class="col-xs-4">
                                        <select name="recipes[{{ $key }}][item]" class="related-items form-control">
                                            @foreach($relatedItems as $relatedItem)
                                                <option value="{{ $relatedItem->id }}" {{ $recipe->recipe_item_id == $relatedItem->id ? 'selected' : '' }}>{{ $relatedItem->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-xs-1 text-center inventory-enabled"><i class="fa fa-chevron-right mrg10T"></i></div>
                                    <div class="col-xs-4 inventory-enabled">
                                        <input type="text" name="recipes[{{ $key }}][amount]" class="form-control" value="{{ $recipe ? $recipe->amount : old('recipes.'.$key.'.amount') }}"><input type="hidden" name="recipes[{{ $key }}][recipe_id]" value="{{ $recipe->id }}">
                                    </div><div class="col-xs-1"><a href="javascript:;" class="btn btn-md btn-danger btn-remove-related-item-row"><i class="fa fa-trash"></i></a></div>
                                </div>
                            @endforeach
                            @endif
                        </div>
                        <a href="javascript:;" class="btn btn-primary btn-add-related-items" data-counter="{{ isset($recipes) ? ($recipes->count() == 0 ? 0 : $recipes->count() + 1) : 0 }}"><i class="fa fa-plus"></i>&nbsp;&nbsp;ADD RECIPE</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 has-variations">
                <div class="panel panel-default">
                    <div class="panel-heading">Pricing</div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th width="3%"><label class="restop-checkbox mrg0A"><input type="checkbox" id="enable-branch-all"></label></div></th>
                                <th width="15%">Branch Name</th>
                                <th width="17%" class="inventory-enabled inventory-simple">Re-order Amount</th>
                                <th width="17%" class="inventory-enabled inventory-simple">Beginning Inv.</th>
                                <th width="17%">Dine-In Price</th>
                                <th width="17%">Take-Out Price</th>
                                <th width="12%">Editable on POS</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($branches as $key => $branchItem)
                                @php
                                $pricing = false;
                                $pricing = $item->getThisPricing($branchItem);
                                @endphp
                                <tr class="branch-row bg-gray">
                                    <td><label class="restop-checkbox"><input type="checkbox" id="checkbox-{{ $branchItem->id }}" name="branch[{{ $key }}][enable]" class="enable-branch-row" {{ $pricing ? 'checked' : '' }}></label>
                                        <input type="hidden" name="branch[{{ $key }}][id]" value="{{ $branchItem->id }}">
                                        @if($pricing)
                                            <input type="hidden" name="branch[{{ $key }}][pricing_id]" value="{{ $pricing->id }}">
                                        @endif
                                    </td>
                                    <td>{{ $branchItem->title }}</td>
                                    <td class="inventory-enabled inventory-simple">
                                        <div class="input-group">
                                            <input type="text" name="branch[{{ $key }}][reorder_amnt]" class="form-control input-text" placeholder="Re-order Amount" value="{{ $pricing ? $pricing->reorder_amount : old('branch.'.$key.'.reorder_amnt') }}"><span class="input-group-addon amount-uom"></span>
                                        </div>
                                    </td>
                                    <td class="inventory-enabled inventory-simple">
                                        <div class="input-group">
                                            <input type="text" name="branch[{{ $key }}][beginning_inv]" class="form-control {{ $pricing && $pricing->beginning_amount > 0 ? 'permanently-disabled' : '' }}" placeholder="Beginning Inventory" value="{{ $pricing ? $pricing->beginning_amount : old('branch.'.$key.'.beginning_inv') }}" {{ $pricing && $pricing->beginning_amount > 0 ? 'disabled' : '' }}><span class="input-group-addon amount-uom"></span>
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" name="branch[{{ $key }}][dine_in_pr]" class="form-control input-dine-in-price" placeholder="Dine-In Price" value="{{ $pricing ? $pricing->dine_in_price : old('branch.'.$key.'.dine_in_pr') }}">
                                        <div class="tax-included-price">
                                            <em class="font-size-10">Tax included price:</em>
                                            <input type="text" name="branch[{{ $key }}][dine_in_pr_tax_inc]" class="form-control input-dine-in-price" placeholder="Dine-In Price" value="{{ $pricing ? $pricing->dine_in_price_tax_inc : old('branch.'.$key.'.dine_in_price_tax_inc') }}">
                                        </div>
                                    </td>
                                    <td>
                                        <input type="text" name="branch[{{ $key }}][take_out_pr]" class="form-control input-take-out-price" placeholder="Take-Out Price" value="{{ $pricing ? $pricing->take_out_price : old('branch.'.$key.'.take_out_pr') }}">
                                        <div class="tax-included-price">
                                            <em class="font-size-10">Tax included price:</em>
                                            <input type="text" name="branch[{{ $key }}][take_out_pr_tax_inc]" class="form-control input-take-out-price" placeholder="Take-Out Price" value="{{ $pricing ? $pricing->take_out_price_tax_inc : old('branch.'.$key.'.take_out_pr_tax_inc') }}">
                                        </div>
                                    </td>
                                    <td><label class="restop-checkbox"><input type="checkbox" id="checkbox-editable-{{ $branchItem->id }}" name="branch[{{ $key }}][editable_pos]" {{ $pricing ? ($pricing->editable_pos ? 'checked' : '') : old('branch.'.$key.'.editable_pos') }}></label></div></td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="7">No branch was created, please create branches first as the creation of items will not proceed.</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="ccontrol-label">Item Image</label>
                        <div class="fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail {{ $item->image == null ? 'hidden' : '' }}" data-trigger="fileinput" style=""><img src="{{ $item->avatar }}" class="img-responsive"></div>
                            <div>
                                <a href="javascript:;" class="btn btn-azure btn-file btn-sm">
                                    <span class="fileinput-new">Browse...</span>
                                    <input name="image" type="file" id="file-browse">
                                    <div class="ripple-wrapper"></div>
                                </a><br>
                                <em class="font-size-10">For better results, please provide an image with a resolution of 1280 x 720.</em>
                                @if ($errors->has('image'))
                                    <label class="error">{{ $errors->first('image') }}</label>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label class="restop-checkbox"><input type="checkbox" id="checkbox-enable-item" name="active" {{ $item->exists ? 'checked' : ($item->active ? 'checked' : '') }}> Active</label>
                    </div>
                </div>
                <button id="btn-submit-item" type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}