<div class="form-group">
    <div class="col-md-6">
        <div class="form-group">
            <div class="col-md-12">
                <label for="company-name">Category <span class="req">*</span></label>
                <select name="category_id" id="category_id" class="form-control {{ ($errors->has('category_id')) ? 'parsley-error' : '' }}">
                    @foreach($categories as $category)
                        @include('module.forms.item.category-option', [
                            'itemUsed' => true,
                            'category' => $item->category,
                            'itemCategory' => $category
                        ])
                    @endforeach
                </select>
                @if($errors->has('category_id'))
                    <label class="error">{{ $errors->first('category_id') }}</label>
                @endif
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label for="uom_id">Unit of Measurement </label>
                <select name="uom_id" id="uom_id" class="form-control {{ ($errors->has('uom_id')) ? 'parsley-error' : '' }}">
                    @foreach($uoms as $uom)
                        <option value="{{ $uom->id }}" data-uom="{{ $uom->title }}" {{ $item->uom_id == $uom->id ? 'selected' : '' }}>{{ $uom->long_title }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <label for="tax-code">Tax Code <em class="font-red">( Coming Soon )</em></label>
                <select name="tax_code" id="tax-code" class="form-control {{ ($errors->has('tax_code')) ? 'parsley-error' : '' }}">
                    <option value="default">Default</option>
                    <option value="test">Test</option>
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <div class="col-xs-12">
                <label for="attributes">Attributes <em class="font-size-10 font-orange">( To generate variations click the generate button )</em></label>
                <div class="input-group">
                    <select name="item_attributes[]" id="attributes" multiple class="selectize {{ ($errors->has('attributes')) ? 'parsley-error' : '' }}">
                        @foreach($attributes as $attribute)
                            <option value="{{ $attribute->id }}" {{ $item->attributes->contains($attribute->id) ? 'selected' : '' }}>{{ $attribute->title }}</option>
                        @endforeach
                    </select>
                    <div class="input-group-btn">
                        <a href="javascript:;" class="btn btn-warning btn-generate-variations">GENERATE</a>
                    </div>
                </div>

            </div>
        </div>
        <div class="form-group has-variations {{ $item->attributes->count() > 0 ? '' : 'variation-hidden' }}">
            <div class="col-xs-12">
                <label for="cost">Service Cost</label>
                <input type="number" name="cost" id="cost" min="1" class="form-control input-has-variations {{ ($errors->has('cost')) ? 'parsley-error' : '' }}" value="{{ $item->cost ?: old('cost') }}">
                @if($errors->has('cost'))
                    <label class="error">{{ $errors->first('cost') }}</label>
                @endif
            </div>
        </div>
    </div>
</div>