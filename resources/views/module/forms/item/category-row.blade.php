<tr>
    <td><a href="{{ route($routesPrefix.'edit', $wildCards) }}">{{ $category->depth_name }}</a></td>
    <td>{{ $category->notes }}</td>
    <td>{{ $category->branches->implode('title', ', ') }}</td>
    <td></td>
    <td class="text-center">
        <a href="{{ route($routesPrefix.'edit', $wildCards) }}" class="btn btn-xs btn-success">EDIT</a>
        <a href="{{ route($routesPrefix.'delete', $wildCards) }}" class="btn btn-xs btn-danger">DELETE</a>
    </td>
</tr>

@if($category->children()->count() > 0)
    @foreach ($category->children() as $child)
        <?php
        $category = $child;
        if(isset($hasClient)){
            $wildCards = [$client->id, $category->id];
        } else {
            $wildCards = $category->id;
        }
        ?>
        @include('module.forms.item.category-row', [
            'category' => $category,
            'routesPrefix' => $routesPrefix,
            'wildCards' => $wildCards
        ])
    @endforeach
@endif