 <tr>
    <td><p class="font-bold title-option-{{ $option->id }}">{{ isset($fromVariation) ? $variation->title : $option->title }}</p></td>
    <td>
        <div class="related-items-panel">
            <div class="form-group related-items-heading hidden">
                <div class="col-xs-4"><label class="control-label">Item Title</label></div>
                <div class="col-xs-1 inventory-enabled"></div>
                <div class="col-xs-4 inventory-enabled"><label class="control-label">Item Amount</label></div>
                <div class="col-xs-1"></div>
            </div>
        </div>
        @if(isset($fromVariation))
        @php
        $recipes = $variation->recipes->sortBy('created_at');
        @endphp
        @foreach($recipes as $key => $recipe)
            <div class="form-group related-items-row">
                <div class="col-xs-4">
                    <select name="recipes[{{ $option->id }}][{{ $key }}][item]" class="related-items form-control">
                        @foreach($relatedItems as $relatedItem)
                            <option value="{{ $relatedItem->id }}" {{ $recipe->recipe_item_id == $relatedItem->id ? 'selected' : '' }}>{{ $relatedItem->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xs-1 text-center inventory-enabled"><i class="fa fa-chevron-right mrg10T"></i></div>
                <div class="col-xs-4 inventory-enabled">
                    <input type="text" name="recipes[{{ $option->id }}][{{ $key }}][amount]" class="form-control" value="{{ $recipe ? $recipe->amount : old('recipes.'.$option->id.'.'.$key.'.amount') }}">
                    <input type="hidden" name="recipes[{{ $key }}][recipe_id]" value="{{ $recipe->id }}">
                </div>
                <div class="col-xs-1"><a href="javascript:;" class="btn btn-md btn-danger btn-remove-related-item-row"><i class="fa fa-trash"></i></a></div>
            </div>
        @endforeach
        @endif
        <a href="javascript:;" class="btn btn-primary btn-add-related-items for-variation-recipes" data-id="{{ $option->id }}" data-counter="{{ isset($fromVariation) ? ($recipes->count() == 0 ? 0 : $recipes->count() + 1) : 0 }}"><i class="fa fa-plus"></i>&nbsp;&nbsp;ADD RECIPE</a>
    </td>
</tr>