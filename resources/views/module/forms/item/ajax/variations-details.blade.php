<tr>
    <td width="70%">
        <input type="text" name="variations[{{ $attribute->id }}][options][{{ $option->id }}][title]" class="form-control variation-title" data-class=".title-option-{{ $option->id }}" value="{{ (isset($fromVariation)) ? $variation->title :  '- '.$optItem->title }}">
        @if(isset($fromVariation))
            <input type="hidden" name="variations[{{ $attribute->id }}][options][{{ $option->id }}][variation_id]" value="{{ $variation->id }}">
        @else
            <input type="hidden" name="variations[{{ $attribute->id }}][options][{{ $optItem->id }}][id]" value="{{ $optItem->id }}">
        @endif
    </td>
    <td width="30%">
        <input type="text" name="variations[{{ $attribute->id }}][options][{{ $option->id }}][cost]" class="form-control" value="{{ (isset($fromVariation)) ? $variation->cost : '' }}">
    </td>
</tr>