<tr class="variation-pricing-row">
    <td class="text-center">
        <label class="restop-checkbox"><input type="checkbox" id="checkbox-variation-1" name="pricing[{{ $option->id }}][enable]" class="variation-enabler-row" {{ isset($fromVariation) && $variation->enabled_variation ? 'checked' : '' }}></label>
    </td>
    <td><p class="font-bold title-option-{{ $option->id }}">{{ isset($fromVariation) ? $variation->title : $option->title }}</p><input type="hidden" name="pricing[{{ $option->id }}][option]"></td>
    <td class="branch-listing disabled">
        <a href="javascript:;" class="btn btn-yellow btn-xs btn-copy-branches-pricing tooltip-button" title="Copy this branches pricing to all variation rows" data-toggle="tooltip" data-placement="top"><i class="fa fa-clone" aria-hidden="true"></i></a>
        <table class="table table-hover">
            <thead>
            <tr>
                <th width="3%"><label class="restop-checkbox"><input type="checkbox" id="enable-branch-all"></label></th>
                <th width="15%">Branch Name</th>
                <th width="17%" class="inventory-enabled inventory-simple">Beginning Inv.</th>
                <th width="17%" class="inventory-enabled inventory-simple">Re-order Amount</th>
                <th width="17%">Dine-In Price</th>
                <th width="17%">Take-Out Price</th>
                <th width="12%">Editable on POS</th>
            </tr>
            </thead>
            <tbody>
            @forelse($branches as $branchKey => $branchItem)
                @php
                    $pricing = false;
                    if(isset($variation)){
                        $pricing = $variation->getThisPricing($branchItem);
                    }
                @endphp
                <tr class="branch-row bg-gray">
                    <td><label class="restop-checkbox"><input type="checkbox" id="checkbox-{{ $branchItem->id }}" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][enable]" class="enable-branch-row" {{ $pricing ? 'checked' : '' }}></label>
                        <input type="hidden" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][id]" value="{{ $branchItem->id }}">
                        @if($pricing)
                            <input type="hidden" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][pricing_id]" value="{{ $pricing->id }}">
                        @endif
                    </td>
                    <td>{{ $branchItem->title }}</td>
                    <td class="inventory-enabled inventory-simple">
                        <div class="input-group">
                            <input type="text" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][reorder_amnt]" class="form-control" value="{{ $pricing ? $pricing->reorder_amount : old('pricing.'.$option->id.'.branches.'.$branchItem->id.'.reorder_amnt') }}"><span class="input-group-addon amount-uom"></span>
                        </div>
                    </td>
                    <td class="inventory-enabled inventory-simple">
                        <div class="input-group">
                            <input type="text" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][beginning_inv]" class="form-control" value="{{ $pricing ? $pricing->beginning_amount : old('pricing.'.$option->id.'.branches.'.$branchItem->id.'.beginning_inv') }}"><span class="input-group-addon amount-uom"></span>
                        </div>
                    </td>
                    <td>
                        <input type="text" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][dine_in_pr]" class="form-control input-dine-in-price" value="{{ $pricing ? $pricing->dine_in_price : old('pricing.'.$option->id.'.branches.'.$branchItem->id.'.dine_in_pr') }}">
                        <div class="tax-included-price">
                            <em class="font-size-10">Tax included price:</em>
                            <input type="text" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][dine_in_pr_tax_inc]" class="form-control input-dine-in-price" value="{{ $pricing ? $pricing->dine_in_price_tax_inc : old('pricing.'.$option->id.'.branches.'.$branchItem->id.'.dine_in_price_tax_inc') }}">
                        </div>
                    </td>
                    <td>
                        <input type="text" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][take_out_pr]" class="form-control input-take-out-price" value="{{ $pricing ? $pricing->take_out_price : old('pricing.'.$option->id.'.branches.'.$branchItem->id.'.take_out_pr') }}">
                        <div class="tax-included-price">
                            <em class="font-size-10">Tax included price:</em>
                            <input type="text" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][take_out_pr_tax_inc]" class="form-control input-take-out-price" value="{{ $pricing ? $pricing->take_out_price_tax_inc : old('pricing.'.$option->id.'.branches.'.$branchItem->id.'.take_out_pr_tax_inc') }}">
                        </div>
                    </td>
                    <td><label class="restop-checkbox"><input type="checkbox" id="checkbox-editable-{{ $branchItem->id }}" name="pricing[{{ $option->id }}][branches][{{ $branchItem->id }}][editable_pos]" {{ $pricing ? ($pricing->editable_pos ? 'checked' : '') : old('pricing.'.$option->id.'.branches.'.$branchItem->id.'.editable_pos') }}></label></div></td>
                </tr>
            @empty
                <tr>
                    <td colspan=""></td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </td>
</tr>