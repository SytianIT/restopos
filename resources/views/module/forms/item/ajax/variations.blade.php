<div class="variations-box-wrapper">
    <div class="content-box tabs">
        <h5 class="content-box-header bg-blue">
            <span>&nbsp;</span>
            <ul>
                <li><a href="#detail-tabs-1" title="Tab 1">Details</a></li>
                <li><a href="#sell-price-tabs-2" title="Tab 2">Sell Price</a></li>
                <li class="inventory-enabled inventory-composite"><a href="#recipes-tabs-3" title="Tab 3">Recipes</a></li>
            </ul>
        </h5>
        <div id="detail-tabs-1">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th width="5%" class="text-center"></th>
                    <th width="15%">Option</th>
                    <th width="56%">Display Name</th>
                    <th width="24%">Service Cost</th>
                </tr>
                </thead>
                <tbody>
                @foreach($attributes as $key => $attribute)
                <tr>
                    <td class="text-center"><label class="restop-checkbox"><input type="checkbox" id="checkbox-branch-1" name="variations[{{ $attribute->id }}][enable]" checked></label></td>
                    <td><p class="font-bold">{{ $attribute->title }}</p></td>
                    <td colspan="2">
                        <table class="table">
                            <tbody>
                            @php
                                $attrOpt = $attribute->options->keyBy('id');
                            @endphp
                            @if(isset($item))
                            @if($item->variations->count() > 0)
                                @foreach($item->variations as $variation)
                                @php
                                $option = $variation->getThisOption($attrOpt);
                                @endphp
                                @if($option)
                                @php $attrOpt = $attrOpt->forget($option->id); @endphp
                                @include('module.forms.item.ajax.variations-details', [
                                    'attribute' => $attribute,
                                    'option' => $option,
                                    'variation' => $variation,
                                    'fromVariation' => true // To know if loop is in variations
                                ])
                                @endif
                                @endforeach
                            @endif
                            @endif
                            @foreach($attrOpt as $optItem)
                            @include('module.forms.item.ajax.variations-details', [
                                'attribute' => $attribute,
                                'option' => $optItem
                            ])
                            @endforeach
                            </tbody>
                        </table>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="sell-price-tabs-2">
            <table class="table table-variation-branches">
                <thead>
                <tr>
                    <th width="5%" class="text-center"></th>
                    <th width="10%">Option</th>
                    <th width="85%">Display Name</th>
                </tr>
                </thead>
                <tbody>
                @php
                $pricingOpts = clone $options;
                @endphp
                @if(isset($item))
                    @if($item->variations->count() > 0)
                        @foreach($item->variations as $variation)
                            @php
                                $pricingOpt = $variation->getThisOption($pricingOpts);
                                $pricingOpts->forget($pricingOpt->id);
                            @endphp
                            @include('module.forms.item.ajax.variations-pricing', [
                                'option' => $pricingOpt,
                                'variation' => $variation,
                                'fromVariation' => true // To know if loop is in variations
                            ])
                        @endforeach
                    @endif
                @endif
                @foreach($pricingOpts as $key => $option)
                    @include('module.forms.item.ajax.variations-pricing', [
                        'option' => $option,
                    ])
                @endforeach
                </tbody>
            </table>
        </div>
        <div id="recipes-tabs-3">
            <table class="table inventory-composite">
                <thead>
                <tr>
                    <th width="10%">Option</th>
                    <th width="85%">Recipes</th>
                </tr>
                </thead>
                <tbody>
                @php
                    $recipesOpts = $options;
                @endphp
                @if(isset($item))
                    @if($item->variations->count() > 0)
                        @foreach($item->variations as $variation)
                            @php
                                $recipesOpt = $variation->getThisOption($recipesOpts);
                                $recipesOpts->forget($recipesOpt->id);
                            @endphp
                            @include('module.forms.item.ajax.variations-recipes', [
                                'option' => $recipesOpt,
                                'relatedItems' => $relatedItems,
                                'variation' => $variation,
                                'fromVariation' => true // To know if loop is in variations
                            ])
                        @endforeach
                    @endif
                @endif
                @foreach($recipesOpts as $recipesOpt)
                    @include('module.forms.item.ajax.variations-recipes', [
                        'option' => $recipesOpt,
                        'relatedItems' => $relatedItems
                    ])
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>