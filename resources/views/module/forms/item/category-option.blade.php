@php
    $flag = '';
    if(isset($itemUsed)){
        $flag = ($category != null && $category->id == $itemCategory->id) ? 'selected' : '';
    } else if($category->parent_id == $itemCategory->id){
        $flag = 'selected';
    }
@endphp

<option value="{{ $itemCategory->id }}" {{ $flag }}>{{ $itemCategory->depth_name }}</option>

@if($itemCategory->children()->count() > 0)
    @foreach ($itemCategory->children() as $child)
        @php
            $data = [
                'category' => $category,
                'itemCategory' => $child
            ];

            if(isset($itemUsed)){
                $data = array_merge($data, ['itemUsed' => true, 'category' => $item->category]);
            }
        @endphp
        @include('module.forms.item.category-option', $data)
    @endforeach
@endif