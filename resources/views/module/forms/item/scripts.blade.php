<script type="text/javascript" src="{{ asset('vendor/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $(document).ready(function(){
        var $input = $('.datetimepkr'),
            initVal = $input.val();
        $input.datetimepicker({
            defaultDate: initVal != null ? initVal : new Date()
        });

        $(".tabs").tabs();

        // Script to detect changes in UOM
        function getUomChanges($this){
            var element = $this != null ? $this : $('#uom_id'),
                code = element.find('option:selected').data('uom');

            $('.amount-uom').text(code);
        }
        getUomChanges(null);

        $('body').on('change', '#uom_id', function(){
            getUomChanges($(this));
        });

        // Script for variation enabler
        function toggleTableClass($this){
            var wrapper = $this.closest('.variation-pricing-row'),
                flag = $this.is(':checked'),
                listing = wrapper.find('.branch-listing');

            if(flag){
                listing.removeClass('disabled');
            } else {
                listing.addClass('disabled');
            }
        }

        function detectVariationEnabled(){
            $('.variation-enabler-row').each(function(){
                toggleTableClass($(this));
            });
        }
        detectVariationEnabled();

        $('body').on('change', '.variation-enabler-row', function(){
            toggleTableClass($(this));
        });

        // Branch enabler
        $("body").on('click', '#enable-branch-all', function () {
            var $this = $(this),
                $brRow = $this.closest('table').find('.branch-row'),
                $checker = $brRow.find('.branch-enabler-checkbox .checker'),
                $span  = $checker.find('span'),
                isChecked = !!$this.is(':checked');

            if(isChecked){
                $brRow.addClass('enable');
//                $checker.addClass('focus');
//                $span.addClass('checked');
                $brRow.find('.enable-branch-row').prop('checked', true);
            } else {
                $brRow.removeClass('enable');
//                $checker.removeClass('focus');
//                $span.removeClass('checked');
                $brRow.find('.enable-branch-row').prop('checked', false);
            }
            detectBranchEnabled();
        })

        @if(!isset($editPage))
        // Script for Duplicator of Dine-in Price
        $('body').on('keyup', '.input-dine-in-price', function(){
            var $this = $(this),
                wrapper = $this.closest('.branch-row'),
                val = $this.val();

            wrapper.find('.input-take-out-price').val(val);
        })
        @endif

        // Script for detecting disabled branch pricing
        function toggleInputValues(action, wrapper){
            if(action == 'remove'){
                wrapper.find('input.form-control, input[type="checkbox"], select').not('.enable-branch-row, .permanently-disabled').removeAttr('disabled');
            } else if(action == 'add'){
                wrapper.find('input.form-control, input[type="checkbox"], select').not('.enable-branch-row').attr('disabled', true);
                // wrapper.find('input.form-control, input.custom-checkbox, select').val('');
                if(wrapper.find('.checkbox div.checker > span').hasClass('checked')){
                    wrapper.find('.checkbox div.checker').removeClass('focus');
                    wrapper.find('.checkbox div.checker > span').removeClass('checked');
                }
            }
        }

        function detectBranchEnabled(){
            $('.branch-enabler-checkbox').each(function(){
                var $this = $(this),
                    parentWrapper = $this.closest('.branch-row'),
                    enabler = $this.find('.enable-branch-row');

                if(enabler.is(':checked')){
                    toggleInputValues('remove', parentWrapper);
                } else {
                    toggleInputValues('add', parentWrapper);
                }
            })
        }
        detectBranchEnabled();

        $('body').on('change', '.enable-branch-row', function () {
            var $this = $(this),
                parentWrapper = $this.closest('.branch-row'),
                flag =  $this.is(':checked');

            if(flag){
                toggleInputValues('remove', parentWrapper);
            } else {
                toggleInputValues('add', parentWrapper);
            }
        })

        // Script for displaying Tax Include Price Inputs
        function toggleTaxIncludedFields($this){
            var $this = $this != null ? $this : $('#tax-code'),
                val = $this.val(),
                taxElements = $('.tax-included-price');

            if(val != 'default'){
                taxElements.removeClass('hidden');
            } else {
                taxElements.addClass('hidden');
            }
        }
        toggleTaxIncludedFields(null);
        $('#tax-code').on('change', function(){
            toggleTaxIncludedFields($(this));
        });

        // Script for detect changes in attributes input
        function toggleAttributesFields($this){
            var $this = $this != null ? $this : $('#attributes'),
                variationsWrapper = $('#variations'),
                val = $this.val(),
                hasVarClass = $('.has-variations'),
                hiddenClass = 'attributes-hidden'
                panelVariation = $('.panel-variations');

            if(val != '' && val != undefined &&val != null){
                hasVarClass.addClass(hiddenClass);
            } else {
                variationsWrapper.empty();
                hasVarClass.removeClass(hiddenClass);
                panelVariation.hide();
            }

            return true;
        }
        toggleAttributesFields(null);
        $('#attributes').on('change', function(){
            toggleAttributesFields($(this));
        });

        // Item Inventory Type Toggle
        function toggleInventoryTypeFields($this){
            var $this = ($this != null) ? $this : $('#inventory-type'),
                val = $this.val(),
                simpleClass = $('.inventory-simple'),
                compositeClass = $('.inventory-composite'),
                compoHiddenClass = 'composite-hidden',
                simpHiddenClass = 'simple-hidden';

            if(val == 'simple'){
                simpleClass.removeClass(simpHiddenClass);
                compositeClass.addClass(compoHiddenClass);
            } else if(val == 'composite'){
                compositeClass.removeClass(compoHiddenClass);
                simpleClass.addClass(simpHiddenClass);
            }

            return true;
        }
        toggleInventoryTypeFields(null);
        $('#inventory-type').on('change', function(){
            toggleInventoryTypeFields($(this));
        });

        // Item Inventory Tracking Toggle
        function toggleInventoryTrackingFields($this){
            var $this = ($this != null) ? $this : $('#enable-inventory'),
                proped = $this.is(':checked'),
                hiddenClass = 'inventory-hidden';

            if(proped){
                $('.inventory-enabled').removeClass(hiddenClass);
            } else {
                $('.inventory-enabled').addClass(hiddenClass);
            }

            return true;
        }
        toggleInventoryTrackingFields(null);
        $('#enable-inventory').on('change', function(){
            toggleInventoryTrackingFields($(this));
        });

        // Script for adding a related item
        $('body').on('click', '.btn-add-related-items', function () {
            var invTrack = $('#enable-inventory').is(':checked'),
                $this = $(this),
                panel = $this.siblings('.related-items-panel'),
                heading = panel.find('.related-items-heading'),
                count = parseInt($this.data('counter')),
                newCount = count + 1;

            if($this.hasClass('for-variation-recipes')){
                var dataId = $this.data('id'),
                    selectName = 'recipes['+dataId+']['+newCount+'][item]',
                    inputName = 'recipes['+dataId+']['+newCount+'][amount]';
            } else {
                var selectName = 'recipes['+newCount+'][item]',
                    inputName = 'recipes['+newCount+'][amount]';
            }
            var rowHtml = '<div class="form-group related-items-row">'
                             +'<div class="col-xs-4">'
                                 +'<select name="'+selectName+'" class="related-items form-control">'
                                     @foreach($relatedItems as $relateditem)
                                     +'<option value="{{ $relateditem->id }}">{{ $relateditem->title }}</option>'
                                     @endforeach
                                 +'</select>'
                             +'</div>'
                         +'<div class="col-xs-1 text-center inventory-enabled" '+ (!invTrack ? 'style="display:none"' : '') +'><i class="fa fa-chevron-right mrg10T"></i></div>'
                         +'<div class="col-xs-4 inventory-enabled" '+ (!invTrack ? 'style="display:none"' : '') +'>'
                         +'<input type="text" name="'+inputName+'" class="form-control">'
                         +'</div><div class="col-xs-1"><a href="javascript:;" class="btn btn-md btn-danger btn-remove-related-item-row"><i class="fa fa-trash"></i></a></div></div>';

            if(heading.hasClass('hidden')){
                heading.removeClass('hidden');
            }

            panel.append(rowHtml);
            $this.data('counter', newCount);
        })

        $('body').on('click', '.btn-remove-related-item-row', function(){
            var $this = $(this),
                panel = $this.closest('.related-items-panel'),
                heading = panel.find('.related-items-heading'),
                wrapper = $this.closest('.related-items-row');

            wrapper.fadeOut(function(){
                $(this).remove();
                var count = panel.find('.related-items-row').length;
                if(count < 1){
                    heading.addClass('hidden');
                }
            });
        });

        // Script for Generating of Variations
        function generateVariations($this){
            var $this = $this,
                attributes = $('#attributes').val(),
                wrapperId = $('#variations'),
                panelVar = $('.panel-variations'),
                flag = true;

            if(attributes == null){
                flag = false;
                swal('Oops...','We cannot generate variations, because you do not select any attributes!','error');
            }

            if(flag){
                $.ajax({
                    url : '{{ $variationsUrl }}', // PHP Route to get variation generator URL
                    type : 'GET',
                    dataType : 'HTML',
                    data : {
                        item_attributes : attributes,
                    },
                    beforeSend : function (response){
                        panelVar.stop().fadeIn();
                        wrapperId.hide();
                        panelVar.find('.variation-creating').stop().fadeIn();
                    },
                    error : function (response){
                        swal('Oops...','Something went wrong!','error');
                    },
                    success : function (response){
                        panelVar.find('.variation-creating').fadeOut(function () {
                            wrapperId.fadeIn();
                            wrapperId.html(response);
                            wrapperId.find(".tabs").tabs();
                            restopCheckbox();
                            wrapperId.find('[data-toggle="tooltip"]').tooltip();
                            toggleInventoryTrackingFields(null);
                            toggleInventoryTypeFields(null);
                            toggleTaxIncludedFields(null);
                            detectVariationEnabled();
                            detectBranchEnabled();
                            getUomChanges(null);
                        });
                    }
                });
            }
        }
        @if(isset($item) && $item->attributes->count() > 0)
        generateVariations(null);
        @endif

        $('body').on('click', '.btn-generate-variations', function () {
            generateVariations(null);
        });

        // Script to detect Variation title changes and apply to all
        $('body').on('change', '.variation-title',function(){
            var $this = $(this),
                val = $this.val(),
                className = $this.data('class');

            $(className).text(val);
        });

        // Script for Creating New Add-on Row
        function makeSortable($this){
            var $this = $this != null ? $this : $('#add-ons-rows').find('.add-on-row-inner');
            $this.sortable({
                cursor: "move",
                update: function( event, ui ) {}
            });
        }
        makeSortable(null);

        $('#create-addon').on('click', function(){
            var $this = $(this),
                rowWrapper = $('#add-ons-rows'),
                rowInner = rowWrapper.find('.add-on-row-inner'),
                rowHeading = rowWrapper.find('.add-on-heading'),
                rowCount = parseInt($this.data('counter')),
                newCount = rowCount + 1,
                rowHtml = '<div class="add-on mrg15B row">'
                        +'<div class="col-xs-3">'
                        +'<input type="text" name="addon['+rowCount+'][title]" class="form-control">'
                        +'</div>'
                        +'<div class="col-xs-5">'
                        +'<div class="row">'
                        +'<div class="col-xs-6"><input type="text" name="addon['+rowCount+'][dine_in_pr]" class="form-control"></div>'
                        +'<div class="col-xs-6"><input type="text" name="addon['+rowCount+'][take_out_pr]" class="form-control"></div>'
                        +'</div>'
                        +'<div class="row mrg5T tax-included-price">'
                        +'<div class="col-xs-6"><em class="font-size-10">Tax included price:</em><input type="text" name="addon['+rowCount+'][dine_in_pr_tax_inc]" class="form-control"></div>'
                        +'<div class="col-xs-6"><em class="font-size-10">Tax included price:</em><input type="text" name="addon['+rowCount+'][take_out_pr_tax_inc]" class="form-control"></div>'
                        +'</div>'
                        +'</div>'
                        +'<div class="col-xs-1"><label class="restop-checkbox"><input type="checkbox" id="checkbox-editable-id" name="addon['+rowCount+'][editable_pos]" class="custom-checkbox"></label></div>'
                        +'<div class="col-xs-2"><a href="javascript:;" class="font-red btn-remove-add-on-row"><i class="fa fa-trash"></i> Delete</a> <a href="javascript:;" class="sorting-icon sorting-hand mrg15L"><i class="fa fa-sort font-size-16" aria-hidden="true"></i> </a></div>'
                        +'</div>';

            rowHeading.removeClass('hidden');
            rowInner.append(rowHtml);
            restopCheckbox();
            toggleTaxIncludedFields(null);
            $this.data('counter', newCount);
            makeSortable(rowInner);
        });

        function updateRowPosition(){
            var rowWrapper = $('#add-ons-rows'),
                rowInner = rowWrapper.find('.add-on-row-inner');

            rowInner.find('.add-on').each(function(i){
                var row = $(this);

                row.find('input').each(function(){
                    var s = $(this).attr('name'),
                        index = 6;
                    s = s.substr(0, index) + i + s.substr(index + 1);
                    $(this).attr('name', s);
                })
            });
        }

        $('#add-ons-rows .add-on-row-inner').on( "sortupdate", function( event, ui ) {
            console.log(ui.item.index());
            updateRowPosition();
        } );

        $('body').on('click', '.btn-remove-add-on-row', function(){
            var $this = $(this),
                rowWrapper = $('#add-ons-rows'),
                rowHeading = rowWrapper.find('.add-on-heading'),
                row = $this.closest('.add-on');

            row.fadeOut(function(){
                $(this).remove();
                var rowCount = rowWrapper.find('.add-on').length;
                if(rowCount == 0){
                    rowHeading.addClass('hidden');
                }
            });
        });

        // Script to detect first the Field requires for Item Form
        $('body').on('click', '#btn-submit-item', function (e) {
            e.preventDefault();
            var $this = $(this),
                flag = true,
                variationFlag = true,
                inTitle = $('#title'),
                inCategory = $('#category_id'),
                inAttributes = $('#attributes'),
                variationsWrapper = $('#variations');

            var title = inTitle.val();
            if(title == '' || title == null){
                flag = false;
                inTitle.siblings('label.error').remove();
                inTitle.after('<label class="error">Title is required.</label>');
            }

            var category = inCategory.find('option:selected').length;
            if(category < 1){
                flag = false;
                inCategory.siblings('label.error').remove();
                inCategory.after('<label class="error">Category is required.</label>');
            }

            if(inAttributes.find('option:selected').length > 0){
                if($(variationsWrapper).find('.variations-box-wrapper').length < 1){
                    variationFlag = false;
                }
            }

            if(flag){
                if(variationFlag == false){
                    swal({
                        title: 'Are you sure?',
                        text: "You selected an attribute but did not generate any variations!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: "Yes, Continue!",
                        confirmButtonColor: "#FF5722",
                        closeOnConfirm: false
                    }).then(function(isConfirm){
                        if(isConfirm) {
                            $('#form-item-creation').submit();
                        }
                    });
                } else {
                    $('#form-item-creation').submit();
                }
            }
        });
    });
</script>