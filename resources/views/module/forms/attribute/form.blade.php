{!! Form::model($attribute, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Attribute Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="title">Title <span class="req">*</span></label>
                                <input type="text" name="title" id="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $attribute->title ?: old('title') }}">
                                @if($errors->has('title'))
                                    <label class="error">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Branch <span class="req">*</span></label>
                                <select name="branch[]" id="branch" multiple class="selectize restop-select {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}" {{ $attribute->branches->contains($branch->id) ? 'selected' : '' }}>{{ $branch->title }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('branch'))
                                    <label class="error">{{ $errors->first('branch') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Remarks</label>
                                <textarea name="remarks" id="remarks" cols="30" rows="3" class="form-control">{{ $attribute->remarks ?: old('remarks') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Options</div>
                    <div class="panel-body">
                        <div class="options-row">
                            @if($attribute->options->count() > 0)
                                @foreach($attribute->options as $key => $option)
                                <div class="option-row mrg15B">
                                    <div class="input-group">
                                        <input id="option" name="options[{{ $key }}][title]" value="{{ $option->title }}" class="form-control" type="text">
                                        <input type="hidden" name="options[{{ $key }}][id]" value="{{ $option->id }}">
                                        <span class="input-group-addon"><button type="button" class="text-danger btn-remove-option no-border"><i class="fa fa-trash"></i></button></span>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </div>
                        <a href="javascript:;" class="btn btn-primary btn-add-option"><i class="fa fa-plus"></i> Add Option</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel">
            <div class="panel-body">
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}