<script>
    $(document).ready(function () {
        $('.btn-add-option').on('click', function () {
            var optRow = $('.options-row'),
                count = optRow.find('.option-row').length,
                optItem = '<div class="option-row mrg15B">'
                    +'<div class="input-group"><input id="option" name="options['+count+'][title]" class="form-control" type="text"> <span class="input-group-addon"><button type="button" class="btn btn-danger btn-xs btn-remove-option"><i class="fa fa-trash"></i></button></span></div>'
                    +'</div>';

            optRow.append(optItem);
        });

        $('body').on('click', '.btn-remove-option', function () {
            $(this).closest('.option-row').fadeOut(function () {
                $(this).remove();
            });
        })
    });
</script>