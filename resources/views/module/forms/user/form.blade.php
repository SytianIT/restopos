{!! Form::model($user, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">User Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label">First Name <span class="req">*</span></label>
                        <input type="text" name="first_name" class="form-control {{ ($errors->has('first_name')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ $user->first_name ?: old('first_name') }}">
                        @if ($errors->has('first_name'))
                        <label class="error">{{ $errors->first('first_name') }}</label>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Last Name</label>
                        <input type="text" name="last_name" class="form-control {{ ($errors->has('last_name')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ $user->last_name ?: old('last_name') }}">
                        @if ($errors->has('last_name'))
                            <label class="error">{{ $errors->first('last_name') }}</label>
                        @endif
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label">Middle Name</label>
                        <input type="text" name="middle_name" class="form-control {{ ($errors->has('last_name')) ? 'parsley-error' : '' }}" placeholder="Middle Name" value="{{ $user->middle_name ?: old('last_name') }}">
                        @if ($errors->has('last_name'))
                            <label class="error">{{ $errors->first('last_name') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-6">
                        <label class="control-label">Email <span class="req">*</span></label>
                        <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="Email" value="{{ $user->email ?: old('email') }}">
                        @if ($errors->has('email'))
                            <label class="error">{{ $errors->first('email') }}</label>
                        @endif
                    </div>
                    <div class="col-sm-6">
                        <label class="control-label">Contact No.</label>
                        <input type="text" name="contact_no" class="form-control {{ ($errors->has('contact_no')) ? 'parsley-error' : '' }}" placeholder="Contact Number" value="{{ $user->contact_no ?: old('contact_no') }}">
                        @if ($errors->has('contact_no'))
                            <label class="error">{{ $errors->first('contact_no') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label class="control-label">Account Type <span class="req">*</span></label>
                        <select name="account_type" id="account_type" class="roles form-control restop-select {{ ($errors->has('account_type')) ? 'parsley-error' : '' }}">
                            <option value="">Select</option>
                            <option value="client">Client</option>
                            <option value="user">Regular User</option>
                        </select>
                    </div>
                    <div class="col-sm-4 hidden account-type-toggle">
                        <label class="control-label">Role <span class="req">*</span></label>
                        <select name="role_id[]" id="role_id" {{ (!isset($clientUser)) ? 'multiple' : '' }} class="restop-select roles {{ (!isset($clientUser)) ? 'multi-select' : 'form-control' }} {{ ($errors->has('role_id')) ? 'parsley-error' : '' }}">
                            @foreach($roles as $role)
                                <option value="{{ $role->id }}" {{ isset($userCurrentRoles) && in_array($role->id, $userCurrentRoles) ? 'selected' : '' }}>{{ $role->title }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('role_id'))
                            <label class="error">{{ $errors->first('role_id') }}</label>
                        @endif
                    </div>
                    @if(isset($clientRights))
                    <div class="col-sm-4 hidden account-type-toggle">
                        <label class="control-label">Branch Assignment <span class="req">*</span></label>
                        <select name="branches" id="branches" class="roles selectize restop-select {{ ($errors->has('role_id')) ? 'parsley-error' : '' }}" multiple>
                            @foreach($branches as $branch)
                                <option value="{{ $branch->id }}" {{ isset($userCurrentBranches) && in_array($branch->id, $userCurrentBranches) ? 'selected' : '' }}>{{ $branch->title }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('branches'))
                            <label class="error">{{ $errors->first('branches') }}</label>
                        @endif
                    </div>
                    @endif
                </div>
                <hr>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Username <span class="req">*</span></label>
                        <input type="text" name="username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}" placeholder="Username" value="{{ $user->username ?: old('username') }}">
                        @if ($errors->has('username'))
                        <label class="error">{{ $errors->first('username') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Password @if($user->password == null)<span class="req">*</span>@endif</label>
                        @if($user->password != null)
                        <span class="small">( To change password, type here the new password. )</span>
                        @endif
                        <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" placeholder="Password" value="{{ old('password') }}">
                        @if ($errors->has('password'))
                        <label class="error">{{ $errors->first('password') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Password Confirmation @if($user->password == null)<span class="req">*</span>@endif</label>
                        <input type="password" name="password_confirmation" class="form-control {{ ($errors->has('password_confirmation')) ? 'parsley-error' : '' }}" placeholder="Confirm Password" value="{{ old('password_confirmation') }}">
                        @if ($errors->has('password_confirmation'))
                        <label class="error">{{ $errors->first('password_confirmation') }}</label>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="ccontrol-label">Profile Image</label>
                        <div class="fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail {{ $user->profile_img == null ? 'hidden' : '' }}" data-trigger="fileinput" style="max-width: 350px; max-height: 350px;"><img src="{{ $user->avatar }}" class="img-responsive"></div>
                            <div>
                                <a href="javascript:;" class="btn btn-azure btn-file btn-sm">
                                    <span class="fileinput-new">Browse...</span>
                                    <input name="image" type="file" id="file-browse">
                                <div class="ripple-wrapper"></div></a><br>
                                <em class="font-size-10">For better results, please provide an image with a resolution of 350 x 350.</em>
                                @if ($errors->has('image'))
                                <label class="error">{{ $errors->first('image') }}</label>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($showButton))
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
                @endif
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@section('scripts')
    <script>
        $(document).ready(function(){
            function toggleAccountType($this){
                var $this = $this != null ? $this : $('#account_type'),
                    val = $this.val();

                if(val == 'user'){
                    $('.account-type-toggle').removeClass('hidden');
                } else {
                    $('.account-type-toggle').addClass('hidden');
                }
            }
            toggleAccountType(null);

            $('#account_type').on('change', function () {
                toggleAccountType($(this));
            })
        });
    </script>
@stop