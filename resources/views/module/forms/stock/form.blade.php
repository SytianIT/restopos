{!! Form::model($stockTransfer, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Stock Transfer Details</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="user">Prepared By <span class="req">*</span></label>
                                <input type="text" name="user" id="user" class="form-control disabled {{ ($errors->has('user')) ? 'parsley-error' : '' }}" value="{{ $stockTransfer->user ? $stockTransfer->user->name : $authUser->name }}">
                                @if($errors->has('user'))
                                    <label class="error">{{ $errors->first('user') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="datetime">Transfer Date <span class="req">*</span></label>
                                <input type="text" name="datetime" id="datetime" class="form-control datepicker {{ ($errors->has('datetime')) ? 'parsley-error' : '' }}" value="{{ $stockTransfer->datetime ? $stockTransfer->datetime->format('m/d/Y') : Carbon\Carbon::today()->format('m/d/Y') }}">
                                @if($errors->has('datetime'))
                                    <label class="error">{{ $errors->first('datetime') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Remarks</label>
                                <textarea name="remarks" id="remarks" cols="30" rows="3" class="form-control resize-none">{{ $stockTransfer->remarks ?: old('remarks') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <div class="content-box border-top border-red">
                                    <h3 class="content-box-header clearfix">Transfer from<small></small></h3>
                                    <div class="content-box-wrapper">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="from-branch-id">Branch <span class="req">*</span></label>
                                                <select name="branch_id" id="from-branch-id" data-target="dropdown-from-section" class="restop-select form-control dropdown-location-change {{ ($errors->has('branch_id')) ? 'parsley-error' : '' }}">
                                                    @foreach($branches as $branch)
                                                        <option value="{{ $branch->id }}" {{ $branch->id == $stockTransfer->branch_id ? 'selected' : '' }}>{{ $branch->title }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('branch_id'))
                                                    <label class="error">{{ $errors->first('branch_id') }}</label>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="dropdown-from-section">
                                            @foreach($branches as $branch)
                                                <div id="dropdown-from-section-{{ $branch->id }}" class="form-group dropdown-section" style="display:none">
                                                    <div class="col-sm-12">
                                                        <label for="from-location-id">Stock Location <span class="req">*</span></label>
                                                        <select name="from_location_id" id="from-location-id" class="restop-select form-control {{ ($errors->has('from_location_id')) ? 'parsley-error' : '' }}" disabled>
                                                            @foreach($branch->stockLocations as $stockLocation)
                                                                <option value="{{ $stockLocation->id }}" {{ $stockLocation->id == $stockTransfer->from_location_id ? 'selected' : '' }}>{{ $stockLocation->location_title }}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('from_location_id'))
                                                            <label class="error">{{ $errors->first('from_location_id') }}</label>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="content-box border-top border-green">
                                    <h3 class="content-box-header clearfix">Transfer to<small></small></h3>
                                    <div class="content-box-wrapper">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <label for="to-branch-id">Branch <span class="req">*</span></label>
                                                <select name="to_branch_id" id="to-branch-id" data-target="dropdown-to-section" class="restop-select form-control dropdown-location-change {{ ($errors->has('to_branch_id')) ? 'parsley-error' : '' }}">
                                                    @foreach($branches as $branch)
                                                        <option value="{{ $branch->id }}" {{ $branch->id == $stockTransfer->to_branch_id ? 'selected' : '' }}>{{ $branch->title }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('to_branch_id'))
                                                    <label class="error">{{ $errors->first('to_branch_id') }}</label>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="dropdown-to-section">
                                            @foreach($branches as $branch)
                                                <div id="dropdown-to-section-{{ $branch->id }}" class="form-group dropdown-section" style="display:none">
                                                    <div class="col-sm-12">
                                                        <label for="to-location-id">Stock Location <span class="req">*</span></label>
                                                        <select name="to_location_id" id="to-location-id" class="restop-select form-control {{ ($errors->has('to_location_id')) ? 'parsley-error' : '' }}" disabled>
                                                            @foreach($branch->stockLocations as $stockLocation)
                                                                <option value="{{ $stockLocation->id }}" {{ $stockLocation->id == $stockTransfer->to_location_id ? 'selected' : '' }}>{{ $stockLocation->location_title }}</option>
                                                            @endforeach
                                                        </select>
                                                        @if($errors->has('to_location_id'))
                                                            <label class="error">{{ $errors->first('to_location_id') }}</label>
                                                        @endif
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel">
            <div class="panel-body">
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
        </div>
        </div>
    </div>
</div>
{!! Form::close() !!}