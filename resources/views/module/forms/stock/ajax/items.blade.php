<!-- This blade can also be used by page load -->
<table class="table table-hover table-bordered">
    <thead>
    <tr>
        <th width="5%">#</th>
        <th width="60%">Product Name</th>
        <th width="15%">Measure</th>
        <th width="10%">Qty</th>
        <th width="10%">&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    @forelse($stockTransfer->stockTransferItems as $key => $stockTransferItem)
        <tr>
            <td>{{ $key }}</td>
            <td>{{ $stockTransferItem->item->title }}</td>
            <td>{{ $stockTransferItem->uom->title }}</td>
            <td>{{ $stockTransferItem->qty }}</td>
            <td class="text-center">@if($stockTransfer->isProcessing() || $stockTransfer->isPending())<a href="{{ route('client.stock-transfer.items.delete', [$stockTransfer, $stockTransferItem]) }}" class="font-red btn-remove-item"><i class="fa fa-trash"></i></a>@endif</td>
        </tr>
    @empty
        <tr>
            <td colspan="5">Currently no items included in this stock transfer record.</td>
        </tr>
    @endforelse
    </tbody>
    <tfoot>
    <tr class="bg-gray-alt">
        <td colspan="3" class="text-right font-bold">Total Items:</td>
        <td id="total-items-col">{{ $stockTransfer->stockTransferItems->sum('qty') }}</td>
        <td></td>
    </tr>
    </tfoot>
</table>