<script>
    $(document).ready(function(){
        function detectBranchDropdownValue($this){
            var $this = $this,
                element = $this.find('option:selected'),
                target = '#'+$this.data('target')+'-'+element.val(),
                wrapper = $('.'+$this.data('target'));

            wrapper.find('.dropdown-section').not(target).fadeOut(function () {
                $(this).find('select').attr('disabled', true);
                wrapper.find(target).fadeIn(function () {
                    $(this).find('select').removeAttr('disabled');
                });
            });
        }

        $('.dropdown-location-change').each(function() {
            detectBranchDropdownValue($(this));
        });

        $('body').on('change', '.dropdown-location-change', function(){
            detectBranchDropdownValue($(this));
        })

        // Start scripts for adding items module
        @if(isset($addItems))

            var itemForm = $('#stock-transfer-item'),
                prodSelect = $('#item'),
                qtyInput = $('#quantity'),
                stockTransfer = prodSelect.data('stockTransfer'),
                currentLocation = window.location;

            function generateProductSelectOption(){
                $.ajax({
                    url : currentLocation + '/get/products',
                    type : 'GET',
                    data : {
                        stock_transfer : stockTransfer
                    },
                    error : function(response){
                        swal(
                            'Oops..',
                            'Something went wrong, please refresh the page!',
                            'error'
                        );
                    },
                    success : function(response){
                        var html = '';
                        $.each(response, function(i, item) {
                            html += '<optgroup label=" '+i+'">';
                            $.each(item, function(x, value) {
                                html += '<option value="'+value.item.id+'">'+value.item.title+'</option>';
                            });
                            html += '</optgroup>';
                        });

                        prodSelect.append(html);
                        prodSelect.chosen();
                        $(".chosen-single div").html('<i class="glyph-icon icon-caret-down"></i>');
                    }
                });
            }
            generateProductSelectOption();

            function prepareResponse(action, message){
                var noticeWrapper = $('.items-notice');

                if(action == 'clear'){
                    noticeWrapper.addClass('hidden');
                    noticeWrapper.find('p').text('');
                }

                if(action == 'show'){
                    noticeWrapper.removeClass('hidden');
                    noticeWrapper.find('p').text(message);
                }
            }

            $('#btn-add-item').on('click', function(){

                $.ajax({
                    url : currentLocation + '/add',
                    type : 'POST',
                    dataType: 'json',
                    data : itemForm.serialize(),
                    beforeSend : function(){
                        prepareResponse('clear', null);
                    },
                    error : function(response){
                        swal(
                            'Oops..',
                            'Something went wrong, please try again later!',
                            'error'
                        );
                    },
                    success : function(response){
                        if(response.success){
                            // Reload the page
                            location.reload();

                            return true;
                        }

                        prepareResponse('show', response.message);
                    }
                });
            });

            $('body').on('click', '.btn-remove-item', function(e){
                e.preventDefault();

                var url = $(this).attr('href'),
                    $this = $(this);
                $.ajax({
                    url : url,
                    type : 'GET',
                    dataType: 'json',
                    error : function(response){
                        swal(
                            'Oops..',
                            'Something went wrong, please try again later!',
                            'error'
                        );
                    },
                    success : function(response){
                        if(response.success){
                            // Reload the page
                            $this.closest('tr').fadeOut(function(){
                                $this.remove();
                                $('#total-items-col').text(response.message);
                            });

                            return true;
                        }

                        prepareResponse('show', response.message);
                    }
                })
            });

        @endif
    });
</script>