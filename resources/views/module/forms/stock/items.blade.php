<div class="content-box border-top border-gray">
    <h3 class="content-box-header">Transfer Details
        @if($stockTransfer->isPending())
        <a href="{{ route('client.stock-transfer.transfer', $stockTransfer) }}" class="btn btn-md btn-info mrg25L mrg5R">Transfer Stock</a>
        @elseif($stockTransfer->isProcessing())
        <a href="{{ route('client.stock-transfer.receive', $stockTransfer) }}" class="btn btn-md btn-success mrg25L mrg5R">Receive Stocks</a>
        @endif
        @if($stockTransfer->isPending() || $stockTransfer->isProcessing())
        <a href="{{ route('client.stock-transfer.cancel', $stockTransfer) }}" class="btn btn-md btn-danger">Cancel</a>
        @endif
        <span class="pull-right font-size-15">{!! $stockTransfer->statusForHuman() !!}</span></h3>
    <div class="content-box-wrapper">
        <div class="row mrg0A">
            <div class="col-lg-4">
                <p><strong>Branch: </strong>{{ $stockTransfer->fromBranch->title }}</p>
                <p><strong>Location: </strong>{{ $stockTransfer->fromLocation->location_title }}</p>
            </div>
            <div class="col-lg-4">
                <p><strong>To Branch: </strong>{{ $stockTransfer->toBranch->title }}</p>
                <p><strong>To Location: </strong>{{ $stockTransfer->toLocation->location_title }}</p>
            </div>
            <div class="col-lg-4">
                <p><strong>Date: </strong>{{ $stockTransfer->formatFormDate('datetime') }}</p>
                <p><strong>Remarks: </strong>{{ $stockTransfer->remarks }}</p>
            </div>
        </div>
    </div>
</div>
<div class="content-box border-top border-gray">
    <h3 class="content-box-header">Items</h3>
    <div class="content-box-wrapper">
        <div class="table-items">
            @include('module.forms.stock.ajax.items', [
                'stockTransfer' => $stockTransfer
            ])
        </div>
        @if($stockTransfer->isPending())
        <hr>
        <div class="alert alert-notice items-notice hidden">
            <div class="bg-blue alert-icon"><i class="glyph-icon icon-info"></i></div>
            <div class="alert-content">
                <h4 class="alert-title">Cannot add item</h4>
                <p></p>
            </div>
        </div>
        {!! Form::open(['method' => 'POST', 'class' => 'form form-inline form-add-items', 'id' => 'stock-transfer-item']) !!}
        <h3>Quick Add Items</h3>
        <div class="form-group mrg15R" style="min-width: 320px">
            <label for="item" class="control-label display-block" data-stockTransfer="{{ $stockTransfer->id }}">Product</label>
            <select name="item_id" id="item"></select>
        </div>
        <div class="form-group mrg15R">
            <label for="quantity" class="control-label display-block">Quantity</label>
            <input type="text" name="quantity" id="quantity" class="form-control">
        </div>
        <div class="form-group">
            <label class="display-block control-label">&nbsp;</label>
            <button type="button" id="btn-add-item" class="btn btn-success"><i class="fa fa-plus mrg5R"></i>Add</button>
        </div>
        {!! Form::close() !!}
        @endif
    </div>
</div>