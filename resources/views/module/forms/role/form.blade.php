{!! Form::model($role, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal bordered-row form-validation-true']) !!}
<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Title <span class="req">*</span></label>
                        <input type="text" name="title" placeholder="Role Title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $role->title ?: old('title') }}">
                        @if ($errors->has('title'))
                            <label class="error">{{ $errors->first('title') }}</label>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Description</label>
                        <textarea name="description" class="form-control textarea-no-resize" rows="5" placeholder="Description">{{ $role->description ?: old('description') }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label">Permissions <span class="req">*</span></label>
                        <div class="row mrg10T">
                            <?php $ctr = 1; $countPermission = $permissions->count(); $division = round($countPermission / 2); ?>
                            @foreach($permissions as $permission)
                                @if($ctr == 1)
                                    <div class="col-xs-6">
                                        @endif
                                        <label class="restop-checkbox">
                                            <input type="checkbox" id="{{ $permission->name }}" checked name="permissions[]" value="{{ $permission->id }}" {{ isset($currentPermissions) && in_array($permission->id, $currentPermissions) ? 'checked' : '' }}>
                                            {{ $permission->title }}
                                        </label>
                                        @if($ctr == $division)
                                    </div>
                                    <div class="col-xs-6">
                                        @endif
                                        <?php $ctr++; ?>
                                        @endforeach
                                    </div>
                        </div>
                        @if ($errors->has('permissions'))
                            <label class="error">{{ $errors->first('permissions') }}</label>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-default">
            <div class="panel-body">
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}