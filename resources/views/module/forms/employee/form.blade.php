{!! Form::model($employee, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Employee Information</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-6">
								<label for="employee-number">Employee ID number </label>
								<input type="text" name="employee_number" id="employee-number" max="11" class="form-control {{ ($errors->has('employee_number')) ? 'parsley-error' : '' }}" value="{{ $employee->employee_number ?: old('employee_number') }}">
							</div>
							<div class="col-sm-6">
								<label for="branch">Branch <span class="req">*</span></label>
								<select name="branch" id="branch" class="selectize restop-select {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
									@foreach($branches as $branch)
									<option value="{{ $branch->id }}" {{ $branch->id == $employee->branch_id ? 'selected' : '' }}>{{ $branch->title }}</option>
									@endforeach
								</select>
								@if($errors->has('branch'))
								<label class="error">{{ $errors->first('branch') }}</label>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="first-name">First Name <span class="req">*</span></label>
								<input type="text" name="first_name" id="first-name" class="form-control" value="{{ $employee->first_name ?: old('first_name') }}">
								@if($errors->has('first_name'))
								<label class="error">{{ $errors->first('first_name') }}</label>
								@endif
							</div>
							<div class="col-sm-4">
								<label for="first-name">Last Name <span class="req">*</span></label>
								<input type="text" name="last_name" id="last-name" class="form-control" value="{{ $employee->last_name ?: old('last_name') }}">
								@if($errors->has('last_name'))
								<label class="error">{{ $errors->first('last_name') }}</label>
								@endif
							</div>
							<div class="col-sm-3">
								<label for="first-name">Gender <span class="req">*</span></label>
								<div class="radio-groups">
									<label for="male-gender" class="radio-inline restop-radio"><input type="radio" id="male-gender" name="gender" value="male" {{ $employee->gender == 'male' ? 'checked' : '' }}> Male</label>
									<label for="female-gender" class="radio-inline restop-radio"><input type="radio" id="female-gender" name="gender" value="female" {{ $employee->gender == 'female' ? 'checked' : '' }}> Female</label>
								</div>
								@if($errors->has('gender'))
								<label class="error">{{ $errors->first('gender') }}</label>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="email">E-Mail </label>
								<input type="text" name="email" id="email" class="form-control" value="{{ $employee->email ?: old('email') }}">
								@if($errors->has('email'))
								<label class="error">{{ $errors->first('email') }}</label>
								@endif
							</div>
							<div class="col-sm-4">
								<label for="mobile">Mobile</label>
								<input type="text" name="mobile" id="mobile" class="form-control" value="{{ $employee->mobile ?: old('mobile') }}">
							</div>
							<div class="col-sm-4">
								<label for="telephone">Telephone</label>
								<input type="text" name="telephone" id="telephone" class="form-control" value="{{ $employee->telephone ?: old('telephone') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label for="address_1">Address 1</label>
								<input type="text" name="address_1" id="address-1" class="form-control" value="{{ $employee->address_1 ?: old('address_1') }}">
							</div>
							<div class="col-sm-6">
								<label for="address_2">Address 2 </label>
								<input type="text" name="address_2" id="address-2" class="form-control" value="{{ $employee->address_2 ?: old('address_2') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="city">City</label>
								<input type="text" name="city" id="city" class="form-control" value="{{ $employee->city ?: old('city') }}">
							</div>
							<div class="col-sm-4">
								<label for="country">Country</label>
								<select name="country" id="country" class="selectize restop-select {{ ($errors->has('country')) ? 'parsley-error' : '' }}">
									@foreach($countries as $country)
									<option value="{{ $country->id }}" {{ $client->country == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-4">
								<label for="postal_code">Postal Code</label>
								<input type="text" name="postal_code" id="postal_code" class="form-control" value="{{ $employee->postal_code ?: old('postal_code') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-6">
								<label for="date-employed">Date Hired</label>
								<input type="text" name="date_employed" id="date-employed" class="datepicker form-control" value="{{ $employee->formatFormDate('date_employed') ?: old('date_employed') }}">
							</div>
							<div class="col-sm-6">
								<label for="employee-end-date">Employment End Date</label>
								<input type="text" name="employee_end_date" id="employee-end-date" class="datepicker form-control" value="{{ $employee->formatFormDate('employee_end_date') ?: old('employee_end_date') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-xs-12">
								<label for="postal_code">Notes</label>
								<textarea name="notes" id="notes" rows="3" class="form-control">{{ $employee->notes }}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="panel">
			<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-12">
	                    <label>Company Logo</label>
	                    <div>
	                        <div class="fileinput fileinput-new mrg0B" data-provides="fileinput">
	                            <div class="fileinput-preview thumbnail {{ $employee->image == null ? 'hidden' : '' }}" data-trigger="fileinput" style="max-width: 350px; max-height: 350px;"><img src="{{ $employee->avatar }}" class="img-responsive"></div>
	                            <div>
	                                <a href="javascript:;" class="btn btn-azure btn-xs btn-file">
	                                    <span class="fileinput-new">Select image</span>
	                                    <input name="image" type="file" id="file-browse">
	                                <div class="ripple-wrapper"></div></a><br>
	                                <em class="font-size-10">For better results, please provide an image with a resolution of 350 x 350.</em>
	                                @if ($errors->has('image'))
	                                <label class="error">{{ $errors->first('image') }}</p>
	                                @endif
	                            </div>
	                        </div>
	                    </div>
                    </div>
                </div>
                <div class="form-group">
                	<div class="col-sm-12">
						<label for="active" class="restop-checkbox">
							<input type="checkbox" name="active" id="active" {{ ($employee->id == null ? 'checked' : ($employee->active ? 'checked' : '')) }}> Active
						</label>
                	</div>
                </div>
				<button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}