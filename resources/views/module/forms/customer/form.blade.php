{!! Form::model($customer, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-lg-9">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Customer Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-lg-4">
                                <label for="first-name">First Name <span class="req">*</span></label>
                                <input type="text" name="first_name" id="first-name" class="form-control" value="{{ $customer->first_name ?: old('first_name') }}">
                                @if($errors->has('first_name'))
                                    <label class="error">{{ $errors->first('first_name') }}</label>
                                @endif
                            </div>
                            <div class="col-lg-4">
                                <label for="first-name">Last Name </label>
                                <input type="text" name="last_name" id="last-name" class="form-control" value="{{ $customer->last_name ?: old('last_name') }}">
                                @if($errors->has('last_name'))
                                    <label class="error">{{ $errors->first('last_name') }}</label>
                                @endif
                            </div>
                            <div class="col-lg-3">
                                <label for="first-name">Gender <span class="req">*</span></label>
                                <div class="radio-groups">
                                    <label for="male-gender" class="restop-radio radio-inline"><input type="radio" id="male-gender" name="gender" value="male" {{ $customer->gender == 'male' ? 'checked' : '' }}> Male</label>
                                    <label for="female-gender" class="restop-radio radio-inline"><input type="radio" id="female-gender" name="gender" value="female" {{ $customer->gender == 'female' ? 'checked' : '' }}> Female</label>
                                </div>
                                @if($errors->has('gender'))
                                    <label class="error">{{ $errors->first('gender') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-4 input-birthdays">
                                <label for="email" class="display-block">Birthday </label>
                                {{--<input type="hidden" id="old-birthday" name="old_birthday">--}}
                                {{--<div id="birthdayPicker"></div>--}}
                                <div class="bday-col">
                                    <select name="b_month" id="b-month" class="month restop-select form-control {{ ($errors->has('b_month')) ? 'parsley-error' : '' }}">
                                        @php $months = generateBirthPicker('months'); @endphp
                                        @foreach($months as $key => $month)
                                            <option value="{{ $key }}">{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="bday-col">
                                    <select name="b_day" id="b-day" class="day restop-select form-control {{ ($errors->has('b_day')) ? 'parsley-error' : '' }}">
                                        @php $days = generateBirthPicker('days'); @endphp
                                        @foreach($days as $day)
                                            <option value="{{ $day }}">{{ $day }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="bday-col">
                                    <select name="b_year" id="b-month" class="year restop-select form-control {{ ($errors->has('b_year')) ? 'parsley-error' : '' }}" placeholder="Year">
                                        @php $years = generateBirthPicker('year'); @endphp
                                        @for($i = $years['start']; $i >= $years['end']; $i--)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <label for="occupation">Occupation </label>
                                <input type="text" name="occupation" id="occupation" class="form-control" value="{{ $customer->occupation ?: old('occupation') }}">
                            </div>
                            <div class="col-lg-4">
                                <label for="email">E-Mail </label>
                                <input type="email" name="email" id="email" class="form-control" value="{{ $customer->email ?: old('email') }}">
                                @if($errors->has('email'))
                                    <label class="error">{{ $errors->first('email') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <label for="mobile">Mobile </label>
                                <input type="text" name="mobile" id="mobile" class="form-control" value="{{ $customer->mobile ?: old('mobile') }}">
                            </div>
                            <div class="col-lg-6">
                                <label for="telephone">Telephone</label>
                                <input type="text" name="telephone" id="telephone" class="form-control" value="{{ $customer->telephone ?: old('telephone') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-6">
                                <label for="address_1">Address 1 </label>
                                <input type="text" name="address_1" id="address-1" class="form-control" value="{{ $customer->address_1 ?: old('address_1') }}">
                            </div>
                            <div class="col-lg-6">
                                <label for="address_2">Address 2 </label>
                                <input type="text" name="address_2" id="address-2" class="form-control" value="{{ $customer->address_2 ?: old('address_2') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-4">
                                <label for="city">City <span class="req">*</span></label>
                                <input type="text" name="city" id="city" class="form-control" value="{{ $customer->city ?: old('city') }}">
                                @if($errors->has('city'))
                                    <label class="error">{{ $errors->first('city') }}</label>
                                @endif
                            </div>
                            <div class="col-lg-4">
                                <label for="country">Country</label>
                                <select name="country" id="country" class="selectize restop-select {{ ($errors->has('country')) ? 'parsley-error' : '' }}">
                                    @foreach($countries as $country)
                                        <option value="{{ $country->id }}" {{ $client->country == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-lg-4">
                                <label for="postal_code">Postal Code</label>
                                <input type="text" name="postal_code" id="postal_code" class="form-control" value="{{ $customer->postal_code ?: old('postal_code') }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="postal_code">Notes</label>
                                <textarea name="notes" id="notes" rows="3" class="form-control">{{ $customer->notes }}</textarea>
                            </div>
                            <div class="col-lg-6">
                                <label for="branch">Branch <span class="req">*</span></label>
                                <select name="branch" id="branch" class="selectize restop-select {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}" {{ $branch->id == $customer->branch_id ? 'selected' : '' }}>{{ $branch->title }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('branch'))
                                    <label class="error">{{ $errors->first('branch') }}</label>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-lg-12">
                        <label>Image</label>
                        <div>
                            <div class="fileinput fileinput-new mrg0B" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail {{ $customer->image == null ? 'hidden' : '' }}" data-trigger="fileinput" style="max-width: 350px; max-height: 350px;"><img src="{{ $customer->avatar }}" class="img-responsive"></div>
                                <div>
                                    <a href="javascript:;" class="btn btn-azure btn-xs btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        <input name="image" type="file" id="file-browse">
                                        <div class="ripple-wrapper"></div></a><br>
                                    <em class="font-size-10">For better results, please provide an image with a resolution of 350 x 350.</em>
                                    @if ($errors->has('image'))
                                        <label class="error">{{ $errors->first('image') }}</label>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <label class="display-block">Adds</label>
                        <label for="receive-email-notification" class="restop-checkbox font-size-13 font-normal">
                            <input type="checkbox" name="receive_email_notification" id="receive-email-notification" {{ (($customer->id == null) ? 'checked' : ($customer->receive_email_notification ? 'checked' : '')) }}> Enable receive of email notifications
                        </label>
                        <label for="receive-sms-notification" class="restop-checkbox font-size-13 font-normal">
                            <input type="checkbox" name="receive_sms_notification" id="receive-sms-notification" {{ (($customer->id == null) ? 'checked' : ($customer->receive_sms_notification ? 'checked' : '')) }}> Enable receive of SMS notifications
                        </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-12">
                        <label for="active" class="restop-checkbox">
                            <input type="checkbox" name="active" id="active" {{ ($customer->id == null ? 'checked' : ($customer->active ? 'checked' : '')) }}> Active
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}