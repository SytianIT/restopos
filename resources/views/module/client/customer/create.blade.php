@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.customers') }}">Customers</a></li>
    <li class="active">Add Customer</li>
@stop

@section('main-title', 'Add Customer')

@section('content')

    @include('module.forms.customer.form', [
        'customer' => new App\Repository\Customer\Customer,
        'showButton' => true,
        'buttonText' => 'Create Customer',
    ])
@stop

@section('scripts')
    <script src="{{ asset('vendor/birthday-picker-master/js/jquery-birthday-picker.js') }}"></script>
    <script>
        $(document).ready(function(){

            var oldDate = ($(".old-birthday").val() != '') ? $(".old-birthday").val() : null;
            $("#birthdayPicker").birthdayPicker({
                "monthFormat" : "long",
                "defaultDate" : oldDate
            });

        });
    </script>
@stop
