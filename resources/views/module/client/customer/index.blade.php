@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.customers') }}">Customers</a></li>
    <li class="active">Customers</li>
@stop

@section('main-title', 'Customers')

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="size-md">
                {{-- @if($authUser->hasPermission('create_roles')) --}}
                <a href="{{ route('client.customers.create') }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE CUSTOMERS</a>
                {{-- @endif --}}
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered" id="datatable-example">
                <thead>
                <tr>
                    <th width="5%" style="">&nbsp;</th>
                    <th width="20%">Name</th>
                    <th width="10%">Branch</th>
                    <th width="20%">Mobile</th>
                    <th width="10%">Date Registered</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customers as $customer)
                    <tr>
                        <td><a href="{{ route('client.customers.edit', $customer->id) }}"><img src="{{ $customer->avatar }}" alt="{{ $customer->name }}" class="img-responsive"></a></td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->branch->title }}</td>
                        <td>{{ $customer->mobile }}</td>
                        <td>{{ $customer->formatFormDate('created_at') }}</td>
                        <td class="text-center">
                            <a href="{{ route('client.customers.edit', $customer->id) }}" class="btn btn-xs btn-success">EDIT</a>
                            <a href="{{ route('client.customers.delete', $customer->id) }}" class="btn btn-xs btn-danger">DELETE</a>
                        </td>
                    </tr>
                @empty
                    <tr><td colspan="6">No records found!</td></tr>
                @endforelse
                </tbody>
            </table>
            {!! $customers->render() !!}
        </div>
    </div>
@stop