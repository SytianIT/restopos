@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('client.stock-locations') }}"> Stock Locations</a></li>
<li class="active">Add Stock Location</li>
@stop

@section('main-title', 'Add Stock Location')

@section('content')

@include('module.forms.stock-location.form', [
    'stockLocation' => new App\Repository\StockLocation\StockLocation,
    'buttonText' => 'Create Stock Location',
])
@stop