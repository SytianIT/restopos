@extends('layouts.app')

@section('crumbs')
<li class="active">Stock Locations</li>
@stop

@section('main-title')
Stock Locations  
@stop

@section('content')

<div class="panel panel-default">
    <div class="panel-body">
        <div class="pull-right text-right">
            <a href="{{ route('client.stock-locations.create') }}" class="btn btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE NEW</a>
        </div>
        {!! Form::open(['method' => 'GET']) !!}
        <div class="form form-inline filter-options">
            <div class="form-group" style="min-width: 140px;">
                <label for="filter-stocklocation-branch" class="control-label">Branches</label>
                <select name="filter_stocklocation_branch" id="filter-stocklocation-branch" class="form-control" style="min-width: 140px;">
                    @foreach($branches as $itemBranch)
                    <option value="{{ $itemBranch->id }}" {{ (Request::get('filter_stocklocation_branch') == $itemBranch->id ? 'selected' : '') }}>{{ $itemBranch->title }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label class="control-label">&nbsp;</label>
                <button type="submit" class="btn btn-azure"><i class="fa fa-filter mrg5R"></i>Filter</button>
            </div>
        </div>
        {!! Form::close() !!}
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-hover table-bordered" id="datatable-example">
            <thead>
                <tr>
                    <th width="20%">Location Name</th>
                    <th width="20%">Branch</th>
                    <th width="50%">Remarks</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($stockLocations as $stockLocation)
                <tr>
                    <td>{{ $stockLocation->location_title }}</td>
                    <td>{{ $stockLocation->branch->title }}</td>
                    <td>{{ $stockLocation->remarks }}</td>
                    <td class="text-center">
                        <a href="{{ route('client.stock-locations.edit', $stockLocation->id) }}" class="btn btn-success btn-xs">EDIT</a>
                        <a href="{{ route('client.stock-locations.delete', $stockLocation->id) }}" class="btn btn-danger btn-xs confirmAlert">DELETE</a>
                    </td>
                </tr>   
                @empty
                <tr>
                    <td colspan="4">No record of stock locations found!</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop
