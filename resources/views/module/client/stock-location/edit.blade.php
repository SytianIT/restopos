@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('client.stock-locations') }}"> Stock Locations</a></li>
<li class="active">Edit Stock Location</li>
@stop

@section('main-title', 'Edit Stock Location')

@section('content')

@include('module.forms.stock-location.form', [
    'stockLocation' => $stockLocation,
    'method' => 'PUT',
    'buttonText' => 'Update Stock Location',
])
@stop