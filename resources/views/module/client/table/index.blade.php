@extends('layouts.app')

@section('crumbs')
    <li class="active">Tables</li>
@stop

@section('main-title')
    Tables
@stop

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="pull-right text-right">
                <a href="{{ route('client.tables.create') }}" class="btn btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE NEW</a> |
                <a href="{{ route('client.tables.sections') }}" class="btn btn-azure"><i class="fa fa-list mrg5R"></i>SECTIONS</a>
            </div>
            {!! Form::open(['method' => 'GET']) !!}
            <div class="form form-inline filter-options">
                <div class="form-group" style="min-width: 140px;">
                    <label for="filter-tables-branch" class="control-label">Branches</label>
                    <select name="filter_tables_branch" id="branch" class="form-control" style="min-width: 140px;">
                        @foreach($branches as $itemBranch)
                            <option value="{{ $itemBranch->id }}" {{ (Request::get('filter_tables_branch') == $itemBranch->id ? 'selected' : '') }}>{{ $itemBranch->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group dropdown-sections" style="min-width: 140px;">
                    @foreach($branches as $branch)
                        <div id="dropdown-section-{{ $branch->id }}" class="dropdown-section" style="display: none">
                            <label for="company-name" class="control-label">Table Sections</label>
                            <select name="filter_tables_section" id="section" class="selectize">
                                <option value="">Select</option>
                                @foreach($branch->tableSections as $section)
                                    <option value="{{ $section->id }}" {{ Request::get('filter_tables_section') == $section->id ? 'selected' : '' }}>{{ $section->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endforeach
                </div>
                <div class="form-group">
                    <label class="control-label">&nbsp;</label>
                    <button type="submit" class="btn btn-azure"><i class="fa fa-filter mrg5R"></i>Filter</button>
                </div>
            </div>
            {!! Form::close() !!}
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-tables-list" id="datatable-example">
                <thead>
                <tr>
                    <th width="3%"></th>
                    <th width="20%">Title</th>
                    <th width="20%">Section</th>
                    <th width="42%">Remarks</th>
                    <th width="5%">Status</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody class="sortable" data-entityname="table">
                @forelse($tables as $table)
                    <tr data-itemId="{{ $table->id }}">
                        <td class="text-center sortable-handle"><span class="glyphicon glyphicon-sort glyphicon-sorter"></span></td>
                        <td>{{ $table->title }}</td>
                        <td>{{ $table->section->title }}</td>
                        <td>{{ $table->remarks }}</td>
                        <td class="text-center">{!! $table->active ? '<code class="font-green">ACTIVE</code>' : '<code class="font-red">INACTIVE</code>' !!}</td>
                        <td class="text-center">
                            <a href="{{ route('client.tables.edit', $table->id) }}" class="btn btn-success btn-xs">EDIT</a>
                            <a href="{{ route('client.tables.delete', $table->id) }}" class="btn btn-danger btn-xs confirmAlert">DELETE</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">No record of tables locations found!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $tables->render() !!}
        </div>
    </div>
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    @include('module.client.table.inc.scripts')
@stop
