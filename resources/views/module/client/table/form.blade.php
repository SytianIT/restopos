{!! Form::model($table, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Table Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="title">Table name <span class="req">*</span></label>
                                <input type="text" name="title" id="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $table->title ?: old('title') }}">
                                @if($errors->has('title'))
                                    <label class="error">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Branch <span class="req">*</span></label>
                                <select name="branch" id="branch" class="selectize {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}" {{ $branch->id == $table->branch_id ? 'selected' : '' }}>{{ $branch->title }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('branch'))
                                    <label class="error">{{ $errors->first('branch') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group dropdown-sections">
                            @foreach($branches as $branch)
                            <div id="dropdown-section-{{ $branch->id }}" class="col-sm-12 dropdown-section" style="display: none">
                                <label for="company-name">Table Sections <span class="req">*</span></label>
                                <select name="section" id="section" class="selectize {{ ($errors->has('section')) ? 'parsley-error' : '' }}">
                                    @foreach($branch->tableSections as $section)
                                        <option value="{{ $section->id }}" {{ $section->id == $table->section_id ? 'selected' : '' }}>{{ $section->title }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('section'))
                                    <label class="error">{{ $errors->first('section') }}</label>
                                @endif
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Remarks</label>
                                <textarea name="remarks" id="remarks" cols="30" rows="5" class="form-control">{{ $table->remarks ?: old('remarks') }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-info">
                            <label><input type="checkbox" id="checkbox-enable-table" name="active" class="custom-checkbox" {{ $table->id == null ? 'checked' : ($table->active ? 'checked' : '') }}> Active</label>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}