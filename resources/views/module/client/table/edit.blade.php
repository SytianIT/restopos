@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.tables') }}"> Tables</a></li>
    <li class="active">Edit Table</li>
@stop

@section('main-title', 'Edit Table')

@section('content')

    @include('module.client.table.form', [
        'table' => $table,
        'method' => 'PUT',
        'buttonText' => 'Update Table',
    ])
@stop

@section('scripts')
    @include('module.client.table.inc.scripts')
@stop