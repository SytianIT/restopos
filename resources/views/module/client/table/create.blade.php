@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.tables') }}"> Tables</a></li>
    <li class="active">Add Table</li>
@stop

@section('main-title', 'Add Table')

@section('content')

    @include('module.client.table.form', [
        'table' => new App\Repository\Table\Table,
        'buttonText' => 'Create Table',
    ])
@stop

@section('scripts')
    @include('module.client.table.inc.scripts')
@stop