<script>
    $(document).ready(function () {
        {{--@if(isset($tables) && $tables->count() > 0)--}}
            {{--$(document).ready(function(){--}}
                {{--$('.table-tables-list').dataTable({--}}
                    {{--searching: false,--}}
                    {{--"paging": false,--}}
                    {{--"order": false,--}}
                    {{--"columnDefs": [--}}
                        {{--{ "orderable": false, "targets": [0,4] }--}}
                    {{--]--}}
                {{--});--}}
            {{--});--}}
        {{--@endif--}}

        function detectBranchDropdownValue($this){
            var $this = $this != null ? $this : $('#branch'),
                element = $this.find('option:selected'),
                target = '#dropdown-section-'+element.val(),
                wrapper = $('.dropdown-sections');

            wrapper.find('.dropdown-section').not(target).fadeOut(function () {
                $(this).find('select').attr('disabled', true);
                wrapper.find(target).fadeIn(function () {
                    $(this).find('select').removeAttr('disabled');
                });
            });
        }

        detectBranchDropdownValue(null);
        $('body').on('change', '#branch', function(){
            detectBranchDropdownValue($(this));
        })
    });
</script>