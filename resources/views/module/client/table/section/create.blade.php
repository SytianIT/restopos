@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.tables') }}"> Tables</a></li>
    <li><a href="{{ route('client.tables.sections') }}">Sections</a></li>
    <li class="active">Add Section</li>
@stop

@section('main-title', 'Add Section')

@section('content')

    @include('module.client.table.section.form', [
        'section' => new App\Repository\Table\TableSection,
        'buttonText' => 'Create Section',
    ])

@stop