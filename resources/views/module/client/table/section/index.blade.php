@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.tables') }}">Tables</a></li>
    <li class="active">Sections</li>
@stop

@section('main-title', 'Sections')

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="pull-right text-right">
                <a href="{{ route('client.tables.sections.create') }}" class="btn btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE NEW</a>
            </div>
            {!! Form::open(['method' => 'GET']) !!}
            <div class="form form-inline filter-options">
                <div class="form-group" style="min-width: 140px;">
                    <label for="filter-tables-branch" class="control-label">Branches</label>
                    <select name="filter_tables_branch" id="filter-tables-branch" class="form-control" style="min-width: 140px;">
                        @foreach($branches as $itemBranch)
                            <option value="{{ $itemBranch->id }}" {{ (Request::get('filter_tables_branch') == $itemBranch->id ? 'selected' : '') }}>{{ $itemBranch->title }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="control-label">&nbsp;</label>
                    <button type="submit" class="btn btn-azure"><i class="fa fa-filter mrg5R"></i>Filter</button>
                </div>
            </div>
            {!! Form::close() !!}
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-tables-list" id="datatable-example">
                <thead>
                <tr>
                    <th width="20%">Title</th>
                    <th width="60%">Tables Assigned</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($sections as $section)
                    <tr>
                        <td>{{ $section->title }}</td>
                        <td>{{ $section->tables->count() }}</td>
                        <td class="text-center">
                            <a href="{{ route('client.tables.sections.edit', $section->id) }}" class="btn btn-success btn-xs">EDIT</a>
                            <a href="{{ route('client.tables.sections.delete', $section->id) }}" class="btn btn-danger btn-xs {{ $section->tables->count() > 0 ? 'deleteAlert' : 'confirmAlert' }}" {!! $section->tables->count() > 0 ? 'data-confirm-text="There`s '.$section->tables->count().' table assigned to this section, please delete them first to continue deleting this section."' : '' !!}>DELETE</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">No record of table sections found!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
            {!! $sections->render() !!}
        </div>
    </div>
@stop

@section('scripts')
    @if($sections->count() > 0)
        <script>
            $(document).ready(function(){
                $('.table-tables-list').dataTable({
                    searching: false,
                    "paging": false,
                    "order": [0, 'asc'],
                    "columnDefs": [
                        { "orderable": false, "targets": 2 }
                    ]
                });
            });
        </script>
    @endif
@stop
