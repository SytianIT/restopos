@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.tables') }}"> Tables</a></li>
    <li><a href="{{ route('client.tables.sections') }}">Sections</a></li>
    <li class="active">Update Section</li>
@stop

@section('main-title', 'Update Section')

@section('content')

    @include('module.client.table.section.form', [
        'section' => $section,
        'method' => 'PUT',
        'buttonText' => 'Update Section',
    ])

@stop