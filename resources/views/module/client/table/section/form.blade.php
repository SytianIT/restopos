{!! Form::model($section, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-9">
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Table Section Information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="title">Section name <span class="req">*</span></label>
                                <input type="text" name="title" id="title" class="form-control {{ ($errors->has('title')) ? 'parsley-error' : '' }}" value="{{ $section->title ?: old('title') }}">
                                @if($errors->has('title'))
                                    <label class="error">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="company-name">Branch <span class="req">*</span></label>
                                <select name="branch" id="branch" class="selectize {{ ($errors->has('branch')) ? 'parsley-error' : '' }}">
                                    @foreach($branches as $branch)
                                        <option value="{{ $branch->id }}" {{ $branch->id == $section->branch_id ? 'selected' : '' }}>{{ $branch->title }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('branch'))
                                    <label class="error">{{ $errors->first('branch') }}</label>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="panel">
            <div class="panel-body">
                <button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}