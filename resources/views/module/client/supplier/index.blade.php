@extends('layouts.app')

@section('crumbs')
<li class="active">Suppliers</li>
@stop

@section('main-title', 'Suppliers')

@section('content')

<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
        {{-- @if($authUser->hasPermission('create_roles')) --}}
        <a href="{{ route('client.suppliers.create') }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE SUPPLIER</a>
        {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-hover table-bordered" id="datatable-example">
            <thead>
                <tr>
                    <th>Supplier Name</th>
                    <th>Address</th>
                    <th>Branches</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($suppliers as $supplier)
                <tr>
                    <td><a href="{{ route('client.suppliers.edit', $supplier->id) }}">{{ $supplier->title }}</a></td>
                    <td>{{ $supplier->address }}</td>
                    <td>{{ $supplier->branches->implode('title', ',') }}</td>
                    <td class="text-center">
                        <a href="{{ route('client.suppliers.edit', $supplier->id) }}" class="btn btn-xs btn-success">EDIT</a>
                        <a href="{{ route('client.suppliers.delete', $supplier->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">No records of suppliers found!</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop