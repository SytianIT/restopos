@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('client.suppliers', $client) }}">Suppliers</a></li>
<li class="active">Add Supplier</li>
@stop

@section('main-title', 'Add Supplier')

@section('content')

@include('module.forms.supplier.form', [
    'supplier' => new App\Repository\Supplier\Supplier,
    'showButton' => true,
    'buttonText' => 'Create Supplier',
])
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $(document).ready(function(){
    	var initVal = $('.datetimepkr').val();
    	$(".datetimepkr").datetimepicker({
    		defaultDate: initVal != null ? initVal : new Date(),
    	});
    });
</script>
@stop