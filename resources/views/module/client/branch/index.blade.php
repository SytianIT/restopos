@extends('layouts.app')

@section('crumbs')
<li class="active">Branches</li>
@stop

@section('main-title', 'Branches')

@section('content')

<div class="panel panel-default">
    <div class="panel-body">
        {{-- <div class="size-md">
        <a href="{{ route('client.branches.create') }}" class="btn btn-sm btn-azure">CREATE BRANCH</a>
        </div> --}}
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-branches-list" id="datatable-example">
            <thead>
                <tr>
                    <th width="5%">&nbsp;</th>
                    <th>Branch Name</th>
                    <th>Address</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($branches as $branch)
                <tr>
                    <td><img src="{{ asset($branch->avatar) }}" class="img-responsive"></td>
                    <td>{{ $branch->title }}</td>
                    <td>{{ $branch->address_1 }}</td>
                    <td class="text-center">
                        <a href="{{ route('client.branches.edit', $branch->id) }}" class="btn btn-xs btn-success">EDIT</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {!! $branches->render() !!}
    </div>
</div>
@stop

@section('scripts')
    @if($branches->count() > 0)
    <script>
        $(document).ready(function(){
            $('.table-branches-list').dataTable({
                searching: false,
                "paging": false,
                "order": [1, 'asc'],
                "columnDefs": [
                    { "orderable": false, "targets": [0,3] }
                ]
            });
        });
    </script>
    @endif
@stop