@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('crumbs')
<li><a href="{{ route('client.branches') }}"> Branches</a></li>
<li class="active">Add Branch</li>
@stop

@section('main-title', 'Add Branch')

@section('content')
@include('module.forms.branch.form', [
    'branch' => new App\Repository\Branch\Branch,
    'showButton' => true,
    'buttonText' => 'Create Branch',
])
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $(document).ready(function(){
    	var initVal = $('.datetimepkr').val();
    	$(".datetimepkr").datetimepicker({
    		defaultDate: initVal != null ? initVal : new Date(),
    	});
    });
</script>
@stop