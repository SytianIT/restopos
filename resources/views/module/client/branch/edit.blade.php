@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('crumbs')
<li><a href="{{ route('client.branches') }}">Branches</a></li>
<li class="active">{{ $branch->title }} - Edit Branch</li>
@stop

@section('main-title', 'Edit Branch')

@section('content')
@include('module.forms.branch.form', [
    'branch' => $branch,
    'clientLvl' => true,
    'method' => 'PUT',
    'showButton' => true,
    'buttonText' => 'Update Branch',
])
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $(document).ready(function(){
    	var initVal = $('.datetimepkr').val();
    	$(".datetimepkr").datetimepicker({
    		defaultDate: initVal,
    	});
    });
</script>
@stop