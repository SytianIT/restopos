@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('client.employees', $client->id) }}">Employees</a></li>
<li class="active">Add Employee</li>
@stop

@section('main-title', 'Add Employee')

@section('content')

@include('module.forms.employee.form', [
    'employee' => new App\Repository\Employee\Employee,
    'showButton' => true,
    'buttonText' => 'Create Employee',
])
@stop