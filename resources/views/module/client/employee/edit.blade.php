@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('client.employees', $client->id) }}">Employees</a></li>
<li class="active">Edit Employee</li>
@stop

@section('main-title', 'Employees')

@section('content')

@include('module.forms.employee.form', [
    'employee' => $employee,
    'method' => 'PUT',
    'showButton' => true,
    'buttonText' => 'Update Employee',
])
@stop