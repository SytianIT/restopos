@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.roles', $client->id) }}">Roles</a></li>
    <li class="active">Edit Role</li>
@stop

@section('main-title', 'Edit Role')

@section('content')
    @include('module.forms.role.form', [
        'role' => $role,
        'method' => 'PUT',
        'buttonText' => 'Update Role'
    ])
@stop