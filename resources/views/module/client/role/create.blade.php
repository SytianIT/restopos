@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.roles', $client->id) }}">Roles</a></li>
    <li class="active">Add Role</li>
@stop

@section('main-title', 'Add Role')

@section('content')
    @include('module.forms.role.form', [
        'role' => new App\Role,
        'buttonText' => 'Create Role'
    ])
@stop