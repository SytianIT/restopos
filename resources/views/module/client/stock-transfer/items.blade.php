@extends('layouts.app')

@section('main-id', 'app')

@section('crumbs')
    <li><a href="{{ route('client.stock-transfer') }}"> Stock Transfers</a></li>
    <li class="active">Stock Transfer Items</li>
@stop

@section('main-title', 'Stock Transfer #'.$stockTransfer->id)

@section('content')
    @include('module.forms.stock.items', [
        'stockTransfer' => $stockTransfer
    ])
@stop

@section('scripts')
    @include('module.forms.stock.scripts', [
        'addItems' => true
    ])
@stop