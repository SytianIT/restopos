@extends('layouts.app')

@section('crumbs')
    <li class="active">Stock Transfers</li>
@stop

@section('main-title')
    Stock Transfers
@stop

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="mrg20B">
                <a href="{{ route('client.stock-transfer') }}" class="btn btn-info"><i class="fa fa-angle-double-left mrg5R"></i>BACK</a>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-hover table-bordered" id="datatable-example">
                <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="15%">Transfer Date</th>
                    <th width="15%">Transfer By</th>
                    <th width="20%">From Branch</th>
                    <th width="20">To Branch</th>
                    <th width="8%">No. of Items</th>
                    <th width="7%">Status</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($stockTransfers as $stockTransfer)
                    <tr>
                        <td>{{ $stockTransfer->id }}</td>
                        <td>{{ $stockTransfer->datetime->format('F d, Y') }}</td>
                        <td>{{ $stockTransfer->user->name }}</td>
                        <td>{{ $stockTransfer->fromBranch->title }}</td>
                        <td>{{ $stockTransfer->toBranch->title }}</td>
                        <td>{{ $stockTransfer->stockTransferItems->count() }}</td>
                        <td>{!! $stockTransfer->statusForHuman() !!}</td>
                        <td class="text-center">
                            <a href="{{ route('client.stock-transfer.items', $stockTransfer->id) }}" class="btn btn-info btn-xs">ITEMS</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8">No record of stock transfers found!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop
