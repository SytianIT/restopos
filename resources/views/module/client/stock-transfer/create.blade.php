@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.stock-transfer') }}"> Stock Transfers</a></li>
    <li class="active">Add Stock Transfer</li>
@stop

@section('main-title', 'Add Stock Transfer')

@section('content')

    @include('module.forms.stock.form', [
        'stockTransfer' => new App\Repository\Stock\StockTransfer,
        'buttonText' => 'Save and continue',
    ])
@stop

@section('scripts')
    @include('module.forms.stock.scripts')
@stop