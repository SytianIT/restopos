@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.users', $client->id) }}">Users</a></li>
    <li class="active">Add User</li>
@stop

@section('main-title', 'Add User')

@section('content')
    @include('module.forms.user.form', [
        'user' => new App\User,
        'showButton' => true,
        'buttonText' => 'Create User',
        'clientUser' => true,
        'clientRights' => true,
    ])
@stop