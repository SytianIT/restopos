@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.users', $client->id) }}">Users</a></li>
    <li class="active">Edit User</li>
@stop

@section('main-title', 'Edit User')

@section('content')
    @include('module.forms.user.form', [
        'user' => $user,
        'method' => 'PUT',
        'showButton' => true,
        'buttonText' => 'Update User',
        'clientUser' => true,
        'clientRights' => true,
    ])
@stop