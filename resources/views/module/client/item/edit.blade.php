@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.items') }}">Items</a></li>
    <li class="active">Edit Item</li>
@stop

@section('main-title', 'Edit Item')

@section('content')
    @include('module.forms.item.form', [
        'item' => $item,
        'method' => 'PUT',
        'showButton' => true,
        'buttonText' => 'Update Item',
    ])
@stop

@section('scripts')
    @include('module.forms.item.scripts', [
        'variationsUrl' => route('client.items.generate-variations', 'item='.$item->id),
        'editPage' => true,
    ])
@stop