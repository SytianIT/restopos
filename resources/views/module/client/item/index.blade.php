@extends('layouts.app')

@section('crumbs')
    <li class="active">Items</li>
@stop

@section('main-title', 'Items')

@section('content')

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="size-md">
                <a href="{{ route('client.items.create') }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE ITEM</a>&nbsp;|&nbsp;<a href="{{ route('client.items.categories') }}" class="btn btn-sm btn-azure"><i class="fa fa-list mrg5R"></i>CATEGORIES</a>
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered" id="datatable-example">
                <thead>
                <tr>
                    <th width="3%"></th>
                    <th width="15%">Title</th>
                    <th width="15%">Category</th>
                    <th width="20%">Variations</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($items as $item)
                    <tr>
                        <td><a href="{{ route('client.items.edit', $item->id) }}"><img src="{{ $item->avatar }}" alt="{{ $item->title }}" class="img-responsive"></a></td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->category->title }}</td>
                        <td>{{ $item->variations->count() }}</td>
                        <td class="text-center">
                            <a href="{{ route('client.items.edit', $item->id) }}" class="btn btn-success btn-xs">EDIT</a>
                            <a href="{{ route('client.items.delete', $item->id) }}" class="btn btn-danger btn-xs confirmAlert">DELETE</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">No records of categories found!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop