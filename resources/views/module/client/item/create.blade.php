@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.items') }}">Items</a></li>
    <li class="active">Add Item</li>
@stop

@section('main-title', 'Add Item')

@section('content')
    @include('module.forms.item.form', [
        'item' => new App\Repository\Item\Item,
        'showButton' => true,
        'buttonText' => 'Create Item',
    ])
@stop

@section('scripts')
    @include('module.forms.item.scripts', [
        'variationsUrl' => route('client.items.generate-variations', $client->id)
    ])
@stop