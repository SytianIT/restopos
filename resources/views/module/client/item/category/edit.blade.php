@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.items') }}">Items</a></li>
    <li><a href="{{ route('client.items.categories', $client->id) }}">Categories</a></li>
    <li class="active">Edit Category</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Edit Category')

@section('content')

    @include('module.forms.item.category', [
        'category' => $category,
        'method' => 'PUT',
        'showButton' => true,
        'buttonText' => 'Update Category',
    ])
@stop