@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('client.items') }}">Items</a></li>
    <li><a href="{{ route('client.items.categories', $client->id) }}">Categories</a></li>
    <li class="active">Add Category</li>
@stop

@section('main-title', 'Add Categories')

@section('content')

    @include('module.forms.item.category', [
        'category' => new App\Repository\Item\ItemCategory,
        'showButton' => true,
        'buttonText' => 'Create Category',
    ])
@stop