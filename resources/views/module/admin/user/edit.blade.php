@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.users') }}">Users</a></li>
<li class="active">Edit User</li>
@stop

@section('main-title', 'Edit User')s

@section('content')
@include('module.forms.user.form', [
    'user' => $user,
    'method' => 'PUT',
    'showButton' => true,
    'buttonText' => 'Update User'
])
@stop