{{-- TO BE DEPRECIATED --}}
{!! Form::model($user, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
    <div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">User Details</div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">First Name <span class="req">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="firstname" class="form-control {{ ($errors->has('firstname')) ? 'parsley-error' : '' }}" placeholder="First Name" value="{{ $user->first_name ?: old('firstname') }}">
                        @if ($errors->has('firstname'))
                        <label class="error">{{ $errors->first('firstname') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="lastname" class="form-control {{ ($errors->has('lastname')) ? 'parsley-error' : '' }}" placeholder="Last Name" value="{{ $user->last_name ?: old('lastname') }}">
                        @if ($errors->has('lastname'))
                        <label class="error">{{ $errors->first('lastname') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Middle Name</label>
                    <div class="col-sm-9">
                        <input type="text" name="middlename" class="form-control {{ ($errors->has('middlename')) ? 'parsley-error' : '' }}" placeholder="Middle Name" value="{{ $user->middle_name ?: old('middlename') }}">
                        @if ($errors->has('middlename'))
                        <label class="error">{{ $errors->first('middlename') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Contact No.</label>
                    <div class="col-sm-9">
                        <input type="text" name="mobile_number" class="form-control {{ ($errors->has('mobile_number')) ? 'parsley-error' : '' }}" placeholder="Contact Number" value="{{ $user->mobile_number ?: old('mobile_number') }}">
                        @if ($errors->has('mobile_number'))
                        <label class="error">{{ $errors->first('mobile_number') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Role <span class="req">*</span></label>
                    <div class="col-sm-9">
                        <select name="role_id[]" id="role_id" multiple class="roles multi-select {{ ($errors->has('role_id')) ? 'parsley-error' : '' }}">
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}" {{ isset($currentUserRole) && in_array($role->id, $currentUserRole) ? 'selected' : '' }}>{{ $role->name }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('role_id'))
                        <label class="error">{{ $errors->first('role_id') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Username <span class="req">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="username" class="form-control {{ ($errors->has('username')) ? 'parsley-error' : '' }}" placeholder="Username" value="{{ $user->username ?: old('username') }}">
                        @if ($errors->has('username'))
                        <label class="error">{{ $errors->first('username') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Email <span class="req">*</span></label>
                    <div class="col-sm-9">
                        <input type="text" name="email" class="form-control {{ ($errors->has('email')) ? 'parsley-error' : '' }}" placeholder="Email" value="{{ $user->email ?: old('email') }}">
                        @if ($errors->has('email'))
                        <label class="error">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-9">
                        @if($user != null)
                        <span class="small">To change password, type here the new password.</span>
                        @endif
                        <input type="password" name="password" class="form-control {{ ($errors->has('password')) ? 'parsley-error' : '' }}" placeholder="Password" value="{{ old('password') }}">
                        @if ($errors->has('password'))
                        <label class="error">{{ $errors->first('password') }}</p>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Password Confirmation</label>
                    <div class="col-sm-9">
                        <input type="password" name="password_confirmation" class="form-control {{ ($errors->has('password_confirmation')) ? 'parsley-error' : '' }}" placeholder="Confirm Password" value="{{ old('password_confirmation') }}">
                        @if ($errors->has('password_confirmation'))
                        <label class="error">{{ $errors->first('password_confirmation') }}</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="panel">
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Profile Image</label>
                    <div class="col-sm-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail {{ $user->profile_img == null ? 'hidden' : '' }}" data-trigger="fileinput" style="max-width: 350px; max-height: 350px;"><img src="{{ $user->avatar }}" class="img-responsive"></div>
                            <div>
                                <a href="javascript:;" class="btn btn-azure btn-file">
                                    <span class="fileinput-new">Select image</span>
                                    <input name="image" type="file" id="file-browse">
                                <div class="ripple-wrapper"></div></a><br>
                                <em class="font-size-11">For better results, please provide an image with a resolution of 350 x 350.</em>
                                @if ($errors->has('image'))
                                <label class="error">{{ $errors->first('image') }}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @if(isset($showButton))
                <button type="submit" class="btn btn-azure btn-block">{{ $buttonText }}</button>
                @endif
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}