@extends('layouts.app')

@section('crumbs')
<li class="active">Users</li>
@stop

@section('main-title', 'Users')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
        {{-- @if($authUser->hasPermission('create_roles')) --}}
        <a href="{{ route('admin.users.create') }}" class="btn btn-sm btn-azure">CREATE USER</a>
            {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-hover table-bordered" id="datatable-example">
            <thead>
                <tr>
                    <th width="5%">&nbsp;</th>
                    <th>Last Name</th>
                    <th>First Name</th>
                    <th>Email</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($users as $user)
                <tr>
                    <td><a href="{{ route('admin.users.edit', $user->id) }}"><img src="{{ $user->avatar }}" alt="{{ $user->name }}" class="img-responsive"></a></td>
                    <td>{{ $user->last_name }}</td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->email }}</td>
                    <td class="text-center">
                        <a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-xs btn-success">EDIT</a>
                        <a href="{{ route('admin.users.delete', $user->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr><td colspan="6">No records found!</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop