@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.users') }}">Users</a></li>
<li class="active">Add User</li>
@stop

@section('main-title', 'Add User')

@section('content')
@if($roles->count() < 1)
<div class="panel panel-default">
    <div class="panel-body">
        <div class="alert alert-warning mrg0B">
            <div class="bg-orange alert-icon">
                <i class="glyph-icon icon-warning"></i>
            </div>
            <div class="alert-content">
                <h4 class="alert-title">Warning</h4>
                <p>There are 0 roles found, creation of user may not proceed. <a href="{{ route('admin.roles.create') }}" title="Link">Create Role Now!</a></p>
            </div>
        </div>
    </div>
</div>
@endif
@include('module.forms.user.form', [
    'user' => new App\User,
    'showButton' => true,
    'buttonText' => 'Create User'
])
@stop