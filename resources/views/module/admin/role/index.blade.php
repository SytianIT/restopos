@extends('layouts.app')

@section('crumbs')
<li class="active">Roles</li>
@stop

@section('main-title', 'Roles')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
            {{-- @if($authUser->hasPermission('create_roles')) --}}
            <a href="{{ route('admin.roles.create') }}" class="btn btn-sm btn-azure">CREATE ROLE</a>
            {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-hover table-bordered" id="datatable-example">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Users Assigned</th>
                    <th>Permissions Assigned</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($roles as $role)
                <tr>
                    <td>{{ $role->title }}</td>
                    <td>{{ $role->users()->count() }}</td>
                    <td>{{ $role->permissions()->count() }}</td>
                    <td class="text-center">
                        <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-xs btn-success">EDIT</a>
                        <a href="{{ route('admin.roles.delete', $role->id) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr><td colspan="4">No records found!</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop