@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li><a href="{{ route('admin.clients.users', $client->id) }}">Users</a></li>
<li class="active">Edit User</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Edit User')

@section('content')
@include('module.forms.user.form', [
    'user' => $user,
    'method' => 'PUT',
    'showButton' => true,
    'clientUser' => true,
    'buttonText' => 'Update User',
    'clientRights' => true,
])
@stop