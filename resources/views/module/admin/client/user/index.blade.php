@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li class="active">Users</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Users')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
            {{-- @if($authUser->hasPermission('create_roles')) --}}
            <a href="{{ route('admin.clients.users.create', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE USER</a>
            {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-users-list" id="datatable-example">
            <thead>
                <tr>
                    <th width="5%">&nbsp;</th>
                    <th>First Name</th>
                    <th>Role</th>
                    <th>Email</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($users as $user)
                <tr>
                    <td><a href="{{ route('admin.clients.users.edit', [$client->id, $user->id]) }}"><img src="{{ $user->avatar }}" alt="{{ $user->name }}" class="img-responsive"></a></td>
                    <td>{{ $user->first_name }}</td>
                    <td>{{ $user->role_as_client->title }}</td>
                    <td>{{ $user->email }}</td>
                    <td class="text-center">
                        <a href="{{ route('admin.clients.users.edit', [$client->id, $user->id]) }}" class="btn btn-xs btn-success">EDIT</a>
                        <a href="{{ route('admin.clients.users.delete', [$client->id, $user->id]) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr><td colspan="6">No records found!</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop

@section('scripts')
    @if($users->count() > 0)
        <script>
            $(document).ready(function(){
                $('.table-users-list').dataTable({
                    searching: false,
                    "paging": false,
                    "order": [1, 'asc'],
                    "columnDefs": [
                        { "orderable": false, "targets": [0,4] }
                    ]
                });
            });
        </script>
    @endif
@stop