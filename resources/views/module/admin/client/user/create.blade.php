@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li><a href="{{ route('admin.clients.users', $client->id) }}">Users</a></li>
<li class="active">Add User</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Add User')

@section('content')
@include('module.forms.user.form', [
    'user' => new App\User,
    'showButton' => true,
    'buttonText' => 'Create User',
    'clientUser' => true,
    'clientRights' => true,
])
@stop