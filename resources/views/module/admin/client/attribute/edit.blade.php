@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li><a href="{{ route('admin.clients.attributes', $client->id) }}">Attributes</a></li>
    <li class="active">Edit Attribute</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Edit Attribute')

@section('content')
    @include('module.forms.attribute.form', [
        'attribute' => $attribute,
        'method' => 'PUT',
        'showButton' => true,
        'buttonText' => 'Updating Attribute',
    ])
@stop

@section('scripts')
    @include('module.forms.attribute.scripts')
@stop