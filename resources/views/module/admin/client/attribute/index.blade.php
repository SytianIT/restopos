@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li class="active">Attributes</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Attributes')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="size-md">
                {{-- @if($authUser->hasPermission('create_roles')) --}}
                <a href="{{ route('admin.clients.attributes.create', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE ATTRIBUTE</a>
                {{-- @endif --}}
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered" id="datatable-example">
                <thead>
                <tr>
                    <th>Attribute Name</th>
                    <th>Number of Options</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($attributes as $attribute)
                    <tr>
                        <td>{{ $attribute->title }}</td>
                        <td>{{ $attribute->options->count() }}</td>
                        <td class="text-center">
                            <a href="{{ route('admin.clients.attributes.edit', [$client->id, $attribute->id]) }}" class="btn btn-xs btn-success">EDIT</a>
                            <a href="{{ route('admin.clients.attributes.delete', [$client->id, $attribute->id]) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="4">No records of attributes found!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop