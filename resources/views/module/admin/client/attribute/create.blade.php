@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li><a href="{{ route('admin.clients.attributes', $client->id) }}">Attributes</a></li>
    <li class="active">Add Attribute</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Add Attribute')

@section('content')
    @include('module.forms.attribute.form', [
        'attribute' => new App\Repository\Attribute\Attribute,
        'showButton' => true,
        'buttonText' => 'Create Attribute',
    ])
@stop

@section('scripts')
    @include('module.forms.attribute.scripts')
@stop