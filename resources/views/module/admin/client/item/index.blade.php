@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li class="active">Items</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Items')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="size-md">
                {{-- @if($authUser->hasPermission('create_roles')) --}}
                <a href="{{ route('admin.clients.items.create', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE ITEM</a>&nbsp;|&nbsp;<a href="{{ route('admin.clients.items.categories', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-list mrg5R"></i>CATEGORIES</a>
                {{-- @endif --}}
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered" id="datatable-example">
                <thead>
                <tr>
                    <th width="3%"></th>
                    <th width="15%">Title</th>
                    <th width="15%">Category</th>
                    <th width="20%">Variations</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($items as $item)
                <tr>
                    <td><a href="{{ route('admin.clients.items.edit', [$client->id, $item->id]) }}"><img src="{{ $item->avatar }}" alt="{{ $item->title }}" class="img-responsive"></a></td>
                    <td>{{ $item->title }}</td>
                    <td>{{ $item->category->title }}</td>
                    <td>{{ $item->variations->count() }}</td>
                    <td class="text-center">
                        <a href="{{ route('admin.clients.items.edit', [$client->id, $item->id]) }}" class="btn btn-success btn-xs">EDIT</a>
                        <a href="{{ route('admin.clients.items.delete', [$client->id, $item->id]) }}" class="btn btn-danger btn-xs confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">No records of categories found!</td>
                </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop