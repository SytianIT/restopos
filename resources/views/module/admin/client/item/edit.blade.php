@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li><a href="{{ route('admin.clients.items', $client->id) }}">Items</a></li>
    <li class="active">Edit Item</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Edit Item')

@section('content')
    @include('module.forms.item.form', [
        'item' => $item,
        'method' => 'PUT',
        'showButton' => true,
        'buttonText' => 'Update Item',
    ])
@stop

@section('scripts')
    @include('module.forms.item.scripts', [
        'variationsUrl' => route('admin.clients.items.generate-variations', [$client->id, 'item='.$item->id]),
        'editPage' => true,
    ])
@stop