@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li><a href="{{ route('admin.clients.items', $client->id) }}">Items</a></li>
    <li><a href="{{ route('admin.clients.items.categories', $client->id) }}">Categories</a></li>
    <li class="active">Add Category</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Add Items')

@section('content')
    @include('module.forms.item.category', [
        'category' => new App\Repository\Item\ItemCategory,
        'showButton' => true,
        'buttonText' => 'Create Category',
    ])
@stop