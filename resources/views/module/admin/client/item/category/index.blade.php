@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li><a href="{{ route('admin.clients.items', $client->id) }}">Items</a></li>
    <li class="active">Categories</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Item Categories')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="size-md">
                {{-- @if($authUser->hasPermission('create_roles')) --}}
                <a href="{{ route('admin.clients.items.categories.create', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE CATEGORY</a>
                {{-- @endif --}}
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-item-categories-list" id="datatable-example">
                <thead>
                <tr>
                    <th width="20%">Category Title</th>
                    <th width="30%">Notes</th>
                    <th width="20%">Branches</th>
                    <th width="10%">Item #</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($categories as $category)
                    @include('module.forms.item.category-row', [
                        'category' => $category,
                        'routesPrefix' => 'admin.clients.items.categories.',
                        'wildCards' => [$client->id, $category->id],
                        'hasClient' => true
                    ])
                @empty
                    <tr>
                        <td colspan="5">No records of categories found!</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop

@section('scripts')
    @if($categories->count() > 0)
        <script>
            $(document).ready(function(){
                $('.table-item-categories-list').dataTable({
                    searching: false,
                    "paging": false,
                    "order": [0, 'asc'],
                    "columnDefs": [
                        { "orderable": false, "targets": [4] }
                    ]
                });
            });
        </script>
    @endif
@stop