@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li><a href="{{ route('admin.clients.items', $client->id) }}">Menus</a></li>
    <li><a href="{{ route('admin.clients.items.categories', $client->id) }}">Categories</a></li>
    <li class="active">Edit Category</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Edit Category')

@section('content')
    @include('module.forms.item.category', [
        'category' => $category,
        'method' => 'PUT',
        'showButton' => true,
        'buttonText' => 'Update Category',
    ])
@stop