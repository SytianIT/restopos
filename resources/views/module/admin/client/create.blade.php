@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li class="active">Add Client</li>
@stop

@section('main-title', 'Add Client')

@section('content')
@include('module.admin.client.form', [
    'client' => new App\Repository\Client\Client,
    'buttonText' => 'Create Client'
])
@stop