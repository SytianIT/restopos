@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li class="active">Branches</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Branches')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
        {{-- @if($authUser->hasPermission('create_roles')) --}}
        <a href="{{ route('admin.clients.branches.create', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE BRANCH</a>
        {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-branches-list" id="datatable-example">
            <thead>
                <tr>
                    <th width="5%">&nbsp;</th>
                    <th>Branch Name</th>
                    <th>Address</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($branches as $branch)
                <tr>
                    <td><img src="{{ asset($branch->avatar) }}" class="img-responsive"></td>
                    <td>{{ $branch->title }}</td>
                    <td>{{ $branch->address_1 }}</td>
                    <td class="text-center">
                        <a href="{{ route('admin.clients.branches.edit', [$client->id, $branch->id]) }}" class="btn btn-xs btn-success">EDIT</a>
                        <a href="{{ route('admin.clients.branches.delete', [$client->id, $branch->id]) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr><td colspan="4">No records found!</td></tr>
                @endforelse
            </tbody>
        </table>
        {!! $branches->render() !!}
    </div>
</div>
@stop

@section('scripts')
    @if($branches->count() > 0)
    <script>
        $(document).ready(function(){
            $('.table-branches-list').dataTable({
                searching: false,
                "paging": false,
                "order": [2, 'asc'],
                "columnDefs": [
                    { "orderable": false, "targets": [0,3] }
                ]
            });
        });
    </script>
    @endif
@stop