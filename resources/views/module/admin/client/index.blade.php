@extends('layouts.app')

@section('crumbs')
<li class="active">Clients</li>
@stop

@section('main-title', 'Clients')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
            {{-- @if($authUser->hasPermission('create_roles')) --}}
            <a href="{{ route('admin.clients.create') }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE CLIENT</a>
            {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-client-list">
            <thead>
                <tr>
                    <th width="5%">&nbsp;</th>
                    <th>Client ID</th>
                    <th>Company Name</th>
                    <th>City</th>
                    <th>Contact Person</th>
                    <th>Email</th>
                    <th>Mobile</th>
                    <th class="text-center" width="30%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($clients as $client)
                <tr>
                    <td><a href="{{ route('admin.clients.edit', $client->id) }}"><img src="{{ asset($client->avatar) }}" class="img-responsive"></a></td>
                    <td>{{ $client->cuid }}</td>
                    <td>{{ $client->company_name }}</td>
                    <td>{{ $client->city }}</td>
                    <td>{{ $client->pc_first_name.' '.$client->pc_last_name  }}</td>
                    <td>{{ $client->pc_email }}</td>
                    <td>{{ $client->pc_mobile }}</td>
                    <td class="text-center">
                        <div class="mrg5B">
                            <a href="{{ route('admin.clients.edit', $client->id) }}" class="btn btn-success btn-xs" title="Edit">EDIT</a>
                            <a href="{{ route('admin.clients.branches', $client->id) }}" class="btn btn-blue-alt btn-xs" title="Branches">BRANCHES</a>
                            <a href="{{ route('admin.clients.items', $client->id) }}" class="btn btn-purple btn-xs" title="Items">ITEMS</a>
                            <a href="{{ route('admin.clients.attributes', $client->id) }}" class="btn btn-purple btn-xs" title="Attributes">Attributes</a>
                            <a href="{{ route('admin.clients.employees', $client->id) }}" class="btn btn-primary btn-xs" title="Emmployees">EMPLOYEES</a>
                            <a href="{{ route('admin.clients.customers', $client->id) }}" class="btn btn-primary btn-xs" title="Customers">CUSTOMERS</a>
                        </div>
                        <a href="{{ route('admin.clients.users', $client->id) }}" class="btn btn-azure btn-xs">USERS</a>
                        <a href="{{ route('admin.clients.roles', $client->id) }}" class="btn btn-yellow btn-xs">ROLES</a>
                        <a href="{{ route('admin.clients.delete', $client->id) }}" class="btn btn-danger btn-xs confirmAlert" data-confirm-text="You won`t be able to revert this!">DELETE</a>
                        {{-- <div class="upper-row mrg5B">
                        </div>
                        <div class="lower-row">    
                        </div> --}}
                    </td>
                </tr>
                @empty
                <tr colspan="7">No records found!</tr>
                @endforelse
            </tbody>
        </table>
        {!! $clients->render() !!}
    </div>
</div>
@stop

@section('scripts')
    @if($clients->count() > 0)
    <script>
        $(document).ready(function(){
            $('.table-client-list').dataTable({
                searching: false,
                "paging": false,
                "order": [1, 'asc'],
                "columnDefs": [
                    { "orderable": false, "targets": [0, 7] }
                ]
            });
        });
    </script>
    @endif
@stop