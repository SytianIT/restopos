@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li><a href="{{ route('admin.clients.customers', $client->id) }}">Customers</a></li>
    <li class="active">Edit Customer</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Edit Customer')

@section('content')
    @include('module.forms.customer.form', [
        'customer' => $customer,
        'method' => 'PUT',
        'showButton' => true,
        'buttonText' => 'Update Customer',
    ])
@stop

@section('scripts')
    <script src="{{ asset('vendor/birthday-picker-master/js/jquery-birthday-picker.js') }}"></script>
    <script>
        $(document).ready(function(){

            var oldDate = ($(".old-birthday").val() != '') ? $(".old-birthday").val() : null;
            $("#birthdayPicker").birthdayPicker({
                "monthFormat" : "long",
                "defaultDate" : oldDate
            });

        });
    </script>
@stop
