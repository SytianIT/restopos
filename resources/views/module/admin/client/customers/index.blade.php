@extends('layouts.app')

@section('crumbs')
    <li><a href="{{ route('admin.clients') }}">Clients</a></li>
    <li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
    <li class="active">Customers</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Customers')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="size-md">
                {{-- @if($authUser->hasPermission('create_roles')) --}}
                <a href="{{ route('admin.clients.customers.create', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE CUSTOMERS</a>
                {{-- @endif --}}
            </div>
            <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered" id="datatable-example">
                <thead>
                <tr>
                    <th width="3%" style="">&nbsp;</th>
                    <th width="10%">Name</th>
                    <th width="10%">Branch</th>
                    <th width="10%">Mobile</th>
                    <th width="10%">Date Registered</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse($customers as $customer)
                    <tr>
                        <td><a href="{{ route('admin.clients.customers.edit', [$client->id, $customer->id]) }}"><img src="{{ $customer->avatar }}" alt="{{ $customer->name }}" class="img-responsive"></a></td>
                        <td>{{ $customer->name }}</td>
                        <td>{{ $customer->branch->title }}</td>
                        <td>{{ $customer->mobile }}</td>
                        <td>{{ $customer->formatFormDate('created_at') }}</td>
                        <td class="text-center">
                            <a href="{{ route('admin.clients.customers.edit', [$client->id, $customer->id]) }}" class="btn btn-xs btn-success">EDIT</a>
                            <a href="{{ route('admin.clients.customers.delete', [$client->id, $customer->id]) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                            <a href="#" class="btn btn-blue-alt btn-xs">TRANSACTIONS</a>
                        </td>
                    </tr>
                @empty
                    <tr><td colspan="6">No records found!</td></tr>
                @endforelse
                </tbody>
            </table>
            {!! $customers->render() !!}
        </div>
    </div>
@stop