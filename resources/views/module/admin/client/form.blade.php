{!! Form::model($client, ['method' => isset($method) ? $method : 'POST', 'class' => 'form form-horizontal form-validation-true', 'enctype' => 'multipart/form-data']) !!}
<div class="row">
	<div class="col-sm-9">
		<div class="row">
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading">Client Information</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-9">
								<label for="company-name">Company Name <span class="req">*</span></label>
								<input type="text" name="company_name" id="company-name" class="form-control {{ ($errors->has('company_name')) ? 'parsley-error' : '' }}" value="{{ $client->company_name ?: old('company_name') }}">
								@if ($errors->has('company_name'))
		                        <label class="error">{{ $errors->first('company_name') }}</label>
		                        @endif
							</div>
							<div class="col-sm-3">
								<label for="client-id">Client ID <span class="req">*</span></label>
								<input type="text" name="cuid" id="client-id" class="form-control {{ ($errors->has('cuid')) ? 'parsley-error' : '' }}" value="{{ $client->cuid ?: old('cuid') }}">
								@if ($errors->has('cuid'))
									<label class="error">{{ $errors->first('cuid') }}</label>
								@endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="address-1">Address 1 <span class="req">*</span></label>
								<input type="text" name="address_1" id="address-1" class="form-control {{ ($errors->has('address_1')) ? 'parsley-error' : '' }}" value="{{ $client->address_1 ?: old('address_1') }}">
								@if ($errors->has('address_1'))
		                        <label class="error">{{ $errors->first('address_1') }}</label>
		                        @endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="address-2">Address 2</label>
								<input type="text" name="address_2" id="address-2" class="form-control" value="{{ $client->address_2 ?: old('address_2') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="city">City <span class="req">*</span></label>
								<input type="text" name="city" id="city" class="form-control {{ ($errors->has('city')) ? 'parsley-error' : '' }}" value="{{ $client->city ?: old('city') }}">
								@if ($errors->has('city'))
		                        <label class="error">{{ $errors->first('city') }}</label>
		                        @endif
							</div>
							<div class="col-sm-5">
								<label for="country">Country <span class="req">*</span></label>
								<select name="country" id="country" class="restop-select form-control {{ ($errors->has('country')) ? 'parsley-error' : '' }}">
									@foreach($countries as $country)
									<option value="{{ $country->id }}" {{ $client->getMyCountry($country) }}>{{ $country->name }}</option>
									@endforeach
								</select>
								@if ($errors->has('country'))
		                        <label class="error">{{ $errors->first('country') }}</label>
		                        @endif
							</div>
							<div class="col-sm-3">
								<label for="zip">Zip Code </label>
								<input type="text" name="zip" id="zip" class="form-control {{ ($errors->has('zip')) ? 'parsley-error' : '' }}" value="{{ $client->zip ?: old('zip') }}">
								@if ($errors->has('zip'))
		                        <label class="error">{{ $errors->first('zip') }}</label>
		                        @endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-5">
								<label for="website">Website</label>
								<input type="text" name="website" id="website" class="form-control" value="{{ $client->website ?: old('website') }}">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">Primary Contact Details</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-6">
								<label for="pc-first-name">First Name <span class="req">*</span></label>
								<input type="text" name="pc_first_name" id="pc-first-name" class="form-control {{ ($errors->has('pc_first_name')) ? 'parsley-error' : '' }}"  value="{{ $client->pc_first_name ?: old('pc_first_name') }}">
								@if ($errors->has('pc_first_name'))
		                        <label class="error">{{ $errors->first('pc_first_name') }}</label>
		                        @endif
							</div>
							<div class="col-sm-6">
								<label for="pc-last-name">Last Name <span class="req">*</span></label>
								<input type="text" name="pc_last_name" id="pc-last-name" class="form-control {{ ($errors->has('pc_last_name')) ? 'parsley-error' : '' }}"  value="{{ $client->pc_last_name ?: old('pc_last_name') }}">
								@if ($errors->has('pc_last_name'))
		                        <label class="error">{{ $errors->first('pc_last_name') }}</label>
		                        @endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="pc-email">Email <span class="req">*</span></label>
								<input type="email" name="pc_email" id="pc-email" class="form-control {{ ($errors->has('pc_email')) ? 'parsley-error' : '' }}"  value="{{ $client->pc_email ?: old('pc_email') }}">
								@if ($errors->has('pc_email'))
		                        <label class="error">{{ $errors->first('pc_email') }}</label>
		                        @endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="pc-mobile">Mobile Number <span class="req">*</span></label>
								<input type="text" name="pc_mobile" id="pc-mobile" class="form-control {{ ($errors->has('pc_mobile')) ? 'parsley-error' : '' }}"  value="{{ $client->pc_mobile ?: old('pc_mobile') }}">
								@if ($errors->has('pc_mobile'))
		                        <label class="error">{{ $errors->first('pc_mobile') }}</label>
		                        @endif
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="pc-telephone">Telephone Number</label>
								<input type="text" name="pc_telephone" id="pc-telephone" class="form-control"  value="{{ $client->pc_telephone ?: old('pc_telephone') }}">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="panel panel-default">
					<div class="panel-heading">Secondary Contact Details</div>
					<div class="panel-body">
						<div class="form-group">
							<div class="col-sm-6">
								<label for="sc-first-name">First Name</label>
								<input type="text" name="sc_first_name" id="sc-first-name" class="form-control"  value="{{ $client->sc_first_name ?: old('sc_first_name') }}">
							</div>
							<div class="col-sm-6">
								<label for="sc-last-name">Last Name</label>
								<input type="text" name="sc_last_name" id="sc-last-name" class="form-control" value="{{ $client->sc_last_name ?: old('sc_last_name') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="sc-email">Email</label>
								<input type="email" name="sc_email" id="sc-email" class="form-control" value="{{ $client->sc_email ?: old('sc_email') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="sc-mobile">Mobile Number</label>
								<input type="text" name="sc_mobile" id="sc-mobile" class="form-control" value="{{ $client->sc_mobile ?: old('sc_mobile') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<label for="sc-telephone">Telephone Number</label>
								<input type="text" name="sc_telephone" id="sc-telephone" class="form-control" value="{{ $client->sc_telephone ?: old('sc_telephone') }}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-3">
		<div class="panel panel-default">
			<div class="panel-body">
				<div class="form-group">
					<div class="col-sm-12">
						<label class="control-label">Client Logo</label>
						<div class="fileinput-new" data-provides="fileinput">
							<div class="fileinput-preview thumbnail {{ $client->company_logo == null ? 'hidden' : '' }}" data-trigger="fileinput"><img src="{{ $client->avatar }}" class="img-responsive width-reset"></div>
							<div>
								<a href="javascript:;" class="btn btn-azure btn-xs btn-file">
									<span class="fileinput-new">Select image</span>
									<input name="company_logo" type="file" id="file-browse">
								<div class="ripple-wrapper"></div></a><br>
								<em class="font-size-10">For better results provide an image with a resolution of 50px in height and any in width.</em>
								@if ($errors->has('image'))
								<label class="error">{{ $errors->first('image') }}</label>
								@endif
							</div>
						</div>
                    </div>
                </div>
				<button type="submit" class="btn btn-azure btn-block"><span class="glyphicon glyphicon-save mrg5R"></span>{{ $buttonText }}</button>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}