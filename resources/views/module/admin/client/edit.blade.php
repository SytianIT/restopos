@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li class="active">Edit Client</li>
@stop

@section('main-title', 'Edit Client')

@section('content')
@include('module.admin.client.form', [
    'client' => $client,
    'buttonText' => 'Update Client',
    'method' => 'PUT'
])
@stop