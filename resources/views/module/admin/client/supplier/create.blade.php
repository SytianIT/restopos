@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li><a href="{{ route('admin.clients.suppliers', $client->id) }}">Suppliers</a></li>
<li class="active">Add Supplier</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Add Supplier')

@section('content')
@include('module.forms.supplier.form', [
    'supplier' => new App\Repository\Supplier\Supplier,
    'showButton' => true,
    'buttonText' => 'Create Supplier',
])
@stop

@section('scripts')
<script type="text/javascript" src="{{ asset('vendor/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('vendor/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script>
    $(document).ready(function(){
    	var initVal = $('.datetimepkr').val();
    	$(".datetimepkr").datetimepicker({
    		defaultDate: initVal != null ? initVal : new Date(),
    	});
    });
</script>
@stop