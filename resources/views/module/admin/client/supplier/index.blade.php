@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li class="active">Suppliers</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Suppliers')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
        {{-- @if($authUser->hasPermission('create_roles')) --}}
        <a href="{{ route('admin.clients.suppliers.create', $client->id) }}" class="btn btn-sm btn-azure">CREATE SUPPLIER</a>
            {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-striped table-hover table-bordered" id="datatable-example">
            <thead>
                <tr>
                    <th>Supplier Name</th>
                    <th>Address</th>
                    <th>Branches</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($suppliers as $supplier)
                <tr>
                    <td>{{ $supplier->title }}</td>
                    <td>{{ $supplier->address_1 }}</td>
                    <td></td>
                    <td class="text-center">
                        <a href="{{ route('admin.clients.branches.edit', [$client->id, $supplier->id]) }}" class="btn btn-xs btn-success">EDIT</a>
                        <a href="{{ route('admin.clients.branches.delete', [$client->id, $supplier->id]) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="4">No records of suppliers found!</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop