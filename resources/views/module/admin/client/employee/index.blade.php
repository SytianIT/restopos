@extends('layouts.app')

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li class="active">Employees</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Employees')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">
        <div class="size-md">
        {{-- @if($authUser->hasPermission('create_roles')) --}}
        <a href="{{ route('admin.clients.employees.create', $client->id) }}" class="btn btn-sm btn-azure"><i class="fa fa-plus mrg5R"></i>CREATE EMPLOYEE</a>
        {{-- @endif --}}
        </div>
        <table cellpadding="0" cellspacing="0" border="0" class="mrg10T table table-hover table-bordered table-employees-list" id="datatable-example">
            <thead>
                <tr>
                    <th width="5%" style="">&nbsp;</th>
                    <th width="20%">Name</th>
                    <th width="10%">Branch</th>
                    <th width="20%">Mobile</th>
                    <th class="text-center" width="10%">Actions</th>
                </tr>
            </thead>
            <tbody>
                @forelse($employees as $employee)
                <tr>
                    <td><a href="{{ route('admin.clients.employees.edit', [$client->id, $employee->id]) }}"><img src="{{ $employee->avatar }}" alt="{{ $employee->name }}" class="img-responsive"></a></td>
                    <td>{{ $employee->last_name.', '.$employee->first_name }}</td>
                    <td>{{ $employee->branch->title }}</td>
                    <td>{{ $employee->mobile }}</td>
                    <td class="text-center">
                        <a href="{{ route('admin.clients.employees.edit', [$client->id, $employee->id]) }}" class="btn btn-xs btn-success">EDIT</a>
                        <a href="{{ route('admin.clients.employees.delete', [$client->id, $employee->id]) }}" class="btn btn-xs btn-danger confirmAlert">DELETE</a>
                    </td>
                </tr>
                @empty
                <tr><td colspan="6">No records found!</td></tr>
                @endforelse
            </tbody>
        </table>
    </div>
</div>
@stop

@section('scripts')
    @if($employees->count() > 0)
    <script>
        $(document).ready(function(){
            $('.table-employees-list').dataTable({
                searching: false,
                "paging": false,
                "order": [1, 'asc'],
                "columnDefs": [
                    { "orderable": false, "targets": [0,4] }
                ]
            });
        });
    </script>
    @endif
@stop