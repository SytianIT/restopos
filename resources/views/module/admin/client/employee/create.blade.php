@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li><a href="{{ route('admin.clients.employees', $client->id) }}">Employees</a></li>
<li class="active">Add Employee</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Add Employee')

@section('content')
@include('module.forms.employee.form', [
    'employee' => new App\Repository\Employee\Employee,
    'showButton' => true,
    'buttonText' => 'Create Employee',
])
@stop