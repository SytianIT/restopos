@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
@stop

@section('crumbs')
<li><a href="{{ route('admin.clients') }}">Clients</a></li>
<li><a href="{{ route('admin.clients.edit', $client->id) }}">{{ $client->company_name }}</a></li>
<li><a href="{{ route('admin.clients.employees', $client->id) }}">Employees</a></li>
<li class="active">Edit Employee</li>
@stop

@section('main-title', $client->company_name)
@section('secondary-title', 'Edit Employee')

@section('content')
@include('module.forms.employee.form', [
    'employee' => $employee,
    'method' => 'PUT',
    'showButton' => true,
    'buttonText' => 'Update Employee',
])
@stop