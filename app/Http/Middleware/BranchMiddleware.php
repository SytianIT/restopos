<?php

namespace App\Http\Middleware;

use App\Repository\ContextInterface;
use Closure;
use Illuminate\Support\Facades\Auth;

class BranchMiddleware
{
    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        $user = Auth::guard($guard)->user();

        if ($request->route('branch')) {
            $branch = Branch::whereUuid($request->route('branch'))->firstOrFail();
        } else if (session()->has('pos_branch')) {
            $branch = Branch::whereUuid(session('pos_branch'))->firstOrFail();
        } else {
            abort(403);
        }

        if($user->cannot('access', $branch)) {
            abort(403);
        }
         $this->context->set($branch);

        return $next($request);
    }
}
