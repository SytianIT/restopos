<?php

namespace App\Http\Middleware;

use App\Repository\ContextInterface;
use Closure;
use Illuminate\Support\Facades\Auth;

class ClientMiddleware
{
    public function __construct(ContextInterface $context)
    {
        $this->context = $context;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        $user = Auth::guard($guard)->user();

        if($user->isAdmin()){
            return redirect()->route('admin.dashboard')
                ->withWarning('Access intercepted you were redirected back.');
        } else if($user->isBranch()){
            return redirect()->back()
                ->withWarning('You were redirected back, you are not allowed to access the page.');
        }

        $client = $user->client;

        if($user->cannot('access', $client)) {
            abort(403);
        }
         $this->context->set($client);

        return $next($request);
    }
}
