<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GeneralController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $url = '';

    	if($user == false){

    		$url = route('login');

    	} else {

            if($user->isAdmin()){
                
                $url = route('admin.dashboard');
            
            } else if($user->isClient()){
                
                $url = route('client.dashboard');
            
            } else if($user->isBranch()){
                
                $url = route('branch.dashboard');
            }    

        }


        return redirect()->to($url);
    }
}
