<?php

namespace App\Http\Controllers\Client;

use App\Repository\Table\Table;
use App\Repository\Table\TableRepository;
use App\Repository\Table\TableSection;
use App\Repository\Table\TableSectionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TableSectionController extends Controller
{
    public function __construct(
        TableSectionRepository $tableSectionRepository,
        TableRepository $tableRepository)
    {
        $this->tableSectionRepository = $tableSectionRepository;
        $this->tableRepository = $tableRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $branches = $request->user()->client->branches;

        if(!$request->has('filter_tables_branch')){
            $request->request->add([
                'filter_tables_branch' => $branches->first()->id
            ]);
        }

        $sections = $this->tableSectionRepository->search($request);

        return view('module.client.table.section.index', compact('sections','branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $branches = $request->user()->client->branches;

        return view('module.client.table.section.create', compact('client','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->tableSectionRepository->validateSectionFields($request);

        $section = new TableSection();
        $section = $this->tableSectionRepository->storeSectionFields($request, $section);

        return redirect()->route('client.tables.sections.create')
            ->withSuccess("Table section creation of '{$section->title}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, TableSection $section)
    {
        $branches = $request->user()->client->branches;

        return view('module.client.table.section.edit', compact('section','client','branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TableSection $section)
    {
        $this->tableSectionRepository->validateSectionFields($request);

        $section = $this->tableSectionRepository->storeSectionFields($request, $section);

        return redirect()->route('client.tables.sections', 'filter_tables_branch='.$section->branch_id)
            ->withSuccess("Table section update of '{$section->title}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, TableSection $section)
    {
        $client = $request->user()->client;
        $branch = $client->branches()->find($section->branch_id);

        $section = $this->tableSectionRepository->find($section->id);
        $title = $section->title;
        $section->delete();

        return redirect()->route('client.tables.sections', 'filter_tables_branch='.$branch->id)
            ->withWarning("Deletion of table section '{$title}' was successful");
    }
}
