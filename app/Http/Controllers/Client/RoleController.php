<?php

namespace App\Http\Controllers\Client;

use App\Permission;
use App\Repository\Role\RoleRepository;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permission = new Permission();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = $request->user()->client->roles->sortBy('title');

        return view('module.client.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $client = $request->user()->client;
        $permissions = $this->permission->clientLevel()->orderBy('created_at', 'ASC')->get();

        return view('module.client.role.create', compact('client','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = $request->user()->client;
        $this->roleRepository->validateRoleFields($request);

        $role = new Role();
        $role = $this->roleRepository->fillRoleRecord($request, $role, $client);

        return redirect()->route('client.roles', $client->id)
            ->withSuccess("Role creation of '{$role->title}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Role $role)
    {
        $client = $request->user()->client;
        $permissions = $this->permission->clientLevel()->orderBy('created_at', 'ASC')->get();

        return view('module.client.role.edit', compact('role','client','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $client = $request->user()->client;
        $this->roleRepository->validateRoleFields($request);

        $role = $this->roleRepository->fillRoleRecord($request, $role, $client);

        return redirect()->route('client.roles', $client->id)
            ->withSuccess("Role update of '{$role->title}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Role $role)
    {
        $client = $request->user()->client;
        $role = $client->roles()->find($role->id);
        $title = $role->title;
        $role->delete();

        return redirect()->route('client.roles', $client->id)
            ->withSuccess("Role delete of '{$title}' was successful");
    }
}
