<?php

namespace App\Http\Controllers\Client;

use App\Repository\Attribute\AttributeRepository;
use App\Repository\Item\Item;
use App\Repository\Item\ItemCategoryRepository;
use App\Repository\Item\ItemRepository;
use App\Repository\Etc\Uom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    public function __construct(
        ItemRepository $itemRepository,
        ItemCategoryRepository $itemCategoryRepository,
        AttributeRepository $attributeRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->itemCategoryRepository = $itemCategoryRepository;
        $this->attributeRepository = $attributeRepository;
    }

    public function index(Request $request)
    {
        $client = $request->user()->client;

        $items = $this->itemRepository->search($request, true);

        return view('module.client.item.index', compact('client','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $client = $request->user()->client;
        $branches = $client->branches;

        $attributes = $this->attributeRepository->scope()->get();
        $categories = $this->itemCategoryRepository->scope()->parents()->get();
        $relatedItems = $this->itemRepository->scope()->parentItems()->orderBy('created_at', 'ASC')->get();
        $uoms = Uom::orderBy('long_title', 'ASC')->get();

        return view('module.client.item.create', compact('client','branches','categories','uoms','attributes','relatedItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = $request->user()->client;
        $this->itemRepository->validateItemFields($request);

        $item = new Item();
        $this->itemRepository->startCreatingItem($request, $item, $client);

        return redirect()->route('client.items.create')
            ->withSuccess('Item created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Item $item)
    {
        $client = $request->user()->client;
        $branches = $client->branches;

        $attributes = $this->attributeRepository->scope()->get();
        $categories = $this->itemCategoryRepository->scope()->parents()->get();
        $relatedItems = $this->itemRepository->scope(['pricing'])->parentItems()->orderBy('created_at', 'ASC')->get();
        $uoms = Uom::orderBy('long_title', 'ASC')->get();

        $recipes = $item->recipes->sortBy('created_at');
        $addons = $item->addons->sortBy('position');

        return view('module.client.item.edit', compact('item','client','branches','categories','uoms','attributes','relatedItems','addons','recipes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $item)
    {
        $client = $request->user()->client;

        $this->itemRepository->validateItemFields($request);

        $this->itemRepository->startCreatingItem($request, $item, $client);

        return redirect()->route('client.items.edit', $item->id)
            ->withSuccess('Item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Item $item)
    {
        $client = $request->user()->client;

        $item = $this->itemRepository->find($item->id);
        if($item){
            $title = $item->title;
            $item->delete();

            return redirect()->route('client.items', $item->id)
                ->withWarning("Deletion of item '{$title}' was successful.");
        }

        return redirect()->route('client.items', $item->id)
            ->withWarning('There was a problem in removing the item.');
    }

    /**
     * Method to generate variations and return the html
     */
    public function generateVariations(Request $request)
    {
        $client = $request->user()->client;

        if($request->has('item')){
            $item = Item::find($request->item);
            $variations = $item->variations;
        }

        $attributes = $this->attributeRepository->getAttributesFromRequest($request);
        $options = $this->attributeRepository->getAttributeOptions($attributes)->keyBy('id');
        $branches = $client->branches;
        $relatedItems = $this->itemRepository->scope()->parentItems()->orderBy('created_at', 'ASC')->get();

        return view('module.forms.item.ajax.variations', compact('client','branches','options','attributes','item','variations','relatedItems'));
    }
}
