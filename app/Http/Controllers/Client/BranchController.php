<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Repository\Branch\Branch;
use App\Repository\Branch\BranchRespository;
use App\Repository\Etc\Countries;
use Illuminate\Http\Request;

class BranchController extends Controller
{
	public function __construct(BranchRespository $branchRepository)
	{
		$this->branchRepository = $branchRepository;
	}

    public function index(Request $request = null)
    {
    	$branches = $request->user()->client->branches()->paginate(25);

    	return view('module.client.branch.index', compact('branches'));
    }

    public function create(Request $request = null)
    {
    	return redirect()->back()
            ->withWarning('You are not allowed to access that page!');
    }

    public function store(Request $request)
    {
        return redirect()->back()
            ->withWarning('You are not allowed to access that page!');
    }

    public function edit(Request $request, Branch $branch)
    {
    	$countries = Countries::orderBy('name')->get();

        return view('module.client.branch.edit', compact('countries','branch'));   
    }

    public function update(Branch $branch, Request $request)
    {
        $this->branchRepository->validateBranchFields($request, $branch);

        $branch = $this->branchRepository->fillData($request, $branch);
        $branch->save();

        return redirect()->route('client.branches')
                         ->withSuccess("Branch update of '{$branch->title}' was successfull");
    }

    public function delete(Branch $branch)
    {
        return redirect()->back()
            ->withWarning('You are not allowed to access that page!');
    }
}
