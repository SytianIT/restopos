<?php

namespace App\Http\Controllers\Client;

use App\Repository\Item\ItemCategory;
use App\Repository\Item\ItemCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ItemCategoryController extends Controller
{
    public function __construct(ItemCategoryRepository $itemCategoryRepository)
    {
        $this->itemCategoryRepository = $itemCategoryRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categories = $this->itemCategoryRepository->scope()->parents()->get();

        return view('module.client.item.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $client = $request->user()->client;
        $categories = $this->itemCategoryRepository->scope()->parents()->get();
        $branches = $client->branches;

        return view('module.client.item.category.create', compact('client','categories','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = $request->user()->client;
        $this->itemCategoryRepository->validateCategoryFields($request);

        $category = new ItemCategory();
        $category = $this->itemCategoryRepository->fillCategoryRecords($request, $category, $client);

        return redirect()->route('client.items.categories.create', $client->id)
            ->withSuccess("Category creation of '{$category->title}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ItemCategory $category)
    {
        $client = $request->user()->client;
        $categories = $this->itemCategoryRepository->scope()->parents()->get();
        $branches = $client->branches;

        return view('module.client.item.category.edit', compact('client','category','categories','branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ItemCategory $category)
    {
        $client = $request->user()->client;
        $this->itemCategoryRepository->validateCategoryFields($request);

        $category = $this->itemCategoryRepository->fillCategoryRecords($request, $category, $client);

        return redirect()->route('client.items.categories', $client->id)
            ->withSuccess("Category update of '{$category->title}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, ItemCategory $category)
    {
        $client = $request->user()->client;
        $category = $client->itemCategories()->find($category->id);
        $title = $category->title;
        $category->delete();

        return redirect()->route('client.items.categories', $client->id)
            ->withWarning("Category delete of '{$title}' was successful");
    }
}
