<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Repository\StockLocation\StockLocation;
use App\Repository\StockLocation\StockLocationRepository;
use Illuminate\Http\Request;

class StockLocationController extends Controller
{
	public function __construct(
	    StockLocationRepository $stockLocationRepository)
	{
		$this->stockLocationRepository = $stockLocationRepository;
	}
	
    public function index(Request $request = null)
    {
        $branches = $request->user()->client->branches;

        $firstBranch = $branches->first();
        if($request->has('filter_stocklocation_branch')){
            $firstBranch = $request->user()->client->branches()->where('id', '=', $request->filter_stocklocation_branch)->first();
        }

    	$stockLocations = $this->stockLocationRepository->getAllBy('branch_id', $firstBranch->id);

    	return view('module.client.stock-location.index', compact('stockLocations','branches'));
    }

    public function create(Request $request = null)
    {
    	$branches = $request->user()->client->branches;

    	return view('module.client.stock-location.create', compact('branches'));
    }

    public function store(Request $request)
    {
    	$this->stockLocationRepository->validateStockLocationFields($request);

    	$stockLocation = new StockLocation;
    	$stockLocation = $this->stockLocationRepository->fillStockLocationRecords($request, $stockLocation);
    	$stockLocation->save();

    	return redirect()->route('client.stock-locations', 'filter_stocklocation_branch='.$stockLocation->branch_id)
                         ->withSuccess("Stock location creation of '{$stockLocation->location_title}' was successfull");
    }

    public function edit(Request $request, StockLocation $stockLocation)
    {
        $branches = $request->user()->client->branches;

        return view('module.client.stock-location.edit', compact('branches','stockLocation'));
    }

    public function update(Request $request, StockLocation $stockLocation)
    {
        $this->stockLocationRepository->validateStockLocationFields($request, $stockLocation);

        $stockLocation = $this->stockLocationRepository->fillStockLocationRecords($request, $stockLocation);
        $stockLocation->save();

        return redirect()->route('client.stock-locations', 'filter_stocklocation_branch='.$stockLocation->branch_id)
                         ->withSuccess("Stock location update of '{$stockLocation->location_title}' was successfull");   
    }

    public function delete(StockLocation $stockLocation)
    {
        $stockLocation = $this->stockLocationRepository->find($stockLocation->id);
        $title = $stockLocation->location_title;
        $stockLocation->delete();

        return redirect()->route('client.stock-locations')
                         ->withWarning("Stock location delete of '{$title}' was successfull");
    }
}
