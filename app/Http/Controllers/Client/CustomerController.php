<?php

namespace App\Http\Controllers\Client;

use App\Repository\Customer\Customer;
use App\Repository\Customer\CustomerRepository;
use App\Repository\Etc\Countries;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $client = $request->user()->client;

        $branches = $client->branches;
        $customers = $this->customerRepository->search($request);

        return view('module.client.customer.index', compact('client','customers','branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @internal param Client $client
     */
    public function create(Request $request)
    {
        $client = $request->user()->client;

        $countries = Countries::get();
        $branches = $client->branches;

        return view('module.client.customer.create', compact('client','branches','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = $request->user()->client;
        $this->customerRepository->validateCustomerFields($request);

        $customer = new Customer();
        $customer = $this->customerRepository->fillCustomerRecords($request, $customer, $client);

        return redirect()->route('client.customers.create', $client->id)
            ->withSuccess("Employee creation of '{$customer->name}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Request $request, Customer $customer)
    {
        $client = $request->user()->client;

        $countries = Countries::get();
        $branches = $client->branches;

        return view('module.client.customer.edit', compact('client','customer','branches','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Customer $customer)
    {
        $client = $request->user()->client;

        $this->customerRepository->validateCustomerFields($request);
        $customer = $this->customerRepository->fillCustomerRecords($request, $customer, $client);

        return redirect()->route('client.customers', $client->id)
            ->withSuccess("Employee update of '{$customer->name}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param Customer $customer
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function delete(Customer $customer)
    {
        $customer = $this->customerRepository->find($customer->id);
        $name = $customer->name;
        $customer->delete();

        return redirect()->route('client.customers')
            ->withWarning("Customer '{$name}' deleted successfully");
    }
}
