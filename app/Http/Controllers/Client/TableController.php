<?php

namespace App\Http\Controllers\Client;

use App\Repository\Table\Table;
use App\Repository\Table\TableRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TableController extends Controller
{
    public function __construct(
        TableRepository $tableRepository)
    {
        $this->tableRepository = $tableRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $branches = $request->user()->client->branches;

        if(!$request->has('filter_tables_branch')){
            $request->request->add([
                'filter_tables_branch' => $branches->first()->id
            ]);
        }

        $tables = $this->tableRepository->search($request, ['section']);

        return view('module.client.table.index', compact('client','branches','tables','firstBranch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $branches = $request->user()->client->branches;

        return view('module.client.table.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->tableRepository->validateTableFields($request);

        $table = new Table();
        $table = $this->tableRepository->storeTableFields($request, $table);

        return redirect()->route('client.tables.create')
            ->withSuccess("Table creation of '{$table->title}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Table $table)
    {
        $branches = $request->user()->client->branches;

        return view('module.client.table.edit', compact('branches','table'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Table $table)
    {
        $this->tableRepository->validateTableFields($request);

        $table = $this->tableRepository->storeTableFields($request, $table);

        return redirect()->route('client.tables', 'filter_tables_branch='.$table->branch_id)
            ->withSuccess("Table update of '{$table->title}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Table $table)
    {
        $client = $request->user()->client;
        $branch = $client->branches()->find($table->branch_id);

        $table = $this->tableRepository->find($table->id);
        $title = $table->title;
        $table->delete();

        return redirect()->route('client.tables', 'filter_tables_branch='.$branch->id)
            ->withSuccess("Table delete of '{$title}' was successful");
    }
}
