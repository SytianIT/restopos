<?php

namespace App\Http\Controllers\Client;

use App\Repository\Item\Item;
use App\Repository\Stock\StockTransfer;
use App\Repository\Stock\StockTransferItem;
use App\Repository\Stock\StockTransferRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StockTransferController extends Controller
{
    public function __construct(
        StockTransferRepository $stockTransferRepository)
    {
        $this->stockTransferRepo = $stockTransferRepository;
    }
    
    public function index(Request $request)
    {
        $branches = $request->user()->client->branches;

        $request['active'] = true;
        $stockTransfers = $this->stockTransferRepo->search($request);

        return view('module.client.stock-transfer.index', compact('branches','stockTransfers'));
    }

    public function history(Request $request)
    {
        $history = true;
        $request['history'] = $history;
        $stockTransfers = $this->stockTransferRepo->search($request);

        return view('module.client.stock-transfer.history', compact('branches','stockTransfers','history'));
    }

    public function create(Request $request)
    {
        $branches = $request->user()->client->branches->load('stockLocations');

        return view('module.client.stock-transfer.create', compact('branches'));
    }

    public function store(Request $request)
    {
        $this->stockTransferRepo->validateStockTransferFields($request);

        $stockTransfer = new StockTransfer();
        $stockTransfer = $this->stockTransferRepo->fillStockTransferRecords($request, $stockTransfer);

        return redirect()->route('client.stock-transfer.items', $stockTransfer);
    }

    public function transfer(StockTransfer $stockTransfer)
    {
        $result = $this->stockTransferRepo->transferStocks($stockTransfer);

        if($result['success']){
            $stockTransfer->status = StockTransfer::IN_PROCESS;
            $stockTransfer->save();

            return redirect()->back()
                ->withSuccess($result['message']);
        }

        return redirect()->back()
            ->withError($result['message']);
    }

    public function receive(Request $request, StockTransfer $stockTransfer)
    {
        $this->stockTransferRepo->receiveStocks($request, $stockTransfer);

        return redirect()->back()
            ->withSuccess("Stock receive and put into complete status.");
    }
    
    public function cancel(StockTransfer $stockTransfer)
    {
        $products = $this->stockTransferRepo->cancelStocks($stockTransfer);

        return redirect()->back()
            ->withSuccess("Transfer cancelled.");
    }

    public function items(StockTransfer $stockTransfer)
    {
        return view('module.client.stock-transfer.items', compact('stockTransfer'));
    }

    public function getProducts(StockTransfer $stockTransfer)
    {
        $products = $this->stockTransferRepo->stockTransferLocationProducts($stockTransfer);
        $products = $products->groupBy(function($item, $key){
            return $item->item->category->title;
        });

        return response()->json($products);
    }

    public function addItem(Request $request, StockTransfer $stockTransfer)
    {
        $response = $this->stockTransferRepo->addItem($request, $stockTransfer);

        if($response['success']){
            //return view('module.forms.stock.ajax.items', compact('stockTransfer'));
        }

        return response()->json($response);
    }

    public function deleteItem(StockTransfer $stockTransfer, StockTransferItem $stockTransferItem)
    {
        if($stockTransfer->isProcessing()){
            $response = $this->stockTransferRepo->removeItem($stockTransferItem, $stockTransfer);
        }
        $item = $stockTransfer->stockTransferItems()->find($stockTransferItem->id);
        $item->delete();
        $total = $stockTransfer->stockTransferItems->count() > 0 ? $stockTransfer->stockTransferItems->sum('qty') : 0;

        return response()->json($this->stockTransferRepo->createResponse(true, $total));
    }
}
