<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use App\Repository\Etc\Countries;
use App\Repository\Employee\Employee;
use App\Repository\Employee\EmployeeRepository;
use Illuminate\Http\Request;

class EmployeesController extends Controller
{
    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $employees = $this->employeeRepository->search($request);

        return view('module.client.employee.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    	$client = $request->user()->client;
        $countries = Countries::orderBy('name')->get();
        $branches = $client->branches;

        return view('module.client.employee.create', compact('client','branches','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$client = $request->user()->client;

        $this->employeeRepository->validateEmployeeFields($request);

        $employee = new Employee;
        $employee = $this->employeeRepository->fillEmployeeRecords($request, $employee, $client);

        return redirect()->route('client.employees.create', $client->id)
                         ->withSuccess("Employee creation of '{$employee->name}' was successfull");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Employee $employee)
    {
    	$client = $request->user()->client;
        $countries = Countries::orderBy('name')->get();
        $branches = $client->branches;

        return view('module.client.employee.edit', compact('employee','client','branches','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
    	$client = $request->user()->client;
        $this->employeeRepository->validateEmployeeFields($request);

        $employee = $this->employeeRepository->fillEmployeeRecords($request, $employee, $client);

        return redirect()->route('client.employees', $client->id)
                         ->withSuccess("Employee update of '{$employee->name}' was successfull");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Employee $employee)
    {
        $employee = $this->employeeRepository->find($employee->id);

        $name = $employee->name;
        $employee->delete();

        return redirect()->route('client.employees')
                         ->withWarning("Employee '{$name}' deleted successfully");
    }
}
