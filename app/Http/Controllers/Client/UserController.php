<?php

namespace App\Http\Controllers\Client;

use App\Repository\User\UserRepository;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct(
        UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $authUser = $request->user();

        $users = $authUser->client->users;
        $users = $users->reject(function($user) use ($authUser) {
            return $user->id == $authUser->id;
        });

        return view('module.client.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $client = $request->user()->client;
        $branches = $client->branches->sortByDesc('name');
        $roles = Role::where('client_id', $client->id)
                     ->orWhere('name', Role::BRANCH_OWNER_ROLE)
                     ->orderBy('title')->get();

        return view('module.client.user.create', compact('client','roles','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = $request->user()->client;
        $this->userRepo->validateUserFields($request, null, 'client', $client);

        $user = new User;
        $user = $this->userRepo->fillUserRecords($request, $user, $client);

        return redirect()->route('client.users', $client->id)
            ->withSuccess("User creation of '{$user->name}' was successfull");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, User $user)
    {
        $client = $request->user()->client;
        $branches = $client->branches->sortByDesc('name');
        $roles = Role::where('client_id', $client->id)
            ->orWhere('name', Role::BRANCH_OWNER_ROLE)
            ->orderBy('title')->get();

        return view('module.client.user.edit', compact('user','client','roles','branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $client = $request->user()->client;
        $this->userRepo->validateUserFields($request, $user, 'client', $client);

        $user = $this->userRepo->fillUserRecords($request, $user, $client);

        return redirect()->route('client.users', $client->id)
            ->withSuccess("User update of '{$user->name}' was successfull");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, User $user)
    {
        $client = $request->user()->client;
        $user = $client->users()->find($user->id);
        $name = $user->name;
        $user->delete();

        return redirect()->route('client.users', $client->id)
            ->withWarning("User delete of '{$name}' was successfull");
    }
}
