<?php

namespace App\Http\Controllers\CLient;

use App\Http\Controllers\Controller;
use App\Repository\Client\Client;
use App\Repository\Supplier\Supplier;
use App\Repository\Supplier\SupplierRepository;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
	public function __construct(SupplierRepository $supplierRepository)
	{
		$this->supplierRepository = $supplierRepository;
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$client = $request->user()->client;
        $suppliers = $this->supplierRepository->search($request, ['branches']);

        return view('module.client.supplier.index', compact('client','suppliers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $client = $request->user()->client->load('branches');
        $branches = $client->branches()->get();

        return view('module.client.supplier.create', compact('client','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = $request->user()->client;

        $this->supplierRepository->validateSupplierFields($request);

        $supplier = new Supplier;
        $supplier = $this->supplierRepository->fillSupplierRecords($request, $supplier, $client);

        return redirect()->route('client.suppliers')
                         ->withSuccess("Supplier creation of '{$supplier->title}' was successfull");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Supplier $supplier)
    {
        $client = $request->user()->client->load('branches');
        $branches = $client->branches()->get();

        return view('module.client.supplier.edit', compact('client','branches','supplier'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Supplier $supplier)
    {
        $client = $request->user()->client;

        $this->supplierRepository->validateSupplierFields($request);

        $supplier = $this->supplierRepository->fillSupplierRecords($request, $supplier, $client);

        return redirect()->route('client.suppliers')
                         ->withSuccess("Supplier update of '{$supplier->title}' was successfull");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Supplier $supplier)
    {
        $supplier = $this->supplierRepository->find($supplier->id);
        
        $name = $supplier->title;
        $supplier->delete();

        return redirect()->route('client.suppliers')
                         ->withWarning("Supplier delete of '{$name}' was successfull");
    }
}
