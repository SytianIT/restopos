<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function username()
    {
        return 'username';
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);
        $errors = [];

        // if Client ID is of restop admin then will attempt auth immediately
        if ($request->cuid == User::ADMIN_CLIENT_ID) {
            if ($this->guard()->attempt($credentials, $request->has('remember'))) {
                return $this->sendLoginResponse($request);
            }
        } else {
            // find all with username and then filter by cuid
            $cuid = $request->cuid;
            $users = User::with('client')->where('username', $request->username)->get();
            $user = $users->filter(function($u) use ($cuid) {
                return $u->client && $u->client->cuid == $cuid;
            })->first();

            if ($user) {
                // finally compare the hash password
                if (\Hash::check($request->password, $user->password)) {
                    Auth::login($user);
                    return $this->sendLoginResponse($request);
                }
                $errors['password'] = 'Invalid password';
            } else {
                $errors['username'] = 'Invalid username';
            }

        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            $this->username() => 'required',
            'cuid' => 'required',
            'password' => 'required',
        ]);
    }

    /**
     * Get the failed login response instance.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return redirect()->back()
            ->withInput($request->only($this->username(), 'cuid', 'remember'))
            ->withErrors([
                $this->username() => Lang::get('auth.failed'),
            ]);
    }

    public function authenticated(Request $request)
    {
        if(Auth::user()->isAdmin()){

            return redirect()->route('admin.dashboard');

        } else if(Auth::user()->isClient()){

            return redirect()->route('client.dashboard');

        } else if(Auth::user()->isBranch()){

            return redirect()->route('branch.dashboard');

        } else {

            return redirect()->back()
                             ->withError('There was an error in finding the access rights for this account, please contact the administrator for more info.');
        }
    }
}
