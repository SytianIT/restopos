<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Method to use to paginate collection
     * @param  Collection $results     
     * @param  Integer $currentPage 
     * @param  Integer $perPage     
     * @param  Request $request     
     * @param  String $opt         
     * @return Collection              
     */
    public function paginateResults($results, $currentPage, $perPage, $request = null, $opt = [])
    {
        $temp = $results;
        $currentIndex = (($currentPage == 1 || $currentPage == 0) ? 0 : $currentPage - 1) * $perPage;
        $slice = $temp->slice($currentIndex, $perPage);

        return new LengthAwarePaginator($slice, count($results), $perPage, $currentPage, $opt);
    }
}
