<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\Client\Client;
use App\Repository\User\UserRepository;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class ClientUserController extends Controller
{
	public function __construct(UserRepository $userRepo)
	{
		$this->userRepo = $userRepo;
	}

    public function index(Client $client)
    {
    	$users = $client->users;

    	return view('module.admin.client.user.index', compact('users','client'));
    }

    public function create(Client $client)
    {
    	$roles = $client->roles()->orderBy('title', 'ASC')->get();
        $branches = $client->branches->sortByDesc('name');

    	return view('module.admin.client.user.create', compact('client','roles','branches'));
    }

    public function store(Client $client, Request $request)
    {
    	$this->userRepo->validateUserFields($request, null, 'client', $client);

        $user = new User;
        $user = $this->userRepo->fillUserRecords($request, $user, $client);

        return redirect()->route('admin.clients.users', $client->id)
                         ->withSuccess("User creation of '{$user->name}' was successfull");
    }

    public function edit(Client $client, User $user)
    {
        $roles = $client->roles()->orderBy('title', 'ASC')->get();
        $userCurrentRoles = array_flatten($user->roles->pluck('id')->toArray());
        $branches = $client->branches->sortByDesc('name');
        $userCurrentBranches = $user->branches->pluck('id')->toArray();

        return view('module.admin.client.user.edit', compact('client','roles','user','branches','userCurrentRoles','userCurrentBranches'));
    }

    public function update(Request $request, Client $client, User $user)
    {
        $this->userRepo->validateUserFields($request, $user, 'client', $client);

        $user = $this->userRepo->fillUserRecords($request, $user, $client);

        return redirect()->route('admin.clients.users', $client->id)
                         ->withSuccess("User update of '{$user->name}' was successfull");
    }

    public function delete(Client $client, User $user)
    {
        $user = $client->users()->find($user->id);
        $name = $user->name;
        $user->delete();

        return redirect()->route('admin.clients.users', $client->id)
                         ->withWarning("User '{$user->name}' deleted successfull");
    }
}
