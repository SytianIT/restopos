<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\Permission;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repository\Role\RoleRepository;

class RoleController extends Controller
{
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;    
    }

    /**
	 * Display all role method
	 * @return view
	 */
    public function index()
    {
    	$roles = Role::orderBy('created_at', 'DESC')->admins()->where('id', '<>', 1)->get();

    	return view('module.admin.role.index', compact('roles'));
    }

    /**
     * Creation method for Role
     * @return view w/ var permissions
     */
    public function create()
    {
        $permissions = Permission::orderBy('created_at', 'ASC')->get();

    	return view('module.admin.role.create', compact('permissions'));
    }

    /**
     * Store method to create Role
     * @param  Request $request
     * @return redirect 
     */
    public function store(Request $request)
    {
        $this->roleRepository->validateRoleFields($request);

        $role = new Role;
        $role = $this->roleRepository->fillRoleRecord($request, $role);

        return redirect()->route('admin.roles')
             ->withSuccess("Role creation of '{$role->title}' was successfull");
    }

    /**
     * Edit method for Role records
     * @param  Role  $role
     * @return redirect       
     */
    public function edit(Role $role)
    {
        $permissions = Permission::orderBy('created_at', 'ASC')->get();

        $rolePermissions = $role->permissions;
        $currentPermissions = array_flatten($rolePermissions->pluck('id')->toArray());

        return view('module.admin.role.edit', compact('role','permissions','currentPermissions'));
    }

    /**
     * Update method for Role records
     * @param  Request $request
     * @param  Role   $role  
     * @return redirect          
     */
    public function update(Request $request, Role $role)
    {
        $this->roleRepository->validateRoleFields($request);

        $role = $this->roleRepository->fillRoleRecord($request, $role);

        return redirect()->back()
            ->withSuccess("Role update of '{$role->title}' was successfull");
    }

    public function delete(Role $role)
    {
        $title = $role->title;
        $role->delete();

        return redirect()->back()
            ->withWarning("Role delete of '{$role->title}' was successfull");
    }
}
