<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\User\UserRepository;
use App\Role;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
	public function __construct(UserRepository $userRepo)
	{
		$this->userRepo = $userRepo;
	}

    /**
	 * Display User Index
	 * @return view
	 */
    public function index()
    {
        $users = User::notClientUsers()->removeCurrent()->removeSuperAdmin()->get();

    	return view('module.admin.user.index', compact('users'));
    }

    /**
     * Method to view create form for users
     * @return view
     */
    public function create()
    {
        $roles = Role::where('name', '<>', Role::SUPERADMIN)->get();

    	return view('module.admin.user.create', compact('roles'));
    }

    /**
     * Method to use to store user from creation
     * @param  Request $request
     * @return view
     */
    public function store(Request $request)
    {
        $this->userRepo->validateUserFields($request);

        $user = new User;
        $user = $this->userRepo->fillUserRecords($request, $user);
        $user->save();

        return redirect()->route('admin.users')
                         ->withSuccess("User creation of '{$user->name}' was successfull");
    }

    /**
     * Method use to view the edit form for user
     * @param  User   $user
     * @return view      
     */
    public function edit(User $user)
    {
        $roles = Role::where('name', '<>', Role::SUPERADMIN)->get();
        $userCurrentRoles = array_flatten($user->roles->pluck('id')->toArray());

        return view('module.admin.user.edit', compact('user','roles','userCurrentRoles'));
    }

    /**
     * Method used to update records of user
     * @param  Request $request
     * @param  User    $user   
     * @return view
     */
    public function update(Request $request, User $user)
    {
        $this->userRepo->validateUserFields($request, $user);

        $user = $this->userRepo->fillUserRecords($request, $user);
        $user->save();
        $roles = (count($request->role_id) > 0) ? $request->role_id : [];
        $user->roles()->sync($roles); 

        return redirect()->route('admin.users')
                         ->withSuccess("User update of '{$user->name}' was successfull");
    }

    /**
     * Method to use for deleting of user
     * @param  User   $user
     * @return view      
     */
    public function delete(User $user)
    {
        $name = $user->name;
        $user->delete();

        return redirect()->route('admin.users')
                         ->withSuccess("Deletion of user '{$name}' was successfull");
    }
}
