<?php

namespace App\Http\Controllers\Admin;

use App\Repository\Attribute\AttributeRepository;
use App\Repository\Client\Client;
use App\Repository\Item\Item;
use App\Repository\Item\ItemRepository;
use App\Repository\Etc\Uom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientItemsController extends Controller
{
    public function __construct(ItemRepository $itemRepository, AttributeRepository $attributeRepository)
    {
        $this->itemRepository = $itemRepository;
        $this->attributeRepository = $attributeRepository;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Client $client)
    {
        $query = $client->items()->parentItems()->with('variations');
        $items = $this->itemRepository->filter($request, $query, 'collection');

        return view('module.admin.client.item.index', compact('client','items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $attributes = $client->attributes;
        $branches = $client->branches;
        $categories = $client->itemCategories()->parents()->get();
        $relatedItems = $client->items()->parentItems()->get();
        $uoms = Uom::orderBy('long_title', 'ASC')->get();

        return view('module.admin.client.item.create', compact('client','branches','categories','uoms','attributes','relatedItems'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $this->itemRepository->validateItemFields($request);

        $item = new Item();
        $this->itemRepository->startCreatingItem($request, $item, $client);

        return redirect()->route('admin.clients.items.create', $client->id)
                         ->withSuccess('Item created successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client, Item $item)
    {
        $attributes = $client->attributes;
        $branches = $client->branches;
        $categories = $client->itemCategories()->parents()->get();
        $relatedItems = $client->items()->parentItems()->get();
        $uoms = Uom::orderBy('long_title', 'ASC')->get();

        $item->load([
            'pricing'
        ]);

        $recipes = $item->recipes->sortBy('created_at');
        $addons = $item->addons->sortBy('position');

        return view('module.admin.client.item.edit', compact('item','client','branches','categories','uoms','attributes','relatedItems','addons','recipes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client, Item $item)
    {
        $this->itemRepository->validateItemFields($request);

        $this->itemRepository->startCreatingItem($request, $item, $client);

        return redirect()->route('admin.clients.items.edit', [$client->id, $item->id])
            ->withSuccess('Item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Client $client, Item $item)
    {
        $item = $client->items()->find($item->id);
        if($item){
            $title = $item->title;
            $item->delete();

            return redirect()->route('admin.clients.items', [$client->id, $item->id])
                ->withWarning("Deletion of item '{$title}' was successful.");
        }

        return redirect()->route('admin.clients.items', [$client->id, $item->id])
            ->withWarning('There was a problem in removing the item.');
    }

    /**
     * Method to generate variations and return the html
     */
    public function generateVariations(Request $request, Client $client)
    {
        if($request->has('item')){
            $item = Item::find($request->item);
            $variations = $item->variations;
        }

        $attributes = $this->attributeRepository->getAttributesFromRequest($request);
        $options = $this->attributeRepository->getAttributeOptions($attributes)->keyBy('id');
        $branches = $client->branches;
        $relatedItems = $client->items()->parentItems()->get();

        return view('module.forms.item.ajax.variations', compact('client','branches','options','attributes','item','variations','relatedItems'));
    }
}
