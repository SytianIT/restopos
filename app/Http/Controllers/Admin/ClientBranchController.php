<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\Branch\Branch;
use App\Repository\Branch\BranchRespository;
use App\Repository\Client\Client;
use App\Repository\Etc\Countries;
use Illuminate\Http\Request;

class ClientBranchController extends Controller
{
	public function __construct(BranchRespository $branchRepository)
	{
		$this->branchRepository = $branchRepository;
	}

    public function index(Client $client)
    {
        $branches = $client->branches()->orderBy('title', 'ASC')->paginate(25);

    	return view('module.admin.client.branch.index', compact('client','branches'));
    }

    public function create(Client $client)
    {
    	$countries = Countries::orderBy('name')->get();

    	return view('module.admin.client.branch.create', compact('client','countries'));
    }

    public function store(Client $client, Request $request)
    {
    	$this->branchRepository->validateBranchFields($request);

        $branch = new Branch;
        $branch = $this->branchRepository->fillData($request, $branch, $client);
        $branch->save();

        return redirect()->route('admin.clients.branches', $client->id)
                         ->withSuccess("Branch creation of '{$branch->title}' was successfull");
    }

    public function edit(Client $client, Branch $branch)
    {
        $countries = Countries::orderBy('name')->get();

        return view('module.admin.client.branch.edit', compact('client','countries','branch'));     
    }

    public function update(Client $client, Branch $branch, Request $request)
    {
        $this->branchRepository->validateBranchFields($request, $branch);

        $branch = $this->branchRepository->fillData($request, $branch);
        $branch->save();

        return redirect()->route('admin.clients.branches', $client->id)
                         ->withSuccess("Branch update of '{$branch->title}' was successfull");
    }

    public function delete(Client $client, Branch $branch)
    {
        $title = $branch->title;
        $branch->delete();

        return redirect()->route('admin.clients.branches', $client->id)
                         ->withWarning("Branch '{$title}' deleted successfully");
    }
}
