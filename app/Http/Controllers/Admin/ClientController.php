<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\BaseRepository;
use App\Repository\Client\Client;
use App\Repository\Etc\Countries;
use Illuminate\Http\Request;

class ClientController extends Controller
{
	public function __construct(BaseRepository $baseRepository)
	{
		$this->baseRepository = $baseRepository;
	}

    public function index()
    {
    	$clients = Client::orderBy('created_at', 'DESC')->paginate(25);

    	return view('module.admin.client.index', compact('clients'));
    }

    public function create()
    {
    	$countries = Countries::orderBy('name')->get();

    	return view('module.admin.client.create', compact('countries'));
    }

    public function store(Request $request)
    {
    	$this->validateClientFields($request);

    	$client = new Client;
    	$client = $this->fillClientRecords($request, $client);

    	return redirect()->route('admin.clients')
    					 ->withSuccess('Client successfully created.');
    }

    public function edit(Client $client)
    {
    	$countries = Countries::orderBy('name')->get();

    	return view('module.admin.client.edit', compact('countries','client'));
    }

    public function update(Client $client, Request $request)
    {
    	$this->validateClientFields($request, $client);

    	$client = $this->fillClientRecords($request, $client);

    	return redirect()->route('admin.clients')
    					 ->withSuccess('Client successfully updated.');
    }

    public function delete(Client $client)
    {
    	$name = $client->company_name;
    	$client->delete();

    	return redirect()->route('admin.clients')
    					 ->withWarning("Client '{$name}' successfully deleted.");
    }

    /**
     * Private Methods
     */
    private function validateClientFields($request, $client = null)
    {
        $id = ($client != null) ? ','.$client->id : '';

    	$this->validate($request, [
    	    'cuid' => 'required|alpha_num|unique:clients,cuid'.$id,
    		'company_name' => 'required|max:50',
    		'address_1' => 'required',
    		'city' => 'required',
    		'country' => 'required',
    		// 'zip' => 'required',
    		'pc_first_name' => 'required',
    		'pc_last_name' => 'required',
    		'pc_email' => 'required|email',
    		'pc_mobile' => 'required',
    	], [
    	    'cuid.required' => 'Client ID is required.'
        ]);

    	return;
    }

    public function fillClientRecords($request, $client)
    {
        $client->cuid = $request->cuid;
    	$client->company_name = $request->company_name;
    	$client->address_1 = $request->address_1;
    	$client->address_2 = $request->address_2;
    	$client->city = $request->city;
    	$client->country = $request->country;
    	$client->zip = $request->zip;
    	$client->website = $request->website;
    	$client->pc_first_name = $request->pc_first_name;
    	$client->pc_last_name = $request->pc_last_name;
    	$client->pc_email = $request->pc_email;
    	$client->pc_mobile = $request->pc_mobile;
    	$client->pc_telephone = $request->pc_telephone;
    	$client->sc_first_name = $request->sc_first_name;
    	$client->sc_last_name = $request->sc_last_name;
    	$client->sc_email = $request->sc_email;
    	$client->sc_mobile = $request->sc_mobile;
    	$client->sc_telephone = $request->sc_telephone;
    	$client->subscription_status = true;

    	if($request->hasFile('company_logo')){
            $client->company_logo = $this->baseRepository->baseUpload($request->file('company_logo'), 'uploads/clients/logo', null, 50);
        }

    	$client->save();

    	return $client;
    }
}
