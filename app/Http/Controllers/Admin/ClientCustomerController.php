<?php

namespace App\Http\Controllers\Admin;

use App\Repository\Client\Client;
use App\Repository\Etc\Countries;
use App\Repository\Customer\Customer;
use App\Repository\Customer\CustomerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientCustomerController extends Controller
{
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Client $client)
    {
        $branches = $client->branches;
        $query = $client->customers();
        $customers = $this->customerRepository->filter($request, $query, 'collection');

        return view('module.admin.client.customers.index', compact('client','customers','branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $countries = Countries::get();

        $branches = $client->branches;

        return view('module.admin.client.customers.create', compact('client','branches','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $this->customerRepository->validateCustomerFields($request);

        $customer = new Customer();
        $customer = $this->customerRepository->fillCustomerRecords($request, $customer, $client);

        return redirect()->route('admin.clients.customers.create', $client->id)
                         ->withSuccess("Employee creation of '{$customer->name}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client, Customer $customer)
    {
        $countries = Countries::get();

        $branches = $client->branches;

        return view('module.admin.client.customers.edit', compact('client','customer','branches','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client, Customer $customer)
    {
        $this->customerRepository->validateCustomerFields($request);

        $customer = $this->customerRepository->fillCustomerRecords($request, $customer, $client);

        return redirect()->route('admin.clients.customers', $client->id)
                         ->withSuccess("Employee update of '{$customer->name}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Client $client, Customer $customer)
    {
        $customer = $client->customers()->find($customer->id);

        $name = $customer->name;
        $customer->delete();

        return redirect()->route('admin.clients.customers', $client->id)
                         ->withWarning("Customer '{$name}' deleted successfully");
    }
}
