<?php

namespace App\Http\Controllers\Admin;

use App\Repository\Attribute\Attribute;
use App\Repository\Attribute\AttributeRepository;
use App\Repository\Client\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientAttributeController extends Controller
{
    public function __construct(AttributeRepository $attributeRepository)
    {
        $this->attributeRepository = $attributeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Client $client)
    {
        $attributes = $client->attributes->sortByDesc('created_at');

        return view('module.admin.client.attribute.index', compact('client','attributes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $branches = $client->branches;

        return view('module.admin.client.attribute.create', compact('client','branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $this->attributeRepository->validateAttributeFields($request);

        $attribute = new Attribute();
        $attribute = $this->attributeRepository->fillAttributeRecords($request, $attribute, $client);

        return redirect()->route('admin.clients.attributes', $client->id)
            ->withSuccess("Attribute creation of '{$attribute->title}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client, Attribute $attribute)
    {
        $branches = $client->branches;

        return view('module.admin.client.attribute.edit', compact('client','attribute','branches'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client, Attribute $attribute)
    {
        $this->attributeRepository->validateAttributeFields($request);

        $attribute = $this->attributeRepository->fillAttributeRecords($request, $attribute, $client);

        return redirect()->route('admin.clients.attributes', $client->id)
            ->withSuccess("Attribute update of '{$attribute->title}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Client $client, Attribute $attribute)
    {
        $title = $attribute->title;
        $attribute->delete();

        return redirect()->route('admin.clients.attributes', $client->id)
            ->withWarning("Attribute deletion of '{$title}' was successful");
    }
}
