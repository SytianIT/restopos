<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\Repository\Client\Client;
use App\Repository\Role\RoleRepository;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientRoleController extends Controller
{
    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Client $client)
    {
        $roles = $client->roles;

        return view('module.admin.client.role.index', compact('client','roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $permissions = Permission::clientLevel()->orderBy('created_at', 'ASC')->get();

        return view('module.admin.client.role.create', compact('client','permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $this->roleRepository->validateRoleFields($request);

        $role = new Role();
        $role = $this->roleRepository->fillRoleRecord($request, $role, $client);

        return redirect()->route('admin.clients.roles', $client->id)
            ->withSuccess("Role creation of '{$role->title}' was successful");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client, Role $role)
    {
        $permissions = Permission::clientLevel()->orderBy('created_at', 'ASC')->get();

        return view('module.admin.client.role.edit', compact('client','role','permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $this->roleRepository->validateRoleFields($request);

        $role = $this->roleRepository->fillRoleRecord($request, $role, $client);

        return redirect()->route('admin.clients.roles', $client->id)
            ->withSuccess("Role update of '{$role->title}' was successful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Role $role)
    {
        //
    }
}
