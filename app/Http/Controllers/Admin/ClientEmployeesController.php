<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repository\Client\Client;
use App\Repository\Employee\Employee;
use App\Repository\Employee\EmployeeRepository;
use App\Repository\Etc\Countries;
use Illuminate\Http\Request;

class ClientEmployeesController extends Controller
{
    protected $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Client $client)
    {
        $query = $client->employees();
        $employees = $this->employeeRepository->filter($request, $query, 'collection');

        return view('module.admin.client.employee.index', compact('client','employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Client $client)
    {
        $countries = Countries::orderBy('name')->get();
        $branches = $client->branches;

        return view('module.admin.client.employee.create', compact('client','branches','countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Client $client)
    {
        $this->employeeRepository->validateEmployeeFields($request);

        $employee = new Employee;
        $employee = $this->employeeRepository->fillEmployeeRecords($request, $employee, $client);

        return redirect()->route('admin.clients.employees.create', $client->id)
                         ->withSuccess("Employee creation of '{$employee->name}' was successfull");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client, Employee $employee)
    {
        $countries = Countries::orderBy('name')->get();
        $branches = $client->branches;

        return view('module.admin.client.employee.edit', compact('employee','client','branches','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client, Employee $employee)
    {
        $this->employeeRepository->validateEmployeeFields($request);

        $employee = $this->employeeRepository->fillEmployeeRecords($request, $employee, $client);

        return redirect()->route('admin.clients.employees', $client->id)
                         ->withSuccess("Employee update of '{$employee->name}' was successfull");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Client $client, Employee $employee)
    {
        $employee = $client->employees()->find($employee->id);

        $name = $employee->name;
        $employee->delete();

        return redirect()->route('admin.clients.employees', $client->id)
                         ->withWarning("Employee '{$name}' deleted successfully");
    }
}
