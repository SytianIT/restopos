<?php

namespace App\Repository\Stock;

use App\Repository\Client\Client;
use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use App\Repository\Item\Item;
use App\Repository\Item\ItemRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StockTransferRepository extends ContextRepository
{
    public function __construct(
        StockTransfer $stockTransfer,
        ContextInterface $context,
        StockBalanceRepository $stockBalanceRepository)
    {
        $this->model = $stockTransfer;
        $this->context = $context;
        $this->user = Auth::user();
        $this->stockBalanceRepo = $stockBalanceRepository;
    }

    public function search(Request $request, $with = [])
    {
        $query = $this->scope($with);
        $query = $this->filter($request, $query);

        return $query->paginate(self::PAGINATE);
    }

    public function filter(Request $request, $query, $returnType = 'query')
    {
        if($request->has('filter_branch')){
            $query = $query->where('branch_id', $request->filter_branch);
        }

        if($request->has('active')){
            $query = $query->active();
        }

        if($request->has('history')){
            $query = $query->history();
        }

        if($returnType == 'collection'){
            return $query->paginate(50);
        }

        return $query; // return type is Query
    }

    public function stockTransferLocationProducts($stockTransfer)
    {
        $stocks = StockBalance::where([
                    ['branch_id', '=', $stockTransfer->fromBranch->id],
                    ['stock_location_id', '=', $stockTransfer->fromLocation->id]
                ])->whereHas('item', function($q){
                    $q->with('category')->where('active', true);
                })->with('item')->get();

        return $stocks;
    }

    public function transferStocks($stockTransfer)
    {
        $branch = $stockTransfer->fromBranch;
        $location = $stockTransfer->fromLocation;
        $stockTransferItems = $stockTransfer->stockTransferItems;
        $deductingItems = [];

        // Combine first the like items in transfer
        foreach ($stockTransferItems as $transferItem){
            $item = $transferItem->item;
            $deductingItems[$item->id]['item'] = $item;
            $deductingItems[$item->id]['qty'] = isset($deductingItems[$item->id]['qty']) ? $deductingItems[$item->id]['qty'] + $transferItem->qty : $transferItem->qty;
        }

        $success = true;
        foreach($deductingItems as $deducting){
            $passed = true;
            //$strictTracking = $context->getSettings('strict_tracking_inventory');
            //if($strickTracking){
            //   $stock = $this->stockBalanceRepo->getStock($deducting['item'], $location, $branch);
            //   $flag = $this->stockBalanceRepo->checkIfSufficientQty($stock, $deducting['qty']);
            //   $passed = $flag['success'];
            //}

            if(!$passed){
                return $this->createResponse(false, 'Transfer failed, please check the remaining quantities of item in your inventory.');
            }

            $response = $this->stockBalanceRepo->deductItemBalance($deducting['item'], $deducting['qty'], $location, $branch);
            if($response['success'] == false){
                return $response;
            }
        }

        return $this->createResponse(true, 'Transfer was made successfully');
    }

    public function receiveStocks($request, $stockTransfer)
    {
        $stockTransferItems = $stockTransfer->stockTransferItems;
        foreach($stockTransferItems as $transferItem){
            $response = $this->stockBalanceRepo->replenishItemBalance($transferItem->item, $transferItem->qty, $stockTransfer->toLocation, $stockTransfer->toBranch);
        }

        $stockTransfer->status = StockTransfer::COMPLETED;
        $stockTransfer->receivedBy()->associate($request->user());
        $stockTransfer->save();

        return true;
    }

    public function cancelStocks($stockTransfer)
    {
        $stockTransferItems = $stockTransfer->stockTransferItems;
        if($stockTransfer->isProcessing()){
            foreach($stockTransferItems as $transferItem){
                $response = $this->stockBalanceRepo->replenishItemBalance($transferItem->item, $transferItem->qty, $stockTransfer->toLocation, $stockTransfer->toBranch);
            }
        }

        $stockTransfer->status = StockTransfer::CANCELLED;
        $stockTransfer->save();

        return true;
    }

    public function addItem($request, $stockTransfer)
    {
        $branch = $stockTransfer->fromBranch;
        $location = $stockTransfer->fromLocation;
        $item = $request->has('item_id') ? Item::find($request->item_id) : null;

        $stock = $this->stockBalanceRepo->getStock($item, $location, $branch);
        $response = $this->stockBalanceRepo->checkIfSufficientQty($stock, $request->quantity);

        if($response['success']){
            $transferItem = new StockTransferItem();
            $transferItem->stock_transfer_id = $stockTransfer->id;
            $transferItem->item_id = $item->id;
            $transferItem->uom_id = $item->uom->id;
            $transferItem->qty = $request->quantity;
            $transferItem->save();

            return $this->createResponse(true);
        }

        return $this->createResponse(false, "There's only {$response['remaining']} {$stock->item->uom->title} remaining quantity of {$stock->item->title} in inventory.");
    }

    public function removeItem($stockTransferItem, $stockTransfer)
    {
        $branch = $stockTransfer->fromBranch;
        $location = $stockTransfer->fromLocation;
        $item = $stockTransferItem->item;

        $response = $this->stockBalanceRepo->replenishItemBalance($item, $stockTransferItem->qty, $location, $branch);

        return $response;
    }

    public function fillStockTransferRecords(Request $request, StockTransfer $stockTransfer)
    {
        $stockTransfer->user()->associate($request->user());
        $stockTransfer->datetime = new Carbon($request->datetime);
        $stockTransfer->remarks = $request->remarks;
        $stockTransfer->branch()->associate($request->branch_id);
        $stockTransfer->fromBranch()->associate($request->branch_id);
        $stockTransfer->fromLocation()->associate($request->from_location_id);
        $stockTransfer->toBranch()->associate($request->to_branch_id);
        $stockTransfer->toLocation()->associate($request->to_location_id);
        $stockTransfer->status = StockTransfer::PENDING;
        $stockTransfer->save();

        return $stockTransfer;
    }
    
    public function validateStockTransferFields(Request $request, StockTransfer $stockTransfer = null)
    {
        $basicFields = [
            'user' => 'required',
            'datetime' => 'required|date',
            'branch_id' => 'required',
            'from_location_id' => 'required',
            'to_branch_id' => 'required',
            'to_location_id' => 'required'
        ];

        $this->validate($request,
            $basicFields,
            [
                'user.required' => 'This field is required',
                'datetime.required' => 'Please pick the transfer date',
                'branch_id.required' => 'Choose a branch',
                'from_location_id.required' => 'Choose the branch stock location',
                'to_branch_id.required' => 'Choose a branch',
                'to_location_id.required' => 'Choose the branch stock location',
            ]
        );

        return false;
    }
}
