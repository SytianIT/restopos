<?php

namespace App\Repository\Stock;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use App\Repository\StockLocation\StockLocation;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockTransfer extends BaseModel implements ContextScope
{
    use SoftDeletes;

    public static $statuses = [
        self::PENDING => 'Pending',
        self::IN_PROCESS => 'In Process',
        self::COMPLETED => 'Completed',
        self::CANCELLED => 'Cancelled'
    ];

    protected $table = 'stock_transfers';

    protected $dates = ['deleted_at', 'datetime', 'received_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function stockTransferItems()
    {
        return $this->hasMany(StockTransferItem::class, 'stock_transfer_id');
    }

    // same as @fromBranch
    public function branch()
    {
        return $this->belongsTo(Branch::class);
    }

    public function fromBranch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function toBranch()
    {
        return $this->belongsTo(Branch::class, 'to_branch_id');
    }


    public function fromLocation()
    {
        return $this->belongsTo(StockLocation::class, 'from_location_id');
    }

    public function toLocation()
    {
        return $this->belongsTo(StockLocation::class, 'to_location_id');
    }

    public function receivedBy()
    {
        return $this->belongsTo(User::class, 'receive_by');
    }

    /**
     * Mutators, Getters and Setters
     */

    public function getStatusNameAttribute()
    {
        if (!isset($this->statuses[$this->status])) return $this->status;
        return $this->statuses[$this->status];
    }

    public function isPending()
    {
       return $this->status == self::PENDING;
    }

    public function isProcessing()
    {
        return $this->status == self::IN_PROCESS;
    }

    public function isCompleted()
    {
        return $this->status == self::COMPLETED;
    }

    /**
     * Scope Queries
     */
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('branch', function($q) use ($context) {
                return $q->where('client_id', $context->id());
            });
        } else if ($context->getInstance() instanceof Branch) {
            return $query->where(function($query) use ($context) {
                $query->where($context->column(), $context->id())
                    ->orWhere('to_branch_id', $context->id());
            });
        }

        return $query;
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::PENDING)
                     ->orWhere('status', self::IN_PROCESS);
    }

    public function scopeHistory($query)
    {
        return $query->where('status', self::CANCELLED)
                     ->orWhere('status', self::COMPLETED);
    }
}
