<?php

namespace App\Repository\Stock;

use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use App\Repository\Item\Item;
use Illuminate\Http\Request;

class StockBalanceRepository extends ContextRepository
{
    public function __construct(
        StockBalance $stockBalance,
        ContextInterface $context)
    {
        $this->stockBalance = $stockBalance;
        $this->context = $context;
    }

    public function getStock($item, $location, $branch)
    {
        return $this->stockBalance->where([
            ['item_id', '=', $item->id],
            ['stock_location_id', '=', $location->id],
            ['branch_id', '=', $branch->id]
        ])->first();
    }
    
    /**
     * This function used to deduct an amount to the inventory of certain item
     * Currently used by: (StockTransfer)
     * @param Request $request
     * @param $location
     * @param $branch
     */
    public function deductItemBalance($item, $quantity, $location, $branch)
    {
        if($item->tracked){
            $stock = $this->getStock($item, $location, $branch);
            $stock->amount = $stock->amount - $quantity;
            $stock->save();

            return $this->createResponse(true, "{$quantity} {$item->uom->title} was removed from {$item->title} inventory.");
        }
    }

    public function replenishItemBalance($item, $quantity, $location, $branch)
    {
        $stock = $this->getStock($item, $location, $branch);
        $stock->amount = $stock->amount + $quantity;
        $stock->save();

        return $this->createResponse(true, "{$quantity} {$item->uom->title} was replenish for {$item->title} inventory.");
    }

    public function checkIfSufficientQty($stock, $quantity)
    {
        if($quantity > $stock->amount){
            $amount = number_format($stock->amount);
            return $this->createResponse(false, "Insufficient stock", ['remaining' => number_format($stock->amount)]);
        }

        return $this->createResponse(true);
    }
}
