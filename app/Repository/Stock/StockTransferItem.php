<?php

namespace App\Repository\Stock;

use App\Repository\BaseModel;
use App\Repository\Etc\Uom;
use App\Repository\Item\Item;
use App\Repository\StockLocation\StockLocation;
use App\User;

class StockTransferItem extends BaseModel
{
    protected $table = 'stock_transfer_items';

    public function uom()
    {
        return $this->belongsTo(Uom::class, 'uom_id');
    }

    public function stockTransfer()
    {
        return $this->belongsTo(StockTransfer::class, 'stock_transfer_id');
    }

    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
