<?php

namespace App\Repository\Stock;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Item\Item;
use App\Repository\StockLocation\StockLocation;

class StockBalance extends BaseModel
{
    protected $table = 'stock_balance';
    
    protected $dates = ['date','created_at','updated_at'];

    /**
     * @return object of Branch class
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    /**
     * @return object of StockLocation class
     */
    public function stockLocation()
    {
        return $this->belongsTo(StockLocation::class, 'stock_location_id');
    }
    
    /**
     * @return object of Item class
     */
    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
}
