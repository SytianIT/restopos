<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 3/8/2017
 * Time: 10:10 AM
 * Description: Interface use for Context Repository to initialize the model for use of scope query
 */

namespace App\Repository;

use Illuminate\Database\Eloquent\Model;

interface ContextInterface
{
    public function set(Model $model);

    public function getInstance();

    public function has();

    public function id();

    public function column();

    public function table();
}