<?php

namespace App\Repository\Role;

use App\Repository\BaseRepository;

class RoleRepository extends BaseRepository
{
    /**
     * Method to change records call by update and store
     * @param  Request $request
     * @param  Role $role
     * @return object
     */
    public function fillRoleRecord($request, $role, $client = null)
    {
    	$slug = str_slug($request->title, '-');

        $role->name = 'test';
    	$role->title = $request->title;
    	$role->description = $request->description;

    	// Save this role to this client
    	if($client != null){
            $role->client()->associate($client->id);
        }

    	$role->save();
    	$role->permissions()->sync(!empty($request->permissions) ? $request->permissions : []);

    	$role->name = $this->setSlugAttribute($role->title, $role->id, $role);
    	$role->save();

    	return $role;
    }

    public function validateRoleFields($request)
    {
        $basicFields = [
            'title' => 'required',
            'permissions' => 'required'
        ];

        $this->validate($request, $basicFields, [
            'title.required' => 'You need to provide this role title.',
            'permissions.required' => 'You have not selected any permissions for this role'
        ]);
    }
}
