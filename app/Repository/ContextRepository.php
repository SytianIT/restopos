<?php

namespace App\Repository;

abstract class ContextRepository extends BaseRepository
{
    const PAGINATE = 25;
    /**
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var App\Repository\Context
     */
    protected $context;

    /**
     * Method Scope for filtering Queries base on current ContextScope
     * use for separation of client records and branches
     *
     * @param array $with
     * @return mixed
     */
    public function scope($with = [])
    {
        $query = $this->model->with($with);
        if ($this->context->has()) {
            if ($this->model instanceof ContextScope) {
                $query = $query->ofContext($this->context);
            } else {
                $query = $query->where($this->context->column(), $this->context->id());
            }
        }

        $classes = class_uses($this->model, false);
        if ($classes && in_array(SortableTrait::class, $classes)) {
            $query = $query->sorted();
        }
        return $query;
    }

    // Other filter queries that uses Scope Query
    public function find($id, $with = array())
    {
        return $this->scope($with)->find($id);
    }

    public function findOrFail($id, $with = array())
    {
        return $this->scope($with)->find($id) ?: abort(404);
    }

    public function findTrashed($id)
    {
        return $this->scope()->onlyTrashed()->find($id);
    }

    public function findDontAbort($id, $with = array())
    {
        return $this->scope($with)->find($id);
    }

    public function findWithTrash($id)
    {
        return $this->scope()->withTrashed()->find($id);
    }

    public function all($with = array())
    {
        return $this->scope($with)->get();
    }

    public function allWithTrashed($with = array())
    {
        return $this->scope($with)->withTrashed()->get();
    }

    public function allTrashed($with = array())
    {
        return $this->scope($with)->onlyTrashed()->get();
    }

    public function getAllBy($key, $value, $with = array())
    {
        $collection = $this->scope($with)->where($key, $value)->get();

        return $collection;
    }

    public function getFirstBy($key, $value, $with = array())
    {
        return $this->scope()->where($key, $value)->first();
    }

    public function getTrashedFirstBy($key, $value, $with = array())
    {
        return $this->scope()->onlyTrashed()->where($key, $value)->first();
    }
}
