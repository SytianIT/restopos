<?php

namespace App\Repository\Client;

use App\Repository\Attribute\Attribute;
use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Customer\Customer;
use App\Repository\Employee\Employee;
use App\Repository\Item\Item;
use App\Repository\Item\ItemCategory;
use App\Repository\Supplier\Supplier;
use App\Role;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends BaseModel
{
    use SoftDeletes;

    /**
     * The attributes that declared in date formats
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attributes()
    {
        return $this->hasMany(Attribute::class, 'client_id');
    }

    /**
     * Relation of one to many for Branch class
     * @return object of branches assigned to this client
     */
    public function branches()
    {
        return $this->hasMany(Branch::class, 'client_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function customers()
    {
        return $this->hasMany(Customer::class, 'client_id');
    }

    /**
     * Eloquent relation for 'one to many' of Employee class
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employees()
    {
        return $this->hasMany(Employee::class, 'client_id');
    }

    /**
     * Relation of one to many for Supplier class
     * @return object of supplier assigned to this client
     */
    public function itemCategories()
    {
        return $this->hasMany(ItemCategory::class, 'client_id');
    }

    /**
     * Eloquent relation for 'one to mane' of Product class
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(Item::class, 'client_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->hasMany(Role::class, 'client_id');
    }

    /**
     * Relation of one to many for Supplier class
     * @return object of supplier assigned to this client
     */
    public function suppliers()
    {
        return $this->hasMany(Supplier::class, 'client_id');
    }

    /**
     * Relation of one to many for User class
     * @return object of users assigned to this client
     */
    public function users()
    {
    	return $this->hasMany(User::class, 'client_id');
    }
    
    // Getters And Setters
    /**
     * Make Avatart Column for Client
     * @return String path of image
     */
    public function getAvatarAttribute()
    {
        return $this->company_logo != null ? asset($this->company_logo) : asset('images/gravatar.jpg');
    }
}
