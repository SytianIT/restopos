<?php

namespace App\Repository\Supplier;

use App\Repository\BaseRepository;
use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use Illuminate\Http\Request;

class SupplierRepository extends ContextRepository
{
    public function __construct(
        Supplier $supplier,
        ContextInterface $context)
    {
        $this->model = $supplier;
        $this->context = $context;
    }

    public function search(Request $request, $with = [])
    {
        $query = $this->scope($with);
        $query = $this->filter($request, $query);

        return $query->paginate(self::PAGINATE);
    }

    public function filter(Request $request, $query, $returnType = 'query')
    {
        // Filter process's here!!

        if($returnType == 'collection'){
            return $query->paginate(50);
        }

        return $query; // return type is Query
    }

	public function fillSupplierRecords($request, $supplier, $client)
	{
    	$supplier->title = $request->title;
    	$supplier->address = $request->address;
    	$supplier->notes = $request->notes;

        // Primary Contact Records
        $supplier->c_first_name = $request->c_first_name;
        $supplier->c_last_name = $request->c_last_name;
        $supplier->c_mobile = $request->c_mobile;
        $supplier->c_telephone = $request->c_telephone;
        $supplier->c_email = $request->c_email;
    	$supplier->client_id = $client->id;
    	
    	$supplier->save();
    	
    	$supplier->branches()->sync($request->branch);
    	$supplier->setName($supplier->title);

    	return $supplier;
	}

    public function validateSupplierFields($request)
    {
		$this->validate($request, 
		[
			'title' => 'required',
			//'address' => 'required',
			'branch' => 'required',
			'c_first_name' => 'required',
            'c_last_name' => 'required',
			//'c_email' => 'required',
			//'c_mobile' => 'required'
		]
		,[
			'title.required' => 'Provide the name of the supplier.',
			'address.required' => 'Provide the address of the supplier.',
			'branch.required' => 'Choose the branch(es) for this supplier.',
			'c_first_name.required' => 'Please provide the first name of the contact person.',
            'c_last_name.required' => 'Please provide the last name of the contact person.',
			'c_email.required' => 'Provide the E-Mail of the contact number.',
			'c_mobile.required' => 'Provide the mobile number of the contact number.'
		]);

        return;
    }
}
