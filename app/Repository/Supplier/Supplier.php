<?php

namespace App\Repository\Supplier;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;

class Supplier extends BaseModel implements ContextScope
{
	/**
     * Relation of one to one for Client class
     * @return collection of client assigned to this supplier
     */
    public function client()
    {
    	return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Relation of many to many for Client class
     * @return collection of client assigned to this supplier
     */
    public function branches()
    {
    	return $this->belongsTomany(Branch::class, 'branch_suppliers', 'supplier_id', 'branch_id');
    }

    /**
     * Scope Queries
     */
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where('client_id', $context->id());
        } else if ($context->getInstance() instanceof Branch) {
            return $query->whereHas('branches', function ($query) use ($context) {
                $query->whereId($context->id());
            });
        }

        return $query;
    }
}
