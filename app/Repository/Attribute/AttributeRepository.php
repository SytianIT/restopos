<?php

namespace App\Repository\Attribute;

use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use Illuminate\Support\Collection;

class AttributeRepository extends ContextRepository
{
    public function __construct(
        Attribute $attribute,
        ContextInterface $context
    )
    {
        $this->model = $attribute;
        $this->context = $context;
    }

    public function fillAttributeRecords($request, $attribute, $client)
    {
        $attribute->title = $request->title;
        $attribute->description = $request->description;
        $attribute->client_id = $client->id;

        $attribute->save();

        $attribute->branches()->sync($request->branch);

        $this->createAttributeOptions($attribute, $request->options);

        return $attribute;
    }

    public function validateAttributeFields($request)
    {
        $this->validate($request,
            [
                'title' => 'required',
                'branch' => 'required'
            ]
            ,[
                'title.required' => 'Provide the title of the category.',
                'branch.required' => 'Please select branch(es) to have this attribute.'
            ]);

        return;
    }

    /**
     * @param Attribute $attribute
     * @param Array $options
     */
    public function createAttributeOptions($attribute, $options = [])
    {
        if(count($options) > 1){
            $newIds = [];
            foreach($options as $key => $option){

                $attributeOpt =  isset($option['id']) ? AttributeOption::find($option['id']) : new AttributeOption();

                $attributeOpt->title = $option['title'];
                $attributeOpt->attribute_id = $attribute->id;
                $attributeOpt->save();

                $newIds[] = $attributeOpt->id;
            }
            // Delete old attribute options if any.
            $this->deleteAttributeOptions($attribute, $newIds);
        }

        return;
    }

    private function deleteAttributeOptions($attribute, $newIds)
    {
        $oldIds = $attribute->options->pluck('id')->toArray();
        $forDeleteIds = array_diff($oldIds, $newIds);

        foreach ($forDeleteIds as $id){
            $option = AttributeOption::find($id);
            if($option != null){
                $option->delete();
            }
        }
    }
    
    // Other Getter Methods
    public function getAttributesFromRequest($request)
    {
        if($request->has('item_attributes')) {
            $attributes = new Collection();
            foreach ($request->item_attributes as $attribute) {
                $attribute = $this->find($attribute)->load('options');
                $attributes = $attributes->push($attribute);
            }

            return $attributes;
        }
    }

    public function getAttributeOptions($attributes)
    {
        $options = new Collection();
        foreach($attributes as $attribute){
            $options = $options->merge($attribute->options);
        }

        return $options;
    }
}
