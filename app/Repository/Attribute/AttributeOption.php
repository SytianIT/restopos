<?php

namespace App\Repository\Attribute;

use App\Repository\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttributeOption extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * @return object of Attribute class
     *
     */
    public function attribute()
    {
        return $this->belongsTo(Attribute::class, 'attribute_id');
    }
}
