<?php

namespace App\Repository\Attribute;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use App\Repository\Item\Item;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attribute extends BaseModel implements ContextScope
{
    use SoftDeletes;

    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * @return collection of Branch class
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'branch_attributes', 'attribute_id', 'branch_id');
    }

    /**
     * @return object of Client class
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return object of Item class
     */
    public function items()
    {
        return $this->belongsToMany(Item::class, 'item_attributes', 'attribute_id', 'item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany(AttributeOption::class, 'attribute_id');
    }

    /**
     * Scope Queries
     */

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where('client_id', $context->id());
        } else if ($context->getInstance() instanceof Branch) {
            return $query->whereHas('branches', function ($query) use ($context) {
                $query->whereId($context->id());
            });
        }

        return $query;
    }
}
