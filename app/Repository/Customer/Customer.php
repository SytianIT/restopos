<?php

namespace App\Repository\Customer;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends BaseModel implements ContextScope
{
    use SoftDeletes;

    protected $dates = ['deleted_at','created_at','updated_at'];

    protected $casts = ['settings' => 'array'];

    /**
     * @return object of Client class
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * @return object of  class
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    /**
     * Setters * Getters
     */
    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getAvatarAttribute()
    {
        return $this->image != null ? asset($this->image) : asset('images/gravatar.jpg');
    }

    /**
     * Scope Queries
     */

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where('client_id', $context->id());
        } else if ($context->getInstance() instanceof Branch) {
            return $query->where('branch_id', $context->id());
        }

        return $query;
    }
}
