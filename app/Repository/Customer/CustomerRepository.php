<?php

namespace App\Repository\Customer;

use App\Repository\BaseRepository;
use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CustomerRepository extends ContextRepository
{
    public function __construct(
        Customer $customer,
        ContextInterface $context
    )
    {
        $this->model = $customer;
        $this->context = $context;
    }

    public function search(Request $request, $with = [])
    {
        $query = $this->scope($with);
        $query = $this->filter($request, $query);

        return $query->paginate(self::PAGINATE);
    }

    public function filter(Request $request, $query, $returnType = 'query')
    {
        // Filter process's here!!
        $query = $query->orderBy('first_name','ASC');

        if($returnType == 'collection'){
            return $query->paginate(50);
        }

        return $query; // return type is Query
    }

    public function fillCustomerRecords($request, $customer, $client)
    {
        $customer->first_name = $request->first_name;
        $customer->last_name = $request->last_name;
        $customer->gender = $request->gender;
        $customer->birthdate = (
            $request->has('b_year') &&
            $request->has('b_month') &&
            $request->has('b_day') ? Carbon::create($request->b_year, $request->b_month, $request->b_day) : null
        );
        $customer->occupation = $request->occupation;
        $customer->email = $request->email;
        $customer->mobile = $request->mobile;
        $customer->telephone = $request->telephone;
        $customer->address_1 = $request->address_1;
        $customer->address_2 = $request->address_2;
        $customer->city = $request->city;
        $customer->country = $request->country;
        $customer->postal_code = $request->postal_code;
        $customer->notes = $request->notes;
        $customer->receive_email_notification = $request->has('receive_email_notification') ? true : false;
        $customer->receive_sms_notification = $request->has('receive_sms_notification') ? true : false;


        $customer->client()->associate($client);
        $customer->branch()->associate($request->branch);

        if($request->hasFile('image')){
            $customer->image = $this->baseUpload($request->file('image'), 'uploads/customers/profile', 450, 450);
        }

        $customer->save();

        return $customer;
    }

    /**
     * Method to validate customer fields before inserting into database
     * @param $request
     */
    public function validateCustomerFields($request)
    {
        $basicFields = [
            'first_name' => 'required',
            'branch' => 'required',
            'gender' => 'required'
        ];

        if($request->hasFile('image')){
            $imageFields = [
                'image' => 'image|max:10000'
            ];

            $basicFields = array_merge($basicFields, $imageFields);
        }

        $messageFields = [
            'first_name' => 'Provide the first name of the employee.',
            'branch' => 'Provide the assignment branch.',
            'gender' => 'Select the gender.',
            'mobile' => 'Provide the mobile number.',
            'address_1' => 'Provide the address.',
            'city' => 'Provide the city.',
        ];

        $this->validate($request, $basicFields, $messageFields);
    }
}
