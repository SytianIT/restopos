<?php
/**
 * Created by PhpStorm.
 * User: jr
 * Date: 3/8/2017
 * Time: 2:15 PM
 */

namespace App\Repository;

use App\Repository\ContextInterface;

interface ContextScope
{
    public function scopeOfContext($query, ContextInterface $context);
}