<?php

namespace App\Repository\StockLocation;

use App\Repository\BaseRepository;
use App\Repository\ContextInterface;
use App\Repository\ContextRepository;

class StockLocationRepository extends ContextRepository
{
    public function __construct(
        StockLocation $stockLocation,
        ContextInterface $context)
    {
        $this->model = $stockLocation;
        $this->context = $context;
    }

	public function fillStockLocationRecords($request, $stockLocation)
	{
		$stockLocation->location_title = $request->location_title;
		$stockLocation->remarks = $request->remarks;
		$stockLocation->branch_id = $request->branch;

		$stockLocation->save();
		$stockLocation->setName($stockLocation->location_title);

		return $stockLocation;
	}

    public function validateStockLocationFields($request)
    {
    	$this->validate($request, 
    	[
    		'location_title' => 'required',
    		'branch' => 'required'
    	]
    	,[
            'location_title' => 'Provide the name of stock location',
    		'branch' => 'Choose the branch that will use this stock location'
        ]);

        return;
    }
}
