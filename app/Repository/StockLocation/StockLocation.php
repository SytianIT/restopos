<?php

namespace App\Repository\StockLocation;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockLocation extends BaseModel implements ContextScope
{

    const LOCATION_DEFAULT = 'stock-location-default';
   
    use SoftDeletes;

    // Declare fillable columns
    protected $fillable = ['name', 'location_title'];

    // Declare date columns
    protected $dates = ['created_at','deleted_at','updated_at'];

    public function branch()
    {
    	return $this->belongsTo(Branch::class, 'branch_id');
    }

    /**
     * Scope Queries
     */
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('branches', function ($query) use ($context) {
                $query->whereClientId($context->id());
            });
        } else if ($context->getInstance() instanceof Branch) {
            return $query->where('branch_id', $context->id());
        }

        return $query;
    }
}
