<?php

namespace App\Repository\Product;

use App\Repository\BaseModel;
use App\Repository\Product\ProductCategory;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $casts = ['settings' => 'array'];

    /**
     * Eloquent relation for 'one to one' of Category class
     * @return object of Category class
     */
    public function category()
    {
    	return $this->belongsTo(ProductCategory::class, 'category_id');
    }
}
