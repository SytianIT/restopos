<?php

namespace App\Repository\Product;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends BaseModel
{
	use SoftDeletes;

    protected $table = "prod_categories";

    // Declare date columns
    protected $dates = ['created_at','deleted_at','updated_at'];

    protected $casts = ['settings' => 'array'];

    /**
     * Relation of many to one for Client class
     * @return object of client assigned to this category
     */
    public function client()
    {
    	return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Relation of many to many for Product class
     * @return collection of products that has this category
     */
    public function products()
    {
    	// return $this->belongsToMany(Branches::class, 'prod_categories_products', 'category_id', 'branch_id');
    }

    /**
     * Relation of many to many for Branch class
     * @return collection of branch that has this category
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'prod_categories_branches', 'category_id', 'branch_id');
    }

    // Scope Queries
    public function scopeParents()
    {
        return $this->whereNull('parent_id');
    }
}
