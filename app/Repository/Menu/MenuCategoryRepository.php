<?php

namespace App\Repository\Menu;

use App\Repository\BaseRepository;

class MenuCategoryRepository extends BaseRepository
{
    public function fillCategoryRecords($request, $category, $client)
	{
    	$category->title = $request->title;
    	$category->notes = $request->notes;
    	$category->client_id = $client->id;
    	$category->parent_id = $request->has('parent_id') ? $request->parent_id : null;
    	
    	$category->save();
    	
    	$category->branches()->sync($request->branch);
    	$category->setName($category->title);

    	return $category;
	}

    public function validateCategoryFields($request)
    {
		$this->validate($request, 
		[
			'title' => 'required',
			'branch' => 'required'
		]
		,[
			'title.required' => 'Provide the title of the category.',
			'branch.required' => 'Please select branch(es) to have this category.'
		]);

        return;
    }
}
