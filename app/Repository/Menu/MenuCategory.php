<?php

namespace App\Repository\Menu;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuCategory extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at','created_at','updated_at'];

    protected $casts = ['settings' => 'array'];

    /**
     * Eloquent relation for 'one to many' of Client class
     * @return collection of Client class
     */
    public function client()
    {
    	return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Eloquent relation for 'one to mane' of Branch class
     * @return collection of Branch class
     */
    public function branches()
    {
    	return $this->belongsToMany(Branch::class, 'menu_categories_branches', 'category_id', 'branch_id');
    }

    // Scope Queries
    public function scopeParents()
    {
        return $this->whereNull('parent_id');
    }
}
