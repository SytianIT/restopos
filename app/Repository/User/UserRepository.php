<?php

namespace App\Repository\User;

use App\Repository\BaseRepository;
use App\Role;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserRepository extends BaseRepository
{
    /**
     * Method to call within this class to update or store user basic fields
     * @param  $request
     * @param  $user
     * @return $user
     */
    public function fillUserRecords($request, $user, $client = null)
    {
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->middle_name = $request->middle_name;
        $user->contact_no = $request->contact_no;
        $user->email = $request->email;
        $user->username = $request->username;

        if($request->has('password')){
            $user->password = Hash::make($request->password);
        }

        if($request->hasFile('image')){
            $user->profile_img = $this->baseUpload($request->file('image'), 'uploads/users/user-pictures', 350, 350);
        }

        if($client != null){
            $user->client()->associate($client->id);
        }

        $user->save();
        
        // Branches assignment to users for client creation of users
        if($request->has('branches')){
            $user->branches()->sync([$request->branches]);
        }

        if($request->has('role_id')){
            $roles = (count($request->role_id) > 0) ? $request->role_id : [];
            $user->roles()->sync($roles);
        }

        return $user;
    }

     /**
     * Method use to validate request fields to create or update user records
     * @param  $request
     * @param  $user
     * @return Boolean          
     */
    public function validateUserFields($request, $user = null, $flags = null, $client = null)
    {
        $passwordFields = [
            'password' => 'required|max:15|min:6|confirmed',
            'password_confirmation' => 'required'
        ];

        $id = ($user != null) ? ','.$user->id : '';
        $basicFields = [
            'first_name' => 'required',
            'role_id' => 'required',
            'email' => 'required|email|unique:users,email'.$id,
            'username' => 'required|max:15',
        ];

        if($user != null){
            if($request->has('password')){
                $basicFields = array_merge($basicFields, $passwordFields);
            }
        }

        if($request->hasFile('image')){
            $basicFields = array_merge($basicFields, [
                'image' => 'required|image'
            ]);
        }

        if($user == null){
            $basicFields = array_merge($basicFields, $passwordFields);
        }

        // Other validations intended for client users creation
        if($flags == 'client'){
            if($request->role_id == Role::BRANCH_OWNER_ROLE){
                $clientFields = [
                    'branches' => 'required'
                ];

                $basicFields = array_merge($basicFields, $clientFields);
            }

            if($client != null){

                // Conditional insert of username validations
                $uniqueRule = Rule::unique('users')->where(function ($query) use ($client){
                    $query->where('client_id', '=', $client->id);
                });

                if($user != null){
                    $uniqueRule = $uniqueRule->ignore($user->id, 'id');
                }

                $clientUsernameFields = ['username' => [
                    'required',
                    'max:15',
                    $uniqueRule
                ]];

                $basicFields = array_merge($basicFields, $clientUsernameFields);
            }
        }

        $this->validate($request, $basicFields, [
            'role_id.required' => 'Please assign roles for this user.',
            'first_name.required' => 'Your first name is required',
            'last_name.required' => 'Your middle name is required',
        ]);

        return true;
    }
}
