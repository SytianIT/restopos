<?php

namespace App\Repository;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class BaseModel extends Model
{
    // Statuses Constants
    const PENDING = 'pending';
    const IN_PROCESS = 'in_process';
    const COMPLETED = 'completed';
    const CANCELLED = 'cancelled';

    public static $statusDisplay = [
        self::PENDING => 'label-gray',
        self::IN_PROCESS => 'label-yellow',
        self::COMPLETED => 'label-success',
        self::CANCELLED => 'label-warning'
    ];

    public static $statuses = [
        self::PENDING => 'Pending',
        self::IN_PROCESS => 'In Process',
        self::COMPLETED => 'Completed',
        self::CANCELLED => 'Cancelled'
    ];

	const DATE_FORMAT = 'd-M-y';
    const DEF_COUNTRY = 'Philippines';

    public function getSetting($name)
    {
    	$value = Arr::get($this->settings, $name);

    	if (is_array($value)) return $value;

    	return trim($value) ? trim($value) : null;
    }

    public function formatFormDate($attr = null) 
    {
        $carbon = $this->{$attr};
        return ($carbon instanceof \Carbon\Carbon) ? $carbon->format('m/d/Y') : null;
    }

    public function formatDate($attr = null) 
    {
        $carbon = $this->{$attr};

        return ($carbon instanceof \Carbon\Carbon) ? $carbon->format(self::DATE_FORMAT) : null;
    }

    public function getMyCountry($country)
    {
        if($this->country == $country->id){
            return 'selected';
        }

        return (self::DEF_COUNTRY == $country->name) ? 'selected' : '';
    }

    public function statusForHuman($attr = null)
    {
        $status = $attr ? $this->{$attr} : $this->status;

        return isset(self::$statusDisplay[$status]) ? '<label class="label label-status '.self::$statusDisplay[$status].'">'.self::$statuses[$status].'</label>' : 'n/a';
    }

    /**
     * Method globally used to make a slug name and look for the existence of it, in model pass
     * @param String $value 
     * @param Integer $id    
     * @param Class $model
      * @return String $slug
     */
    // public function setSlugAttribute($value, $id, $model) {
    public function setName($title = null) {
        $name = $title != null ? $title : $this->title;
        $name = str_slug($name, "-");

        $models = $this->whereRaw("name REGEXP '^{$name}(-[0-9]*)?$'")
                       ->where('id', '<>', $this->id);

        if ($models->count() === 0) {
            $this->name = $name;
            $this->save();

            return;
        }

        return $this->setName($name . '-' . $this->id);
    }

    // Global Getters and Setters
    /**
     * Method globally used to make a slug name and look for the existence of it, in model pass
     * @param String $value 
     * @param Integer $id    
     * @param Class $model 
     * @return String $slug
     */
    // public function setSlugAttribute($value, $id, $model) {
    public function getAvatarAttribute()
    {
        return $this->image != null ? asset($this->image) : asset('images/gravatar.jpg');
    }

    // Functions for nested columns (ProductCategory, MenuCategory)
    /**
     * Function generate an laravel attribute for Depth Name of child row
     * @return String Name with depth indicator
     */
    public function getDepthNameAttribute()
    {
        $name = $this->title;
        if($this->parent_id != null){
            $parent = $this->find($this->parent_id);
            $name = $this->iterateThroughThisParent($parent, $this->title, 1);
        }

        return $name;
    }

    /**
     * Function to get all child row of current model
     * @return collection child rows
     */
    public function children()
    {
        return $this->where('parent_id', '=', $this->id)->get();
    }

    /**
     * Helper function for getDepthNameAttribute function to generate depth names
     * @param   $parent  Parent model
     * @param   $title   Current title
     * @param   $counter Current counter
     * @return           Title with depth indicator
     */
    public function iterateThroughThisParent($parent, $title, $counter)
    {
        $newTitle = '';
        for($i = 1; $i <= $counter; $i++){
            $newTitle = ($newTitle != '') ? '— '.$newTitle : '— '.$title;
        }
        
        if($parent->parent_id != null){
            $upParent = $this->find($parent->parent_id);
            $newTitle = $this->iterateThroughThisParent($upParent, $title, $counter + 1); // '—'
        }

        return $newTitle;
    }
}
