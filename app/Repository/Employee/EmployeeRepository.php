<?php

namespace App\Repository\Employee;

use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use Illuminate\Http\Request;

class EmployeeRepository extends ContextRepository
{
    public function __construct(
        Employee $employee,
        ContextInterface $context
    )
    {
        $this->model = $employee;
        $this->context = $context;
    }

    public function search(Request $request, $with = [])
    {
        $query = $this->scope($with);
        $query = $this->filter($request, $query);

        return $query->paginate(self::PAGINATE);
    }

    public function filter(Request $request, $query, $returnType = 'query')
    {
        // Filter process's here!!

        if($returnType == 'collection'){
            return $query->paginate(50);
        }

        return $query; // return type is Query
    }


    public function fillEmployeeRecords($request, $employee, $client)
	{
		$employee->employee_number = $request->employee_number;
		$employee->first_name = $request->first_name;
		$employee->last_name = $request->last_name;
		$employee->gender = $request->gender;
		$employee->email = $request->email;
		$employee->mobile = $request->mobile;
		$employee->telephone = $request->telephone;
		$employee->address_1 = $request->address_1;
		$employee->address_2 = $request->address_2;
		$employee->city = $request->city;
		$employee->country = $request->country;
		$employee->postal_code = $request->postal_code;
		$employee->date_employed = new \Datetime($request->date_employed);
		$employee->employee_end_date = new \Datetime($request->employee_end_date);
		$employee->notes = $request->notes;


		$employee->client()->associate($client);
		$employee->branch()->associate($request->branch);

		if($request->hasFile('image')){
            $employee->image = $this->baseUpload($request->file('image'), 'uploads/employees/profile', 450, 450);
        }

        $employee->save();

		return $employee;
	}

    /**
     * Method to validate employee fields before inserting into database
     * @param $request
     */
    public function validateEmployeeFields($request)
    {
    	$basicFields = [
    		//'employee_number' => 'required|alpha_num|max:11',
    		'first_name' => 'required',
            'last_name' => 'required',
    		'branch' => 'required',
    		'gender' => 'required',
    		//'mobile' => 'required',
    		//'address_1' => 'required',
    		//'city' => 'required',
    		//'date_employed' => 'required',
    		//'employee_end_date' => 'required'
    	];

    	if($request->hasFile('image')){
    		$imageFields = [
    			'image' => 'image|max:10000'
    		];

    		$basicFields = array_merge($basicFields, $imageFields);
    	}
    	
    	$messageFields = [
    		'employee_number' => 'Please provide the employee number.',
    		'first_name' => 'Provide the first name of the employee.',
    		'branch' => 'Provide the assignment branch.',
    		'gender' => 'Select the gender.',
    		'mobile' => 'Provide the mobile number.',
    		'address_1' => 'Provide the address.',
    		'city' => 'Provide the city.',
    		'date_employed' => 'Pick the date the employee was hired.',
    		'employee_end_date' => 'Pick the employment end date.'
    	];

    	$this->validate($request, $basicFields, $messageFields);
    }
}
