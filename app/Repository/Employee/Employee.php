<?php

namespace App\Repository\Employee;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends BaseModel implements ContextScope
{
	use SoftDeletes;

	protected $dates = ['date_employed','employee_end_date','deleted_at','created_at','updated_at'];

	/**
	 * Eloquent relation for 'one to many' of Client class
	 * @return object of Client class
	 */
	public function client()
	{
		return $this->belongsTo(Client::class, 'client_id');
	}

	/**
	 * Eloquent relation for 'many to many' of Branch class
	 * @return Collection of Branch class
	 */
	public function branch()
	{
		return $this->belongsTo(Branch::class, 'branch_id');
	}

	/**
	 * Eloquent relation for 'many to one' of Country class
	 * @return object of Country class
	 */
	public function country()
	{
		return $this->belongsTo(Country::class, 'country');
	}

	/**
     * Setters * Getters
     */
    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getAvatarAttribute()
    {
        return $this->image != null ? asset($this->image) : asset('images/gravatar.jpg');
    }

    /**
     * Scope Queries
     */

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('branch', function ($query) use ($context) {
                $query->whereClientId($context->id());
            });
        } else if ($context->getInstance() instanceof Branch) {
            return $query->where('branch_id', $context->id());
        }

        return $query;
    }

}
