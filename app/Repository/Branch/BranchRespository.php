<?php

namespace App\Repository\Branch;

use App\Repository\BaseRepository;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;

class BranchRespository extends BaseRepository
{
    public function ()
    {

    }

    public function fillData($request, $branch, $client = null)
    {
        $branch->uuid = $branch->uuid != null ? $branch->uuid : Uuid::uuid5(Uuid::NAMESPACE_DNS, Carbon::now()->timestamp);
    	$branch->title = $request->title;
    	$branch->address_1 = $request->address_1;
    	$branch->address_2 = $request->address_2;
    	$branch->city = $request->city;
    	$branch->country = $request->country;
    	$branch->zip = $request->zip;
    	$branch->website = $request->website;
        $branch->telephone = $request->telephone;
        $branch->mobile = $request->mobile;
        // Primary Contact Records
        $branch->pc_first_name = $request->pc_first_name;
        $branch->pc_last_name = $request->pc_last_name;
        $branch->pc_mobile = $request->pc_mobile;
        $branch->pc_telephone = $request->pc_telephone;
        $branch->pc_email = $request->pc_email;
        // Secondary Contact Records
        $branch->sc_first_name = $request->sc_first_name;
        $branch->sc_last_name = $request->sc_last_name;
        $branch->sc_mobile = $request->sc_mobile;
        $branch->sc_telephone = $request->sc_telephone;
        $branch->sc_email = $request->sc_email;

        // Others
        $branch->date_created = new \DateTime($request->date_created);
        $branch->settings = $request->settings;
        $branch->subscription_expiration = new \DateTime($request->subscription_expiration);
        $branch->active = $request->has('active') ? true : false;

        if($request->hasFile('image')){
            $branch->company_logo = $this->baseUpload($request->file('image'), 'uploads/branches/logo', null, 50);
        }

        if($client != null){
            $branch->client()->associate($client);
        }

        $branch->save();
        $branch->setName();

        if($branch->stockLocations->count() == 0){
            $branch->createDefaultLocation();
        }

        return $branch;
    }

    public function validateBranchFields($request, $branch = null)
    {
        $id = ($branch != null) ? ','.$branch->id : '';
        $basicFields = [
            'title' => 'required',
            'address_1' => 'required|max:25',
            'city' => 'required',
            'country' => 'required',
            'zip' => 'required',
            'pc_first_name' => 'required',
            'pc_last_name' => 'required',
            'pc_email' => 'required',
            'pc_mobile' => 'required'
        ];

        if($request->hasFile('image')){
            $basicFields = array_merge($basicFields, [
                'image' => 'required|image'
            ]);
        }

        $this->validate($request, $basicFields, [
            'address_1.required' => 'Address cannot be empty.',
            'pc_first_name.required' => 'Please provide the primary contact first name.',
            'pc_last_name.required' => 'Please provide the primary contact last name.',
            'pc_email.required' => 'Please provide the primary contact email.',
            'pc_mobile.required' => 'Please provide the primary mobile number.',
        ]);
        return;
    }
}
