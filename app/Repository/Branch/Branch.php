<?php

namespace App\Repository\Branch;

use App\Repository\Attribute\Attribute;
use App\Repository\BaseModel;
use App\Repository\Client\Client;
use App\Repository\Employee\Employee;
use App\Repository\Item\Item;
use App\Repository\Item\ItemCategory;
use App\Repository\Stock\StockBalance;
use App\Repository\StockLocation\StockLocation;
use App\Repository\Table\Table;
use App\Repository\Table\TableSection;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends BaseModel
{
	use SoftDeletes;

	protected $casts = ['settings' => 'array'];

	protected $dates = ['date_created','created_at','updated_at','subscription_expiration'];
	
	/**
     * Relation of many to one for Client class
     * @return object of client assigned to this branch
     */
    public function client()
    {
    	return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Relation of many to many for User class
     * @return object of user assigned to this branch
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_branches', 'user_id', 'branch_id');
    }

    /**
     * Relation of one to many for StockLocation class
     * @return collection of stockLocations assigned to this branch
     */
    public function stockLocations()
    {
        return $this->hasMany(StockLocation::class, 'branch_id');
    }

    /**
     * Eloquent relation for 'many to many' of ItemCategory class
     * @return collection of ItemCategory class
     */
    public function itemCategories()
    {
        return $this->belongsToMany(ItemCategory::class, 'item_categories_branches', 'branch_id', 'category_id');
    }

    /**
     * ELoquent relation of 'many to many' of Item Class
     * @return collection of Item class
     */
    public function items()
    {
        return $this->belongsToMany(Item::class, 'item_branches', 'branch_id', 'item_id');
    }

    /**
     * @return collection of Attribute class
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'branch_attribute', 'branch_id', 'attribute_id');
    }

    /**
     * Eloquent relation for 'one to many' of Employee class
     * @return collection of Employee class
     */
    public function employees()
    {
        return $this->hasMany(Employee::class, 'branch_id');
    }
    
    /**
     * @return collection of Customer class
     */
    public function customers()
    {
        return $this->hasMany(Customer::class, 'branch_id');
    }

    /**
     * @return collection of Table class
     */
    public function tables()
    {
        return $this->hasMany(Table::class, 'branch_id');
    }
    
    /**
     * @return object of TableSection class
     */
    public function tableSections()
    {
        return $this->hasMany(TableSection::class, 'branch_id');
    }

    /**
     * @return object of StockBalance class
     */
    public function stockBalance()
    {
        return $this->hasMany(StockBalance::class, 'branch_id');
    }

    /**
     * Getters, Mutators & Setters
     */
    
    public function getAvatarAttribute()
    {
        return $this->company_logo != null ? asset($this->company_logo) : asset('images/gravatar.jpg');
    }

    public function getThisItemStockBalance($item)
    {
        $balance = $this->stockBalance()->where('item_id', $item->id)->first();

        if($balance->exists){
            return $balance;
        }

        return false;
    }

    /**
     * Other specialized functions
     */
    
    /**
     * Method to create a default stock location for newly created branch
     * @return StockLocation
     */
    public function createDefaultLocation()
    {
        $stockLocation = $this->stockLocations()->create([
            'name' => StockLocation::LOCATION_DEFAULT,
            'location_title' => 'Default Stock Location'
        ]);

        return $stockLocation;
    }
}
