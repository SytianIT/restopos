<?php

namespace App\Repository\Table;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class TableSection extends BaseModel implements ContextScope
{
    use SoftDeletes;

    protected $dates = ['deleted_at','created_at','updated_at'];

    /**
     * @return object of Branch class
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    /**
     * @return object of Table class
     */
    public function tables()
    {
        return $this->hasMany(Table::class, 'section_id');
    }

    /**
     * Getter and Setters
     */

    /**
     * Scope Queries
     */
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('branch', function ($query) use ($context) {
                $query->whereClientId($context->id());
            });
        } else if ($context->getInstance() instanceof Branch) {
            return $query->where('branch_id', $context->id());
        }

        return $query;
    }
}
