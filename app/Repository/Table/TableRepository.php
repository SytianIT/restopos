<?php

namespace App\Repository\Table;

use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use Illuminate\Http\Request;

class TableRepository extends ContextRepository
{
    public function __construct(
        Table $table,
        ContextInterface $context)
    {
        $this->model = $table;
        $this->context = $context;
    }

    public function search(Request $request, $with = [])
    {
        $query = $this->scope($with);
        $query = $this->filter($request, $query);

        return $query->paginate(self::PAGINATE);
    }

    public function filter(Request $request, $query, $returnType = 'query')
    {
        $query = $query->orderBy('position', 'ASC');

        if($request->has('filter_tables_branch')){
            $query = $query->where('branch_id', '=', $request->filter_tables_branch);
        }

        if($request->has('filter_tables_section')){
            $query = $query->where('section_id', '=', $request->filter_tables_section);
        }

        if($returnType == 'collection'){
            return $query->paginate(50);
        }

        return $query; // return type is Query
    }

    public function validateTableFields($request)
    {
        $this->validate($request, [
            'title' => 'required',
            'branch' => 'required',
            'section' => 'required'
        ]);

        return;
    }

    public function storeTableFields($request, $table)
    {
        $table->title = $request->title;
        $table->remarks = $request->remarks;
        $table->section_id = $request->section;
        $table->active = $request->has('active') ? true : false;

//        $maxPos = $table->whereHas('branch', function ($q) use ($branch) {
//            return $q->where('id', '=', $branch);
//        })->where('section_id', '=', $request->section)->max('position');
//
//        $table->position = $maxPos;
        $table->branch()->associate($request->branch);

        $table->save();

        return $table;
    }
}
