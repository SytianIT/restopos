<?php

namespace App\Repository\Table;

use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use Illuminate\Http\Request;

class TableSectionRepository extends ContextRepository
{
    public function __construct(
        TableSection $tableSection,
        ContextInterface $context)
    {
        $this->model = $tableSection;
        $this->context = $context;
    }

    public function search(Request $request, $with = [])
    {
        $query = $this->scope($with);
        $query = $this->filter($request, $query);

        return $query->paginate(self::PAGINATE);
    }

    public function filter(Request $request, $query, $returnType = 'query')
    {
        if($request->has('filter_tables_branch')){
            $query = $query->where('branch_id', '=', $request->filter_tables_branch);
        }

        if($returnType == 'collection'){
            return $query->paginate(50);
        }

        return $query; // return type is Query
    }

    public function validateSectionFields($request)
    {
        $this->validate($request, [
            'title' => 'required',
            'branch' => 'required'
        ]);

        return;
    }

    public function storeSectionFields($request, $section)
    {
        $section->title = $request->title;
        $section->branch()->associate($request->branch);

        $section->save();

        return $section;
    }
}
