<?php

namespace App\Repository\Table;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Rutorika\Sortable\SortableTrait;

class Table extends BaseModel implements ContextScope
{
    use SortableTrait, SoftDeletes;

    protected $dates = ['deleted_at','created_at','deleted_at'];

    /**
     * @return collection of Branch class
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    /**
     * @return object of TableSection class
     */
    public function section()
    {
        return $this->belongsTo(TableSection::class, 'section_id');
    }

    /**
     * Scope Queries
     */
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->whereHas('branch', function ($query) use ($context) {
                $query->whereClientId($context->id());
            });
        } else if ($context->getInstance() instanceof Branch) {
            return $query->where('branch_id', $context->id());
        }

        return $query;
    }

}
