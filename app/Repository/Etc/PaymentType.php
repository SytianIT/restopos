<?php

namespace App\Repository\Etc;

use Illuminate\Database\Eloquent\Model;

class PaymentType extends Model
{
    const CASH = 'cash';
    const CHEQUE = 'cheque';
    const CREDIT_CARD = 'credit_card';
    const GIFT_CERTIFICATE = 'gift_certificate';

    public static $types = [
    	self::CASH => 'Cash',
    	self::CHEQUE => 'Cheque',
    	self::CREDIT_CARD => 'Credit Card',
    	self::GIFT_CERTIFICATE => 'Gift Certificate'
    ];
}
