<?php

namespace App\Repository\Etc;

use App\Repository\Product\Product;
use Illuminate\Database\Eloquent\Model;

class Uom extends Model
{
    protected $table = 'uom';

    /**
     * Eloquent relation for 'one to many' of Product class
     * @return collection of Product class
     */
    public function products()
    {
    	return $this->hasMany(Product::class, 'uom_id');
    }
}
