<?php

namespace App\Repository\Etc;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    const SIMPLE = 'simple';
    const COMPOSITE = 'composite';
}
