<?php

namespace App\Repository\Etc;

use App\Repository\BaseModel;
use App\Repository\Client\Client;

class Countries extends BaseModel
{
    public function clients()
    {
    	return $this->hasMany(Client::class, 'country');
    }
}
