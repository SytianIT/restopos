<?php

namespace App\Repository\Item;

use App\Repository\Branch\Branch;
use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use App\Repository\Stock\StockBalance;
use App\Repository\StockLocation\StockLocation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

class ItemRepository extends ContextRepository
{
    protected $model;
    protected $item;
    protected $client;
    protected $invTracking = false;

    public function __construct(
        Item $item,
        ContextInterface $context
    )
    {
        $this->model = $item;
        $this->context = $context;
    }

    public function search(Request $request, $parents, $with = [])
    {
        $query = $this->scope($with);
        $query = $this->filter($request, $query);

        if($parents){
            $query = $query->parentItems();
        }

        return $query->paginate(self::PAGINATE);
    }

    public function filter(Request $request, $query, $returnType = 'query')
    {
        // Filter process's here!!

        if($returnType == 'collection'){
            return $query->paginate(50);
        }

        return $query; // return type is Query
    }

    /**
     * Method to initialize creation of Item
     * @param $request
     * @param $item
     * @param $client
     */
    public function startCreatingItem($request, $item, $client)
    {
        $item->title = $request->title;
        $item->cost = floatval($request->cost);
        $item->category_id = $request->category_id;
        $item->uom_id = $request->uom_id;
        $item->client_id = $client->id;

        if($request->hasFile('image')){
            $item->image = $this->baseUpload($request->file('image'), 'uploads/items/featured', 450, 450);
        }

        if($request->has('enable_inventory')){
            $this->invtracking = true;
            $item->enable_inventory = true;
            $item->inventory_type = $request->inventory_type;
        }
        $item->active = $request->has('active') ? true : false;
        $item->save();
        $this->item = $item;
        $this->client = $client;
        // Coming soon, configure tax relation.

        // Create Add-ons
        if($request->has('addon')){
            $this->createAddOns($request);
        }

        // Check if creation or update has attributes
        $this->checkAttributes($request);
    }

    /**
     * Method to check if Item instance has attributes or so called Variations
     * @param $request
     */
    private function checkAttributes($request)
    {
        if($request->has('item_attributes')){
            $this->item->attributes()->sync($request->item_attributes);
            $this->makeVariationItems($request);
        } else {
            if(count($request->branch) > 0){
                $this->createPriceSingleItem($request->branch);
            }

            if($request->has('recipes')){
                $this->makeRecipes($request->recipes);
            }
        }
    }

    /**
     * If Checking of attribute is true, this method will be called to iterate to those
     * variations and identify if those are enabled in specific branch or enabled itself
     * @param $request
     */
    private function makeVariationItems($request)
    {
        $parentItem = $this->item;
        foreach ($request->variations as $key => $variation){
            $variation = (object) $variation;
            if(isset($variation->enable) && $variation->enable == 'on'){
                foreach($variation->options as $id => $option){
                    $pricing = $request->pricing[$id];
                    $recipes = $request->has('recipes.'.$id) ? $request->recipes[$id] : null;

                    $this->makeVariationItem($option, $pricing, $recipes, $parentItem);
                }
            }
        }
    }

    /**
     * Method to create the variation Item
     * In this methid $this->item will be replace by current variation
     * to use in method createPriceSingleItem()
     * @param $option
     * @param $pricing
     * @param $recipes
     * @param $parentItem
     * @return bool
     */
    private function makeVariationItem($option, $pricing, $recipes, $parentItem)
    {
        $option = (object) $option;

        $variation = isset($option->variation_id) ? Item::find($option->variation_id) : new Item();
        $variation->title = $option->title;
        $variation->cost = $option->cost != null ? $option->cost : null;
        $variation->main_item_id = $parentItem->id;
        $variation->client_id = $this->client->id;
        $variation->option_id = isset($option->id) ? $option->id : $variation->option_id;
        $variation->enabled_variation = isset($pricing['enable']) ? true : null;
        $variation->save();

        $this->item = $variation;

        if($variation->enabled_variation){
            $this->createPriceSingleItem($pricing['branches']);
            if($recipes != null && count($recipes) > 0){
                $this->makeRecipes($recipes);
            }
        }

        return true;
    }

    /**
     * If condition checking of recipes in methods (makeVariationItem, startCreatingItem)
     * evaluates to true this method will be called to start creating relation to
     * other items that will server as its recipes
     * @param $array
     * @return bool
     */
    private function makeRecipes($array)
    {
        $newRecipes = new Collection();
        $oldRecipes = $this->item->recipes;

        foreach ($array as $row){
            $row = (object) $row;
            $item = Item::find($row->item);

            if($item){
                $recipe = isset($row->recipe_id) ? ItemRecipe::find($row->recipe_id) : new ItemRecipe();
                $recipe->item_id = $this->item->id;
                $recipe->recipe_item_id = $item->id;
                $recipe->amount = is_numeric($row->amount) ? $row->amount : null;
                $recipe->save();
                $newRecipes->push($recipe);
            } else {
                Log::alert("There was no item found while creating the item recipe for '{$this->item->title}' under client '{$this->client->company_name}'");
            }
        }

        $this->deleteObjects($newRecipes, $oldRecipes);

        return true;
    }

    /**
     * Method to create prices for each branches enabled for this item,
     * branches that are not in this current branches array will be removed.
     * @param $array
     * @return bool
     */
    private function createPriceSingleItem($array)
    {
        foreach($array as $pricing){
            $pricing = (object) $pricing;
            $branch = Branch::find($pricing->id);
            $price = isset($pricing->pricing_id) ? ItemPrice::find($pricing->pricing_id) : new ItemPrice();

            if(isset($pricing->enable)){
                if($branch){

                    $this->item->branches()->syncWithoutDetaching([$branch->id]);

                    $price->item()->associate($this->item);
                    $price->branch()->associate($branch);
                    $price->dine_in_price = floatval($pricing->dine_in_pr);
                    $price->take_out_price = floatval($pricing->take_out_pr);
                    $price->dine_in_price_tax_inc = floatval($pricing->dine_in_pr_tax_inc);
                    $price->take_out_price_tax_inc = floatval($pricing->take_out_pr_tax_inc);
                    if(!isset($pricing->pricing_id)){
                        $price->beginning_amount = isset($pricing->beginning_inv) && $pricing->beginning_inv != '' ? $pricing->beginning_inv : null;
                    }
                    $price->reorder_amount = isset($pricing->reorder_amnt) && $pricing->reorder_amnt != '' ? $pricing->reorder_amnt : null;
                    $price->editable_pos = isset($pricing->editable_pos) ? true : false;
                    $price->save();

                    if($branch->getThisItemStockBalance($this->item) == false){
                        $this->createOrUpdateStockBalance($branch, $pricing);
                    }

                } else {
                    Log::alert("There was no branch found while creating the item pricing for '{$this->item->title}' under client '{$this->client->company_name}'");
                }
            } else {
                $price->delete();
                $this->item->branches()->detach($branch->id);
            }
        }

        return true;
    }

    private function createOrUpdateStockBalance($branch, $pricing)
    {
        $stockLocation = $branch->stockLocations()->whereName(StockLocation::LOCATION_DEFAULT)->first();
        $stockBalance = $this->item->getThisBranchStockBalance($branch);
        $balance = $stockBalance != false ? $stockBalance : new StockBalance();

        if($balance){
            $balance->stockLocation()->associate($stockLocation);
            $balance->branch()->associate($branch);
            $balance->item()->associate($this->item);
            $balance->amount = isset($pricing->beginning_inv) && $pricing->beginning_inv != '' ? $pricing->beginning_inv : null;
            $balance->date = Carbon::today();
            $balance->save();

            return $balance;
        }

        return false;
    }

    /**
     * Method to create Add-ons and relate to $this->item
     * @param $request
     * @return bool
     */
    private function createAddOns($request)
    {
        $newAddons = new Collection();
        $oldAddons = $this->item->addons;

        $addons = array_values($request->addon);
        foreach($addons as $key => $row){

            $row = (object) $row;
            $addon = isset($row->addon_id) ? AddOn::find($row->addon_id) : new AddOn();
            $addon->item_id = $this->item->id;
            $addon->position = $key;
            $addon->title = $row->title;
            $addon->dine_in_price = floatval($row->dine_in_pr);
            $addon->dine_in_price_tax_inc = isset($row->dine_in_price_tax_inc) ? floatval($row->dine_in_price_tax_inc) : null;
            $addon->take_out_price = floatval($row->take_out_pr);
            $addon->take_out_price_tax_inc = isset($row->take_out_pr_tax_inc) ? floatval($row->take_out_pr_tax_inc) : null;
            $addon->editable_pos = isset($row->reorder_amnt) ? true : false;

            $addon->save();
            $newAddons->push($addon);
        }

        $this->deleteObjects($newAddons, $oldAddons);

        return true;
    }

    /**
     * General Method used to delete certain collection objects
     * @param $new
     * @param $old
     */
    private function deleteObjects($new, $old)
    {
        $forDeletes = $old->diff($new);
        foreach ($forDeletes as $object) {
            $object->delete();
        }
    }


    public function validateItemFields($request)
    {
        $this->validate($request,
            [
                'title' => 'required',
                'category_id' => 'required',
            ]
            ,[
                'title.required' => 'Provide the title of this item.',
                'category_id.required' => 'Choose the category of this item.',
            ]);

        return;
    }
}
