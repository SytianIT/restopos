<?php

namespace App\Repository\Item;

use App\Repository\Branch\Branch;
use Illuminate\Database\Eloquent\Model;

class ItemPrice extends Model
{
    protected $table = 'item_prices';

    /**
     * @return object of Item class
     */
    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }

    /**
     * @return object of Branch class
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
