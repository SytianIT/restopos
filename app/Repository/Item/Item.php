<?php

namespace App\Repository\Item;

use App\Repository\Etc\Uom;
use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use App\Repository\ContextScope;
use App\Repository\ContextInterface;
use App\Repository\Attribute\Attribute;
use App\Repository\Stock\StockBalance;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends BaseModel implements ContextScope
{
    use SoftDeletes;

    protected $dates = ['created_at','updated_at','deleted_at'];

    protected $casts = ['settings' => 'array'];
    
    /**
     * @return object of AddOn class
     */
    public function addons()
    {
        return $this->hasMany(AddOn::class, 'item_id');
    }
    
    /**
     * @return object of Attribute class
     */
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class, 'item_attributes', 'item_id', 'attribute_id');
    }

    /**
     * @return object of Branches class
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'item_branches', 'item_id', 'branch_id');
    }

    /**
     * @return object of Client class
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Eloquent relation for 'one to one' of ItemCategory class
     * @return object of ItemCategory class
     */
    public function category()
    {
        return $this->belongsTo(ItemCategory::class, 'category_id');
    }

    /**
     * @return collection of Items class
     */
    public function meals()
    {
        return $this->belongsToMany(ItemRecipe::class, 'recipe_item_id');
    }

    /**
     * @return object of ItemPrice class
     */
    public function pricing()
    {
        return $this->hasMany(ItemPrice::class, 'item_id');
    }

    /**
     * @return object of Items class
     */
    public function recipes()
    {
        return $this->hasMany(ItemRecipe::class, 'item_id');
    }
    
    /**
     * @return object of StockBalance class
     */
    public function stockBalance()
    {
        return $this->hasOne(StockBalance::class, 'item_id');
    }

    /**
     * @return object of Uom class
     */
    public function uom()
    {
        return $this->belongsTo(Uom::class, 'uom_id');
    }

    /**
     * @return object of Item class (as Variation)
     */
    public function variations()
    {
        // return $this->belongsToMany(Item::class, 'item_variations', 'main_item_id', 'variation_item_id'); *To be depreciated
        return $this->hasMany(Item::class, 'main_item_id');
    }

    // Scope Queries
    public function scopeParentItems($query)
    {
        return $query->whereNull('main_item_id');
    }

    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where('client_id', $context->id());
        } else if ($context->getInstance() instanceof Branch) {
            return $query->whereHas('branches', function ($query) use ($context) {
                $query->whereId($context->id());
            });
        }

        return $query;
    }

    /**
     * Getters, Mutators & Setters
     */

    public function getThisOption($collection)
    {
        return $collection->first(function ($value, $key) {
            return $value->id == $this->option_id;
        });
    }

    public function getThisPricing($branch)
    {
        return $this->pricing->first(function ($value, $key) use ($branch){
            return $value->branch_id == $branch->id ? $value : false;
        });
    }

    public function getThisBranchStockBalance($branch)
    {
        $balance = $this->stockBalance()->where('branch_id', $branch)->first();

        if($balance->exists){
            return $balance;
        }

        return false;
    }

    public function getTrackedAttribute()
    {
        return $this->enable_inventory;
    }
}
