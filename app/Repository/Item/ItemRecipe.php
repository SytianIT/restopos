<?php

namespace App\Repository\Item;

use Illuminate\Database\Eloquent\Model;

class ItemRecipe extends Model
{
    protected $table = 'item_recipes';
    
    /**
     * @return object of Item class
     */
    public function meal()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
    
    /**
     * @return object of Item class
     */
    public function recipe()
    {
        return $this->belongsTo(Item::class, 'recipe_item_id');
    }
}
