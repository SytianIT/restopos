<?php

namespace App\Repository\Item;

use App\Repository\ContextInterface;
use App\Repository\ContextRepository;
use Illuminate\Http\Request;

class ItemCategoryRepository extends ContextRepository
{
    public function __construct(
        ItemCategory $itemCategory,
        ContextInterface $context
    )
    {
        $this->model = $itemCategory;
        $this->context = $context;
    }

    public function search(Request $request, $parents, $with = [])
    {
        $query = $this->scope($with);

        if($parents){
            $query = $query->parents();
        }

        return $query->paginate(self::PAGINATE);
    }

    public function fillCategoryRecords($request, $category, $client)
    {
        $category->title = $request->title;
        $category->notes = $request->notes;
        $category->client_id = $client->id;
        $category->parent_id = $request->has('parent_id') ? $request->parent_id : null;
        $category->pos_disabled = $request->has('pos_disabled') ? true : false;

        $category->save();

        $category->branches()->sync($request->branch);
        $category->setName($category->title);

        return $category;
    }

    public function validateCategoryFields($request)
    {
        $this->validate($request,
            [
                'title' => 'required',
                'branch' => 'required'
            ]
            ,[
                'title.required' => 'Provide the title of the category.',
                'branch.required' => 'Please select branch(es) to have this category.'
            ]);

        return;
    }
}
