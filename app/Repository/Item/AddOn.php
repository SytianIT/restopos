<?php

namespace App\Repository\Item;

use App\Repository\Item\Item;
use App\Repository\BaseModel;

class AddOn extends BaseModel
{
    use \Rutorika\Sortable\SortableTrait;

    protected $table = 'add_ons';
    
    /**
     * @return object of Item class
     */
    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id');
    }
}
