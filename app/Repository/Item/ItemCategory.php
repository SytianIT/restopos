<?php

namespace App\Repository\Item;

use App\Repository\BaseModel;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use App\Repository\ContextInterface;
use App\Repository\ContextScope;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCategory extends BaseModel implements ContextScope
{
    use SoftDeletes;

    protected $dates = ['deleted_at','updated_at','created_at'];

    protected $casts = ['settings' => 'array'];

    /**
     * Eloquent relation for 'one to many' of Client class
     *
     * @return object of Client class
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Eloquent relation for 'one to mane' of Branch class
     * @return object of Branch class
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'item_categories_branches', 'category_id', 'branch_id');
    }

    /**
     * Eloquent relation for 'one to one' of ItemCategory class
     * @return object of Items class
     */
    public function items()
    {
        return $this->hasMany(Item::class, 'category_id');
    }

    // Scope Queries
    public function scopeOfContext($query, ContextInterface $context)
    {
        if ($context->getInstance() instanceof Client) {
            return $query->where('client_id', $context->id());
        } else if ($context->getInstance() instanceof Branch) {
            return $query->whereHas('branches', function ($query) use ($context) {
                $query->whereId($context->id());
            });
        }

        return $query;
    }

    public function scopeParents($query)
    {
        return $query->whereNull('parent_id');
    }
}
