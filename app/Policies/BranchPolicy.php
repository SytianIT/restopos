<?php

namespace App\Policies;

use App\Repository\Branch\Branch;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BranchPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function access(User $user, Branch $branch)
    {
        if ($user->isOwner()) {
            return $user->client_id == $branch->client_id;
        } else if ($user->isRegUser()) {
            return $user->branches->contains(function($userBranch) use ($branch) {
                return $userBranch->id == $branch->id;
            });
        }

        return false;
    }
}
