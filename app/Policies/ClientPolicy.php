<?php

namespace App\Policies;

use App\User;
use App\Repository\Client\Client;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClientPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function access(User $user, Client $client)
    {
        if (!$user->isClient()) return false;

        return $user->client_id == $client->id;
    }
}
