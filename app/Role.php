<?php

namespace App;

use App\Repository\Client\Client;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
	const SUPERADMIN = 'superadmin';
    const CLIENT_ROLE = 'client_owner';
    const BRANCH_OWNER_ROLE = 'branch_owner';

    const SUPERADMIN_ID = 1;
    const CLIENT_ROLE_ID = 2;
    const BRANCH_OWNER_ROLE_ID = 3;

    /**
	 * The attributes that are mass assignable.
	 * 
	 * @var array
	 */
    protected $fillable = [
    	'name','description','other_data'
    ];

    /**
     * The attributes the cast as array
     * @var [type]
     */
    protected $casts = [
    	'other_data' => 'array'
    ];
    
    /**
     * Relation of many to many for User class.
     * @return Collection collection of staffs assigned to the user.
     */
    public function users()
    {
    	return $this->belongsToMany(User::Class, 'user_roles', 'role_id', 'user_id');
    }

    /**
     * Relation of many to many for Permission class
     * @return Collection collection of permission assigned to the staff
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'role_permissions', 'role_id', 'permission_id');
    }
    
    /**
     * @return object of Client class
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    // Scope Queries
    public function scopeAdmins($query)
    {
        return $query->whereNull('client_id');
    }
}
