<?php

namespace App;

use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    const ADMIN_CLIENT_ID = 'restop';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'middle_name', 'contact_no', 'email', 'username', 'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that cast as arrays.
     *
     * @var array
     */
    protected $casts = [
        'other_data' => 'array',
    ];

    /**
     * The attributes that declared in date formats
     *
     * @var array
     */
    protected $dates = [
        'birthdate', 'created_at', 'updated_at', 'deleted_at'
    ];

    /**
     * Relation of many to many for User class
     * @return collection of all roles assigned to this user
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    }

    /**
     * Relation of many to one for Client class
     * @return object of client assigned to this user
     */
    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    /**
     * Relation of many to many for Branch class
     * @return collection of branch assigned to this user
     */
    public function branches()
    {
        return $this->belongsToMany(Branch::class, 'user_branches', 'user_id', 'branch_id');
    }

    /**
     * Scope Methods
     */
    public function scopeRemoveCurrent($query)
    {
        return $query->where('id', '<>', Auth::user()->id);
    }

    public function scopeRemoveSuperAdmin($query)
    {
        return $query->where('id', '<>', 1);
    }

    public function scopeNotClientUsers($query)
    {
        return $query->whereNull('client_id');
    }

    /**
     * Setters * Getters
     */
    public function getNameAttribute()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function getAvatarAttribute()
    {
        return $this->profile_img != null ? asset($this->profile_img) : asset('images/gravatar.jpg');
    }

    public function getRoleAsClientAttribute()
    {
        return $this->roles()->first();
    }

    public function isAdmin()
    {
        if($this->isThisItsAccess(Role::SUPERADMIN)){
            return true;
        }

        return false;
    }

    public function isClient()
    {
        if($this->isThisItsAccess(Role::CLIENT_ROLE)){
            return true;
        }

        return false;
    }

    public function isBranch()
    {
        if($this->isThisItsAccess(Role::BRANCH_OWNER_ROLE)){
            return true;
        }

        return false;
    }

    /**
     * Other specialized functions
     */
    
    /**
     * Method to check the major roles in accessing this system app
     * @param  String  $roleName
     * @return boolean 
     */
    public function isThisItsAccess($roleName)
    {
        $flag = false;
        $role = $this->roles()->where('name', '=', $roleName)->first();

        if($role != null){
            $flag = true;
        }

        return $flag;
    }

    /**
     * Method to chech if the permission is permitted to this user role
     * @param  String  $permissionName
     * @return boolean 
     */
    public function hasPermission($permissionName)
    {
        $permitted = false;
        $permission = Permission::where('name', '=', $permissionName)->first();
        $assignedRole = $this->roles()->get();

        if($permission){
            $assignedRole->each(function ($item, $val) use (&$permitted, $permission){
                if($item->permissions->contains($permission)){
                    $permitted = true;
                    return false;
                }
            });
        }

        return $permitted;
    }
}
