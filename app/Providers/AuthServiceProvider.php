<?php

namespace App\Providers;

use App\Permission;
use App\Repository\Branch\Branch;
use App\Repository\Client\Client;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Branch::class => \App\Policies\BranchPolicy::class,
        Client::class => \App\Policies\ClientPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(\Illuminate\Contracts\Auth\Access\Gate $gate)
    {
        $this->registerPolicies($gate);

        Gate::before(function ($user, $ability) {
            if ($user->isAdmin()) {
                return true;
            }
        });

        Gate::define('allow', function ($user, $permission) {
            $permission = Permission::whereName($permission)->first();

            // maging false to pag finalized lahat ng permissions
            if (!$permission) return true;

            if ($user->isBranch()) {
                if ($user->hasPermission($permission)) {
                    return true;
                }
                return false;
            }

            // We assume the user is of type Client, Admin or Superadmin
            return true;
        });
    }
}
