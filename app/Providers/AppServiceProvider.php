<?php

namespace App\Providers;

use App\Repository\Context;
use App\Repository\ContextInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * General Application Provider
         * @var view
         */
        View::composer([
            'layouts.*',
            'module.admin.*',
            'module.client.*'
            ], function($view)
        {
            $user = Auth::user();
            
            $view->with('authUser', $user ? $user : null);
        });

        /**
         * General Blade Directives
         */
        Blade::directive('money', function($expression) {
            return "<?php echo number_format($expression, 2) ?>";
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(ContextInterface::class, Context::class);
    }
}
