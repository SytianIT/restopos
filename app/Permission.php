<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
	const GENERAL_LVL = 0;
    const ADMIN_LVL = 1;
    const CLIENT_LVL = 2;
    const BRANCH_LVL = 3;

    /**
	 * The attributes that are mass assignable
	 * @var array
	 */
    protected $fillable = [
    	'name','title','description'
    ];

    /**
     * Relation of many to many for Staff class
     * @return Collection collection of staffs
     */
    public function roles()
    {
    	return $this->belongsToMany(Role::class, 'role_permissions', 'permission_id', 'role_id');
    }

    // Scope Query
    /**
     * @param $query
     * @return mixed
     */
    public function scopeClientLevel($query)
    {
        return $query->where('level', '=', self::CLIENT_LVL)
                     ->orWhere('level', '=', self::BRANCH_LVL);
    }
}
