<?php

function generateBirthPicker($return){
    if($return == 'year'){
        $current = Carbon\Carbon::today()->subYear();
        $startYear = $current->format('Y');
        $endYear = $current->subYears(100);

        return ['start' => $startYear, 'end' => $endYear->format('Y')];
    }

    if($return == 'days'){
        $days = [];
        for($x = 1; $x <= 31; $x++){
            $days[] = $x;
        }

        return $days;
    }

    if($return == 'months'){
        $months = [1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'];

        return $months;
    }
}