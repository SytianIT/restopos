<?php

use App\Repository\Etc\Uom;
use Illuminate\Database\Seeder;

class InitialDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Uom::truncate();

        $ml = new Uom;
        $ml->title = 'ml';
        $ml->long_title = 'Milliliter';
        $ml->save();

        $g = new Uom;
        $g->title = 'g';
        $g->long_title = 'Grams';
        $g->save();

        $oz = new Uom;
        $oz->title = 'oz';
        $oz->long_title = 'Ounces';
        $oz->save();

        $pc = new Uom;
        $pc->title = 'pc';
        $pc->long_title = 'Pieces';
        $pc->save();
    }
}
