<?php

use App\Permission;
use App\Repository\Client\Client;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class InitialUserAccessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $suUser = new User;
        $suUser->first_name = 'Sytian Admin';
        $suUser->email = 'admin@email.com';
        $suUser->username = 'admin';
        $suUser->password = bcrypt('admin123');
        $suUser->save();

        Role::truncate();

        // Roles initialy created, please do not change order it might cause functionality error in some modules.
        $superAdmin = new Role;
        $superAdmin->name = Role::SUPERADMIN;
        $superAdmin->title = 'Super Admin';
        $superAdmin->description = 'Super admin role';
        $superAdmin->save();

        $clientRole = new Role;
        $clientRole->name = Role::CLIENT_ROLE;
        $clientRole->title = 'Client / Owner';
        $clientRole->description = 'Client or Owner Role';
        $clientRole->save();

        $branchRole = new Role;
        $branchRole->name = Role::BRANCH_OWNER_ROLE;
        $branchRole->title = 'Branch Manager';
        $branchRole->description = 'Branch Manager';
        $branchRole->save();

        Permission::truncate();

        $accessAdmin = new Permission;
        $accessAdmin->name = 'administrator';
        $accessAdmin->title = 'Administrator';
        $accessAdmin->level = Permission::ADMIN_LVL;
        $accessAdmin->save();

        $accessClient = new Permission;
        $accessClient->name = 'client';
        $accessClient->title = 'Client / Owner';
        $accessClient->level = Permission::ADMIN_LVL;
        $accessClient->save();

        $accessBranch = new Permission;
        $accessBranch->name = 'branch';
        $accessBranch->title = 'Branch ';
        $accessBranch->level = Permission::CLIENT_LVL;
        $accessBranch->save();

        $suUser->roles()->attach($superAdmin);
        $superAdmin->permissions()->attach([$accessAdmin->id, $accessClient->id, $accessBranch->id]);

        $client = new Client;
        $client->company_name = 'Sytian Productions';
        $client->address_1 = 'East Kamias';
        $client->city = 'Quezon City';
        $client->country = 1;
        $client->zip = '1100';
        $client->pc_first_name = 'Admin Sytian';
        $client->pc_last_name = 'Sytian User';
        $client->pc_mobile = '7788556699';
        $client->pc_email = 'sytian@email.com';
        $client->save();
    }
}
