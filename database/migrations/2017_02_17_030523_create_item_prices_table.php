<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('branch_id');
            //$table->decimal('cost', 15, 2)->nullable();
            $table->decimal('dine_in_price', 15, 2)->nullable();
            $table->decimal('dine_in_price_tax_inc', 15, 2)->nullable();
            $table->decimal('take_out_price', 15, 2)->nullable();
            $table->decimal('take_out_price_tax_inc', 15, 2)->nullable();
            $table->integer('beginning_amount')->nullable();
            $table->integer('reorder_amount')->nullable();
            $table->boolean('editable_pos')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_prices');
    }
}
