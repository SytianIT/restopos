<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('capital');
            $table->string('citizenship');
            $table->string('country_code', 3);
            $table->string('currency');
            $table->string('currency_code');
            $table->string('currency_sub_unit');
            $table->string('full_name');
            $table->string('iso_3166_2', 2);
            $table->string('iso_3166_3', 3);
            $table->string('name');
            $table->string('region_code', 3);
            $table->string('sub_region_code', 3);
            $table->boolean('eea')->default(0);
            $table->string('calling_code', 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}
