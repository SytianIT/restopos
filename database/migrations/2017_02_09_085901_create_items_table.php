<?php

use App\Repository\Etc\Inventory;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('category_id')->nullable();
            $table->integer('uom_id')->nullable();
            $table->integer('main_item_id')->nullable();
            $table->string('image')->nullable();
            $table->string('name')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->decimal('cost', 15, 2)->nullable();
            $table->integer('amount')->nullable();
            $table->text('settings')->nullable();
            $table->boolean('sell_by_weight')->default(false);
            $table->boolean('enable_inventory')->default(false);
            $table->enum('inventory_type', [Inventory::SIMPLE, Inventory::COMPOSITE])->nullable();
            // $table->boolean('for_sale')->default(true);
            $table->boolean('has_variation')->default(false);
            $table->boolean('enabled_variation')->default(null);
            $table->integer('option_id')->nullable();
            $table->boolean('active')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
