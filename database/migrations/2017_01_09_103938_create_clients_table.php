<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cuid')->unique();
            $table->string('company_name');
            $table->string('company_logo')->nullable();
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('city');
            $table->string('country');
            $table->string('zip');
            $table->string('website')->nullable();
            $table->string('pc_first_name');
            $table->string('pc_last_name');
            $table->string('pc_mobile');
            $table->string('pc_telephone')->nullable();
            $table->string('pc_email');
            $table->string('sc_first_name')->nullable();
            $table->string('sc_last_name')->nullable();
            $table->string('sc_mobile')->nullable();
            $table->string('sc_telephone')->nullable();
            $table->string('sc_email')->nullable();
            $table->boolean('subscription_status')->default(false);
            $table->boolean('account_active')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
