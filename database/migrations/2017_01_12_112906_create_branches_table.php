<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uuid');
            $table->integer('client_id');
            $table->string('name')->nullable();
            $table->string('title');
            $table->string('company_logo')->nullable();
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('city')->nullable();
            $table->integer('country');
            $table->string('zip')->nullable();
            $table->string('website')->nullable();
            $table->string('telephone')->nullable();
            $table->string('mobile')->nullable();
            // contact details
            $table->string('pc_first_name')->nullable();
            $table->string('pc_last_name')->nullable();
            $table->string('pc_mobile')->nullable();
            $table->string('pc_telephone')->nullable();
            $table->string('pc_email')->nullable();
            // Secondary contact details
            $table->string('sc_first_name')->nullable();
            $table->string('sc_last_name')->nullable();
            $table->string('sc_mobile')->nullable();
            $table->string('sc_telephone')->nullable();
            $table->string('sc_email')->nullable();
            // Subscription Columns
            $table->dateTime('subscription_expiration')->nullable();

            $table->date('date_created')->nullable();
            $table->boolean('active')->default(1);
            $table->text('settings')->nullable();
            $table->text('notes')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branches');
    }
}
