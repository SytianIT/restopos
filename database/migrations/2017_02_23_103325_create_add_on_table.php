<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddOnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_ons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('position');
            $table->string('title');
            $table->decimal('dine_in_price', 15, 2)->nullable();
            $table->decimal('dine_in_price_tax_inc', 15, 2)->nullable();
            $table->decimal('take_out_price', 15, 2)->nullable();
            $table->decimal('take_out_price_tax_inc', 15, 2)->nullable();
            $table->boolean('editable_pos')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_ons');
    }
}
