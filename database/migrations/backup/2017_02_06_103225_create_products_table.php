<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('title');
            $table->string('sku')->nullable();
            $table->decimal('amount', 15, 2);
            $table->string('image');
            $table->text('description')->nullable();
            $table->integer('category_id');
            $table->integer('brand_id')->nullable();
            $table->integer('client_id');
            $table->text('settings');
            $table->boolean('for_sale')->default(true);
            $table->boolean('active')->default(true);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
