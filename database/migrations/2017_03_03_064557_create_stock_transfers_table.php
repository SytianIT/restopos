<?php

use App\Repository\Stock\StockTransfer;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->timestamp('datetime');
            $table->enum('status', [StockTransfer::PENDING, StockTransfer::IN_PROCESS, StockTransfer::COMPLETED, StockTransfer::CANCELLED]);
            $table->text('remarks')->nullable();
            $table->integer('branch_id');
            $table->integer('from_location_id');
            $table->integer('to_branch_id');
            $table->integer('to_location_id');
//            $table->integer('from_branch_id');
            $table->timestamp('receive_at')->nullable();
            $table->integer('receive_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stock_transfers');
    }
}
