<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', ['uses' => 'GeneralController@index', 'as' => 'app']);
Route::get('/home', ['uses' => 'GeneralController@index', 'as' => 'home']);

Auth::routes();

Route::group(['middleware' => 'auth'], function()
{
	Route::get('logout', ['uses' => 'Auth\LoginController@logout', 'as' => 'auth.logout']);

	/***********************************************************************************************************************************************
	 * Authenticated Routes for Admin Access
	 */
	Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin'], function(){

		Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);

		Route::group(['prefix' => 'client'], function(){
			Route::get('/', ['uses' => 'ClientController@index', 'as' => 'clients']);
			Route::get('create', ['uses' => 'ClientController@create', 'as' => 'clients.create']);
			Route::post('create', ['uses' => 'ClientController@store', 'as' => 'clients.store']);
			Route::get('{client}/edit', ['uses' => 'ClientController@edit', 'as' => 'clients.edit']);
			Route::put('{client}/edit', ['uses' => 'ClientController@update', 'as' => 'clients.update']);
			Route::get('{client}/delete', ['uses' => 'ClientController@delete', 'as' => 'clients.delete']);

			Route::group(['prefix' => '{client}', 'as' => 'clients.'], function(){

			    Route::group(['prefix' => 'users'], function(){
                    Route::get('', ['uses' => 'ClientUserController@index', 'as' => 'users']);
                    Route::get('create', ['uses' => 'ClientUserController@create', 'as' => 'users.create']);
                    Route::post('create', ['uses' => 'ClientUserController@store', 'as' => 'users.store']);
                    Route::get('{user}/edit', ['uses' => 'ClientUserController@edit', 'as' => 'users.edit']);
                    Route::put('{user}/edit', ['uses' => 'ClientUserController@update', 'as' => 'users.update']);
                    Route::get('{user}/delete', ['uses' => 'ClientUserController@delete', 'as' => 'users.delete']);
                });

				Route::group(['prefix' => 'roles'], function(){
                    Route::get('', ['uses' => 'ClientRoleController@index', 'as' => 'roles']);
                    Route::get('create', ['uses' => 'ClientRoleController@create', 'as' => 'roles.create']);
                    Route::post('create', ['uses' => 'ClientRoleController@store', 'as' => 'roles.store']);
                    Route::get('{role}/edit', ['uses' => 'ClientRoleController@edit', 'as' => 'roles.edit']);
                    Route::put('{role}/edit', ['uses' => 'ClientRoleController@update', 'as' => 'roles.update']);
                    Route::get('{role}/delete', ['uses' => 'ClientRoleController@delete', 'as' => 'roles.delete']);
                });

				Route::get('branch', ['uses' => 'ClientBranchController@index', 'as' => 'branches']);
				Route::get('branch/create', ['uses' => 'ClientBranchController@create', 'as' => 'branches.create']);
				Route::post('branch/create', ['uses' => 'ClientBranchController@store', 'as' => 'branches.store']);
				Route::get('branch/{branch}/edit', ['uses' => 'ClientBranchController@edit', 'as' => 'branches.edit']);
				Route::put('branch/{branch}/edit', ['uses' => 'ClientBranchController@update', 'as' => 'branches.update']);
				Route::get('branch/{branch}/delete', ['uses' => 'ClientBranchController@delete', 'as' => 'branches.delete']);

                Route::group(['prefix' => 'items'], function(){

                    Route::group(['prefix' => 'categories', 'as' => 'items.'], function(){
                        Route::get('', ['uses' => 'ClientItemsCategoryController@index', 'as' => 'categories']);
                        Route::get('create', ['uses' => 'ClientItemsCategoryController@create', 'as' => 'categories.create']);
                        Route::post('create', ['uses' => 'ClientItemsCategoryController@store', 'as' => 'categories.store']);
                        Route::get('{category}/edit', ['uses' => 'ClientItemsCategoryController@edit', 'as' => 'categories.edit']);
                        Route::put('{category}/edit', ['uses' => 'ClientItemsCategoryController@update', 'as' => 'categories.update']);
                        Route::get('{category}/delete', ['uses' => 'ClientItemsCategoryController@delete', 'as' => 'categories.delete']);
                    });

                    Route::get('', ['uses' => 'ClientItemsController@index', 'as' => 'items']);
                    Route::get('create', ['uses' => 'ClientItemsController@create', 'as' => 'items.create']);
                    Route::post('create', ['uses' => 'ClientItemsController@store', 'as' => 'items.store']);
                    Route::get('{item}/edit', ['uses' => 'ClientItemsController@edit', 'as' => 'items.edit']);
                    Route::put('{item}/edit', ['uses' => 'ClientItemsController@update', 'as' => 'items.update']);
                    Route::get('{item}/delete', ['uses' => 'ClientItemsController@delete', 'as' => 'items.delete']);

                    Route::get('generate-variations', ['uses' => 'ClientItemsController@generateVariations', 'as' => 'items.generate-variations']);
                });

				Route::get('employees', ['uses' => 'ClientEmployeesController@index', 'as' => 'employees']);
				Route::get('employees/create', ['uses' => 'ClientEmployeesController@create', 'as' => 'employees.create']);
				Route::post('employees/create', ['uses' => 'ClientEmployeesController@store', 'as' => 'employees.store']);
				Route::get('employees/{employee}/edit', ['uses' => 'ClientEmployeesController@edit', 'as' => 'employees.edit']);
				Route::put('employees/{employee}/edit', ['uses' => 'ClientEmployeesController@update', 'as' => 'employees.update']);
				Route::get('employees/{employee}/delete', ['uses' => 'ClientEmployeesController@delete', 'as' => 'employees.delete']);

                Route::get('customers', ['uses' => 'ClientCustomerController@index', 'as' => 'customers']);
                Route::get('customers/create', ['uses' => 'ClientCustomerController@create', 'as' => 'customers.create']);
                Route::post('customers/create', ['uses' => 'ClientCustomerController@store', 'as' => 'customers.store']);
                Route::get('customers/{customer}/edit', ['uses' => 'ClientCustomerController@edit', 'as' => 'customers.edit']);
                Route::put('customers/{customer}/edit', ['uses' => 'ClientCustomerController@update', 'as' => 'customers.update']);
                Route::get('customers/{customer}/delete', ['uses' => 'ClientCustomerController@delete', 'as' => 'customers.delete']);

                Route::get('attributes', ['uses' => 'ClientAttributeController@index', 'as' => 'attributes']);
                Route::get('attributes/create', ['uses' => 'ClientAttributeController@create', 'as' => 'attributes.create']);
                Route::post('attributes/create', ['uses' => 'ClientAttributeController@store', 'as' => 'attributes.store']);
                Route::get('attributes/{attribute}/edit', ['uses' => 'ClientAttributeController@edit', 'as' => 'attributes.edit']);
                Route::put('attributes/{attribute}/edit', ['uses' => 'ClientAttributeController@update', 'as' => 'attributes.update']);
                Route::get('attributes/{attribute}/delete', ['uses' => 'ClientAttributeController@delete', 'as' => 'attributes.delete']);
			});
		});

		Route::group(['prefix' => 'users'], function(){
			Route::get('/', ['uses' => 'UserController@index', 'as' => 'users']);
			Route::get('create', ['uses' => 'UserController@create', 'as' => 'users.create']);
			Route::post('create', ['uses' => 'UserController@store', 'as' => 'users.store']);
			Route::get('{user}/edit', ['uses' => 'UserController@edit', 'as' => 'users.edit']);
			Route::put('{user}/edit', ['uses' => 'UserController@update', 'as' => 'users.update']);
			Route::get('{user}/delete', ['uses' => 'UserController@delete', 'as' => 'users.delete']);
		});

		Route::group(['prefix' => 'roles'], function(){
			Route::get('/', ['uses' => 'RoleController@index', 'as' => 'roles']);
			Route::get('create', ['uses' => 'RoleController@create', 'as' => 'roles.create']);
			Route::post('create', ['uses' => 'RoleController@store', 'as' => 'roles.store']);
			Route::get('{role}/edit', ['uses' => 'RoleController@edit', 'as' => 'roles.edit']);
			Route::put('{role}/edit', ['uses' => 'RoleController@update', 'as' => 'roles.update']);
			Route::get('{role}/delete', ['uses' => 'RoleController@delete', 'as' => 'roles.delete']);
		});
	});

	/***********************************************************************************************************************************************
	 * Authenticated Routes for Client Access
	 */
	Route::group(['middleware' => 'client', 'prefix' => 'client', 'as' => 'client.', 'namespace' => 'Client'], function(){

		Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);

		Route::group(['prefix' => 'branches'], function(){
			Route::get('/', ['uses' => 'BranchController@index', 'as' => 'branches']);
			// Route::get('create', ['uses' => 'BranchController@create', 'as' => 'branches.create']); // To be remove
			// Route::post('create', ['uses' => 'BranchController@store', 'as' => 'branches.store']); // To be remove
			Route::get('{branch}/edit', ['uses' => 'BranchController@edit', 'as' => 'branches.edit']);
			Route::put('{branch}/edit', ['uses' => 'BranchController@update', 'as' => 'branches.update']);
			Route::get('{branch}/delete', ['uses' => 'BranchController@delete', 'as' => 'branches.delete']);
		});

		Route::group(['prefix' => 'stock-locations'], function(){
            Route::get('/', ['uses' => 'StockLocationController@index', 'as' => 'stock-locations']);
            Route::get('create', ['uses' => 'StockLocationController@create', 'as' => 'stock-locations.create']);
            Route::post('create', ['uses' => 'StockLocationController@store', 'as' => 'stock-locations.store']);
            Route::get('{stockLocation}/edit', ['uses' => 'StockLocationController@edit', 'as' => 'stock-locations.edit']);
            Route::put('{stockLocation}/edit', ['uses' => 'StockLocationController@update', 'as' => 'stock-locations.update']);
            Route::get('{stockLocation}/delete', ['uses' => 'StockLocationController@delete', 'as' => 'stock-locations.delete']);
        });

        Route::group(['prefix' => 'stock-transfer'], function(){
            Route::get('/', ['uses' => 'StockTransferController@index', 'as' => 'stock-transfer']);
            Route::get('history', ['uses' => 'StockTransferController@history', 'as' => 'stock-transfer.history']);
            Route::get('create', ['uses' => 'StockTransferController@create', 'as' => 'stock-transfer.create']);
            Route::post('create', ['uses' => 'StockTransferController@store', 'as' => 'stock-transfer.store']);

            Route::group(['prefix' => '{stockTransfer}/items'], function(){
                Route::get('cancel', ['uses' => 'StockTransferController@cancel', 'as' => 'stock-transfer.cancel']);
                Route::get('transfer', ['uses' => 'StockTransferController@transfer', 'as' => 'stock-transfer.transfer']);
                Route::get('receive', ['uses' => 'StockTransferController@receive', 'as' => 'stock-transfer.receive']);

                Route::get('/', ['uses' => 'StockTransferController@items', 'as' => 'stock-transfer.items']);
                Route::get('/get/products', ['uses' => 'StockTransferController@getProducts', 'as' => 'stock-transfer.items.products']);
                Route::post('add', ['uses' => 'StockTransferController@addItem', 'as' => 'stock-transfer.items.add']);
                Route::get('delete/{stockTransferItem}', ['uses' => 'StockTransferController@deleteItem', 'as' => 'stock-transfer.items.delete']);
            });

            Route::get('{stockTransfer}/items', ['uses' => 'StockTransferController@items', 'as' => 'stock-transfer.items']);
            Route::get('{stockTransfer}/edit', ['uses' => 'StockTransferController@edit', 'as' => 'stock-transfer.edit']);
            Route::put('{stockTransfer}/edit', ['uses' => 'StockTransferController@update', 'as' => 'stock-transfer.update']);
            Route::get('{stockTransfer}/delete', ['uses' => 'StockTransferController@delete', 'as' => 'stock-transfer.delete']);

        });

		Route::group(['prefix' => 'suppliers'], function(){
			Route::get('', ['uses' => 'SupplierController@index', 'as' => 'suppliers']);
			Route::get('create', ['uses' => 'SupplierController@create', 'as' => 'suppliers.create']);
			Route::post('create', ['uses' => 'SupplierController@store', 'as' => 'suppliers.store']);
			Route::get('{supplier}/edit', ['uses' => 'SupplierController@edit', 'as' => 'suppliers.edit']);
			Route::put('{supplier}/edit', ['uses' => 'SupplierController@update', 'as' => 'suppliers.update']);
			Route::get('{supplier}/delete', ['uses' => 'SupplierController@delete', 'as' => 'suppliers.delete']);
		});

        Route::group(['prefix' => 'items'], function(){

            Route::group(['prefix' => 'categories', 'as' => 'items.'], function(){
                Route::get('', ['uses' => 'ItemCategoryController@index', 'as' => 'categories']);
                Route::get('create', ['uses' => 'ItemCategoryController@create', 'as' => 'categories.create']);
                Route::post('create', ['uses' => 'ItemCategoryController@store', 'as' => 'categories.store']);
                Route::get('{category}/edit', ['uses' => 'ItemCategoryController@edit', 'as' => 'categories.edit']);
                Route::put('{category}/edit', ['uses' => 'ItemCategoryController@update', 'as' => 'categories.update']);
                Route::get('{category}/delete', ['uses' => 'ItemCategoryController@delete', 'as' => 'categories.delete']);
            });

            Route::get('', ['uses' => 'ItemController@index', 'as' => 'items']);
            Route::get('create', ['uses' => 'ItemController@create', 'as' => 'items.create']);
            Route::post('create', ['uses' => 'ItemController@store', 'as' => 'items.store']);
            Route::get('{item}/edit', ['uses' => 'ItemController@edit', 'as' => 'items.edit']);
            Route::put('{item}/edit', ['uses' => 'ItemController@update', 'as' => 'items.update']);
            Route::get('{item}/delete', ['uses' => 'ItemController@delete', 'as' => 'items.delete']);

            Route::get('generate-variations', ['uses' => 'ItemController@generateVariations', 'as' => 'items.generate-variations']);
        });

        Route::group(['prefix' => 'employees'], function() {
            Route::get('/', ['uses' => 'EmployeesController@index', 'as' => 'employees']);
            Route::get('create', ['uses' => 'EmployeesController@create', 'as' => 'employees.create']);
            Route::post('create', ['uses' => 'EmployeesController@store', 'as' => 'employees.store']);
            Route::get('{employee}/edit', ['uses' => 'EmployeesController@edit', 'as' => 'employees.edit']);
            Route::put('{employee}/edit', ['uses' => 'EmployeesController@update', 'as' => 'employees.update']);
            Route::get('{employee}/delete', ['uses' => 'EmployeesController@delete', 'as' => 'employees.delete']);
        });

        Route::group(['prefix' => 'customers'], function() {
            Route::get('/', ['uses' => 'CustomerController@index', 'as' => 'customers']);
            Route::get('create', ['uses' => 'CustomerController@create', 'as' => 'customers.create']);
            Route::post('create', ['uses' => 'CustomerController@store', 'as' => 'customers.store']);
            Route::get('{customer}/edit', ['uses' => 'CustomerController@edit', 'as' => 'customers.edit']);
            Route::put('{customer}/edit', ['uses' => 'CustomerController@update', 'as' => 'customers.update']);
            Route::get('{customer}/delete', ['uses' => 'CustomerController@delete', 'as' => 'customers.delete']);
        });

        Route::group(['prefix' => 'tables'], function(){

            Route::group(['prefix' => 'sections', 'as' => 'tables.'], function() {
                Route::get('/', ['uses' => 'TableSectionController@index', 'as' => 'sections']);
                Route::get('create', ['uses' => 'TableSectionController@create', 'as' => 'sections.create']);
                Route::post('create', ['uses' => 'TableSectionController@store', 'as' => 'sections.store']);
                Route::get('{section}/edit', ['uses' => 'TableSectionController@edit', 'as' => 'sections.edit']);
                Route::put('{section}/edit', ['uses' => 'TableSectionController@update', 'as' => 'sections.update']);
                Route::get('{section}/delete', ['uses' => 'TableSectionController@delete', 'as' => 'sections.delete']);
            });

            Route::get('/', ['uses' => 'TableController@index', 'as' => 'tables']);
            Route::get('create', ['uses' => 'TableController@create', 'as' => 'tables.create']);
            Route::post('create', ['uses' => 'TableController@store', 'as' => 'tables.store']);
            Route::get('{table}/edit', ['uses' => 'TableController@edit', 'as' => 'tables.edit']);
            Route::put('{table}/edit', ['uses' => 'TableController@update', 'as' => 'tables.update']);
            Route::get('{table}/delete', ['uses' => 'TableController@delete', 'as' => 'tables.delete']);
        });

        Route::group(['prefix' => 'users'], function(){
            Route::get('/', ['uses' => 'UserController@index', 'as' => 'users']);
            Route::get('create', ['uses' => 'UserController@create', 'as' => 'users.create']);
            Route::post('create', ['uses' => 'UserController@store', 'as' => 'users.store']);
            Route::get('{user}/edit', ['uses' => 'UserController@edit', 'as' => 'users.edit']);
            Route::put('{user}/edit', ['uses' => 'UserController@update', 'as' => 'users.update']);
            Route::get('{user}/delete', ['uses' => 'UserController@delete', 'as' => 'users.delete']);
        });

        Route::group(['prefix' => 'roles'], function(){
            Route::get('/', ['uses' => 'RoleController@index', 'as' => 'roles']);
            Route::get('create', ['uses' => 'RoleController@create', 'as' => 'roles.create']);
            Route::post('create', ['uses' => 'RoleController@store', 'as' => 'roles.store']);
            Route::get('{role}/edit', ['uses' => 'RoleController@edit', 'as' => 'roles.edit']);
            Route::put('{role}/edit', ['uses' => 'RoleController@update', 'as' => 'roles.update']);
            Route::get('{role}/delete', ['uses' => 'RoleController@delete', 'as' => 'roles.delete']);
        });
	});

	/***********************************************************************************************************************************************
	 * Authenticated Routes for Branch Access
	 */
	Route::group(['middleware' => 'branch', 'prefix' => 'branch', 'as' => 'branch.', 'namespace' => 'Branch'], function(){

		Route::get('/', ['uses' => 'DashboardController@index', 'as' => 'dashboard']);

	});

	/**
     * Miscellaneous
     */
    Route::post('sort', '\Rutorika\Sortable\SortableController@sort');
});
